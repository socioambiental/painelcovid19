# ChangeLog

## 3.1.2 - Upcoming

### List of completed tasks

* [x] Organização:
  * [x] Tarefas movidas para o escopo mais geral do
        [Ciclo 2 do Módulo de Saúde](https://gitlab.com/socioambiental/saude/-/issues/2):
    * [x] ~~Importar [Dataset completo](https://brasil.io/dataset/covid19/files/)
          ao invés de sobrecarregar a API do Brasil.IO.~~
    * [x] ~~Resolver os dados da Sesai que foram baixados incorretamente.~~
    * [x] ~~Nova rotina de importação dos dados da Sesai (cooperação técnica com a UFABC?).~~
* [x] Dados:
  * [x] [Autenticação da API do Brasil.IO](https://blog.brasil.io/2020/10/31/nossa-api-sera-obrigatoriamente-autenticada/):
      * [x] Solicitação de conta "socioambiental" com email cadastrado "salvaguarda@socioambiental.org".
      * [x] Validação da conta.
      * [x] Criação de token.
      * [x] Teste do token.
      * [x] Implementação do token nas requisições ao Brasil.IO.
