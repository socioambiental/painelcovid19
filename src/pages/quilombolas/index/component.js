import page                         from 'AppLayouts/page/component.vue';
import mapMain                      from 'Covid19Components/maps/main/component.vue';
import controlLayers                from 'Covid19Components/controls/layers/component.vue';
import tableQuilombolasDeathsbycity from 'Covid19Components/tables/quilombolas/deathsbycity/component.vue';
import tablesQuilombolasCases       from 'Covid19Components/tables/quilombolas/cases/component.vue';
import tablesQuilombolasDeaths      from 'Covid19Components/tables/quilombolas/deaths/component.vue';
import tableQuilombolasLocalities   from 'Covid19Components/tables/quilombolas/localities/component.vue';
import chartQuilombolasLocalities   from 'Covid19Components/charts/quilombolas/localities/component.vue';

export default {
  props: { },

  data: () => ({
  }),

  components: {
    'app-page'                       : page,
    'map-main'                       : mapMain,
    'control-layers'                 : controlLayers,
    'table-quilombolas-deathsbycity' : tableQuilombolasDeathsbycity,
    'table-quilombolas-cases'        : tablesQuilombolasCases,
    'table-quilombolas-deaths'       : tablesQuilombolasDeaths,
    'table-quilombolas-localities'   : tableQuilombolasLocalities,
    'chart-quilombolas-localities'   : chartQuilombolasLocalities,
  },
}
