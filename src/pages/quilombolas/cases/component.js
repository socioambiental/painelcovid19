import page                         from 'AppLayouts/page/component.vue';
import mapMain                      from 'Covid19Components/maps/main/component.vue';
import controlLayers                from 'Covid19Components/controls/layers/component.vue';
import tableQuilombolasDeathsbycity from 'Covid19Components/tables/quilombolas/deathsbycity/component.vue';

export default {
  props: { },

  data: () => ({
  }),

  components: {
    'app-page'                       : page,
    'map-main'                       : mapMain,
    'control-layers'                 : controlLayers,
    'table-quilombolas-deathsbycity' : tableQuilombolasDeathsbycity,
  },
}
