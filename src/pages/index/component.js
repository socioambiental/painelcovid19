import page              from 'AppLayouts/page/component.vue';
import boardBrasilTotals from 'Covid19Components/boards/brasil/totals/component.vue';
//import mapMain         from 'Covid19Components/maps/main/component.vue';

export default {
  props: { },

  data: () => ({
  }),

  components: {
    'app-page'           : page,
    'board-brasil-totals': boardBrasilTotals,
    //'map-main'         : mapMain,
  },
}
