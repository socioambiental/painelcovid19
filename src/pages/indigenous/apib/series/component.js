import chartIndigenousApibSeries from 'Covid19Components/charts/indigenous/apib/series/component.vue';

export default {
  components: {
    'chart-indigenous-apib-series' : chartIndigenousApibSeries,
  },
}
