import chartIndigenousApibcolumns from 'Covid19Components/charts/indigenous/apib/columns/component.vue';

export default {
  components: {
    'chart-indigenous-apib-columns' : chartIndigenousApibcolumns,
  },
}
