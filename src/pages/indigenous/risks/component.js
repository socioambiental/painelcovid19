import page                   from 'AppLayouts/page/component.vue';
import mapMain                from 'Covid19Components/maps/main/component.vue';
import controlLayers          from 'Covid19Components/controls/layers/component.vue';
import controlIndigenousRisks from 'Covid19Components/controls/indigenous/risks/component.vue';

export default {
  props: { },

  data: () => ({
  }),

  components: {
    'app-page'                : page,
    'map-main'                : mapMain,
    'control-layers'          : controlLayers,
    'control-indigenous-risks': controlIndigenousRisks,
  },
}
