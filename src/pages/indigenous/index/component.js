import page                       from 'AppLayouts/page/component.vue';
import mapMain                    from 'Covid19Components/maps/main/component.vue';
import controlLayers              from 'Covid19Components/controls/layers/component.vue';
import boardCompare               from 'Covid19Components/boards/compare/component.vue';
import controlIndigenousRisks     from 'Covid19Components/controls/indigenous/risks/component.vue';
import boxTotalsIndigenousSesai   from 'Covid19Components/boxes/totals/indigenous/sesai/component.vue';
import boardIndigenousApibTotals  from 'Covid19Components/boards/indigenous/apib/totals/component.vue';
import chartIndigenousApibSeries  from 'Covid19Components/charts/indigenous/apib/series/component.vue';
import chartIndigenousApibColumns from 'Covid19Components/charts/indigenous/apib/columns/component.vue';

export default {
  props: { },

  data: () => ({
    compareMode: false,
  }),

  components: {
    'app-page'                     : page,
    'map-main'                     : mapMain,
    'control-layers'               : controlLayers,
    'board-compare'                : boardCompare,
    'control-indigenous-risks'     : controlIndigenousRisks,
    'box-totals-indigenous-sesai'  : boxTotalsIndigenousSesai,
    'board-indigenous-apib-totals' : boardIndigenousApibTotals,
    'chart-indigenous-apib-series' : chartIndigenousApibSeries,
    'chart-indigenous-apib-columns': chartIndigenousApibColumns,
  },
}
