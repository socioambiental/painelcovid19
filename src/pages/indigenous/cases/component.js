import page          from 'AppLayouts/page/component.vue';
import mapMain       from 'Covid19Components/maps/main/component.vue';
import controlLayers from 'Covid19Components/controls/layers/component.vue';
import sesai_chart   from 'Covid19Components/charts/indigenous/sesai/component.vue';
import boardCompare  from 'Covid19Components/boards/compare/component.vue';

export default {
  props: { },

  data: () => ({
    compareMode: false,
  }),

  components: {
    'app-page'      : page,
    'map-main'      : mapMain,
    'control-layers': controlLayers,
    'board-compare' : boardCompare,
  },
}
