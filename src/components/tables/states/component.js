export default {
  data: () => ({
    date   : null,

    // Table
    search : '',
    states : [],
  }),

  mounted: function() {
    var self = this;

    self.$store.dispatch('get', {
      url     : '/data/covid19/totals.json',
      key     : 'totals',
      mutation: 'assign',
    })
  },

  // We cannot use mapState as this.$store.state.totals is not initialized in the store
  //computed: mapState(['totals']),
  computed:  {
    totals() {
      if (this.$store.state.totals != undefined) {
        return this.$store.state.totals
      }

      return {}
    },

    headers() {
      return [
        {
          text    : this.$i18n.t('state'),
          align   : 'start',
          sortable: true,
          value   : 'state',
        },
        { text: this.$i18n.t('confirmed'),                      value: 'confirmed' },
        { text: this.$i18n.t('deaths'),                         value: 'deaths' },
        { text: this.$i18n.t('estimated_population_2019'),      value: 'estimated_population_2019' },
        { text: this.$i18n.t('confirmed_per_100k_inhabitants'), value: 'confirmed_per_100k_inhabitants' },
        { text: this.$i18n.t('death_rate'),                     value: 'death_rate', },
      ]
    },
  },

  watch: {
    totals() {
      var self = this;

      if (self.totals.brazil == undefined) {
        return
      }

      var date  = new Date(self.totals.brazil.date);
      self.date = date.toLocaleDateString() + ' - ' + date.getHours() + ':' + date.getMinutes();

      for (var item in self.totals.states) {
        self.states.push(self.totals.states[item]);
      };
    },
  }
}
