export default {
  data: () => ({
    search : '',
  }),

  mounted() {
    var self = this;

    self.$store.dispatch('get', {
      url     : '/data/covid19/quilombolas/cases.json',
      key     : 'quilombolas_cases',
      mutation: 'assign',
    })
  },

  computed: {
    items() {
      if (this.$store.state.quilombolas_cases != undefined) {
        var items = []

        for (var row in this.$store.state.quilombolas_cases) {
          var item      = this.$store.state.quilombolas_cases[row]
              item.date = item.dia + '/' + item.mes + '/' + item.ano

          items.push(item)
        }

        return items
      }

      return []
    },

    headers() {
      return [
        {
          text    : this.$i18n.t('date'),
          align   : 'start',
          sortable: true,
          value   : 'date',
        },
        {
          text    : this.$i18n.t('monitored'),
          align   : 'start',
          sortable: true,
          value   : 'monitorados',
        },
        {
          text    : this.$i18n.t('confirmed_plural'),
          align   : 'start',
          sortable: true,
          value   : 'confirmados',
        },
        {
          text    : this.$i18n.t('deaths'),
          align   : 'start',
          sortable: true,
          value   : 'óbitos',
        },
      ]
    },
  },
}
