export default {
  data: () => ({
    search : '',
  }),

  mounted() {
    var self = this;

    self.$store.dispatch('get', {
      url     : '/data/covid19/quilombolas/localities.json',
      key     : 'quilombolas_localities',
      mutation: 'assign',
    })
  },

  computed: {
    items() {
      if (this.$store.state.quilombolas_localities != undefined) {
        return this.$store.state.quilombolas_localities
      }

      return []
    },

    headers() {
      return [
        {
          text    : this.$i18n.t('state_code'),
          align   : 'start',
          sortable: true,
          value   : 'CD_UF',
        },
        {
          text    : this.$i18n.t('state_acronym'),
          align   : 'start',
          sortable: true,
          value   : 'sigla_UF',
        },
        {
          text    : this.$i18n.t('state'),
          align   : 'start',
          sortable: true,
          value   : 'NM_UF',
        },
        {
          text    : this.$i18n.t('municipality_code'),
          align   : 'start',
          sortable: true,
          value   : 'CD_MUN',
        },
        {
          text    : this.$i18n.t('municipality'),
          align   : 'start',
          sortable: true,
          value   : 'NM_MUN',
        },
        {
          text    : this.$i18n.t('locality_code'),
          align   : 'start',
          sortable: true,
          value   : 'CD_LOCALIDADE',
        },
        {
          text    : this.$i18n.t('quilombo_locality'),
          align   : 'start',
          sortable: true,
          value   : 'NM_LOCALIDADE',
        },
        {
          text    : this.$i18n.t('category_code'),
          align   : 'start',
          sortable: true,
          value   : 'CD_CATEG',
        },
        {
          text    : this.$i18n.t('category'),
          align   : 'start',
          sortable: true,
          value   : 'NM_CATEG',
        },
      ]
    }
  },
}
