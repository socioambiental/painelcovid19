export default {
  data: () => ({
    search : '',
  }),

  mounted() {
    var self = this;

    self.$store.dispatch('get', {
      url     : '/data/covid19/quilombolas/deaths.json',
      key     : 'quilombolas_deaths',
      mutation: 'assign',
    })
  },

  computed: {
    items() {
      if (this.$store.state.quilombolas_deaths != undefined) {
        var items = []

        for (var row in this.$store.state.quilombolas_deaths) {
          var item      = this.$store.state.quilombolas_deaths[row]
              item.date = item.dia + '/' + item.mes + '/' + item.ano

          items.push(item)
        }

        return items
      }

      return []
    },

    headers() {
      return [
        {
          text    : this.$i18n.t('date'),
          align   : 'start',
          sortable: true,
          value   : 'date',
        },
        /*
      {
        text    : this.$i18n.t('name'),
        align   : 'start',
        sortable: true,
        value   : 'nome',
      },
      */
        {
          text    : this.$i18n.t('age'),
          align   : 'start',
          sortable: true,
          value   : 'idade',
        },
        {
          text    : this.$i18n.t('quilombo'),
          align   : 'start',
          sortable: true,
          value   : 'quilombo',
        },
        {
          text    : this.$i18n.t('municipality'),
          align   : 'start',
          sortable: true,
          value   : 'municipio',
        },
        {
          text    : this.$i18n.t('state'),
          align   : 'start',
          sortable: true,
          value   : 'uf',
        },
      ]
    },
  },
}
