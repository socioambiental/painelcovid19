export default {
  data: () => ({
    search : '',
  }),

  mounted() {
    var self = this;

    self.$store.dispatch('get', {
      url     : '/data/covid19/quilombolas/deaths.json',
      key     : 'quilombolas_deaths',
      mutation: 'assign',
    })
  },

  computed: {
    items() {
      if (this.$store.state.quilombolas_deaths != undefined) {
        var data  = {}
        var items = []

        for (var row in this.$store.state.quilombolas_deaths) {
          var item  = this.$store.state.quilombolas_deaths[row]
          var state = item.uf
          var city  = item.municipio
          var code  = item.codigo_ibge

          if (data[code] == undefined) {
            data[code] = {
              uf       : state,
              municipio: city,
              obitos   : 0,
            }
          }

          data[code].obitos++
        }

        // Convert to array
        for (var item in data) {
          items.push(data[item])
        }

        //return items.sort(function(a, b) {
        //  return b.obitos - a.obitos
        //});
        return items
      }

      return []
    },

    headers() {
      return [
        {
          text    : this.$i18n.t('state'),
          align   : 'start',
          sortable: true,
          value   : 'uf',
        },
        {
          text    : this.$i18n.t('municipality'),
          align   : 'start',
          sortable: true,
          value   : 'municipio',
        },
        {
          text    : this.$i18n.t('deaths'),
          align   : 'start',
          sortable: true,
          value   : 'obitos',
        },
      ]
    },
  },
}
