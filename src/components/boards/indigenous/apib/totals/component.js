import boxTotalsIndigenousApib from 'Covid19Components/boxes/totals/indigenous/apib/component.vue';

export default {
  data: () => ({
    compareMode: false,
  }),

  components: {
    'box-totals-indigenous-apib' : boxTotalsIndigenousApib,
  },
}
