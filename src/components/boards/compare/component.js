//import mapMain            from 'Covid19Components/maps/main/component.vue';
import chartIndigenousSesai from 'Covid19Components/charts/indigenous/sesai/component.vue';

export default {
  props: {
    component: {
      type   : String,
      default: 'chart-indigenous-sesai',
    },
    select: {
      type   : Boolean,
      default: true,
    },
  },

  data: () => ({
    compareMode: false,
  }),

  components: {
    //'mapMain'              : mapMain,
    'chart-indigenous-sesai' : chartIndigenousSesai,
  },
}
