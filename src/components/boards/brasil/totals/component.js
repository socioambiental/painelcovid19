import mapCities              from 'Covid19Components/maps/cities/component.vue';
import chartGeneralTimeseries from 'Covid19Components/charts/general/timeseries/component.vue';
import chartGeneralStatemap   from 'Covid19Components/charts/general/statemap/component.vue';
import boxTotalsBrasil        from 'Covid19Components/boxes/totals/brasil/component.vue';
import tableStates            from 'Covid19Components/tables/states/component.vue';
import tableCities            from 'Covid19Components/tables/cities/component.vue';
//import { mapState }         from 'vuex'

export default {
  data: () => ({
    date      : null,
  }),

  components: {
    'map-cities'               : mapCities,
    'chart-general-timeseries' : chartGeneralTimeseries,
    'chart-general-statemap'   : chartGeneralStatemap,
    'box-totals-brasil'        : boxTotalsBrasil,
    'table-states'             : tableStates,
    'table-cities'             : tableCities,
  },
}
