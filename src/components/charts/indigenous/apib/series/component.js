import { mapState } from 'vuex'
import * as chart   from './chart.js'

export default {
  props  : {
    redraw: {
      type   : Boolean,
      default: false,
    },
    width: {
      type   : String,
      default: '100%',
    },
    height: {
      type   : String,
      default: '400px',
    },
  },

  data: () => ({
    locality     : 'brasil',
    chart        : null,
    currentChart : null,
  }),

  mounted: function() {
    var self = this;

    self.getOrUpdate()

    // Rebuild graph on locale-transition event
    self.$root.$on('locale-transition', function(value) {
      if (self.timeSeries != undefined != undefined) {
        self.$nextTick(function() {
          self.buildChart()
        });
      }
    })
  },

  methods: {
    destroyChart: chart.destroy,
    buildChart  : chart.build,

    getOrUpdate() {
      if (this.timeSeries == undefined || this.timeSeries[this.locality] == undefined) {
        this.getData()
      }
      else {
        this.update()
      }
    },

    getTimeSeries() {
      var self = this

      self.$store.dispatch('get', {
        url     : '/data/covid19/indigenas/apib/partial/timeseries.json',
        key     : 'timeSeriesApibPartial',
        mutation: 'assign',
      })

      self.$store.dispatch('get', {
        url     : '/data/covid19/indigenas/sesai/compiled/timeseries/dsei/BRASIL.json',
        key     : 'timeSeriesDsei',
        index   : 'BRASIL',
        mutation: 'include',
      })
    },

    getData() {
      this.getTimeSeries()
    },

    update() {
      // Do not rebuild unnecessarily
      if (this.currentChart == this.locality) {
        return
      }

      if (this.timeSeries != null && this.timeSeries[this.locality] != undefined) {
        this.buildChart()
      }
    },
  },

  computed: {
    style() {
      return 'width:' + this.width + '; height:' + this.height + ';'
    },

    //...mapState(['timeSeriesApibPartial']),
    timeSeries() {
      if (this.$store.state.timeSeriesApibPartial != undefined && this.$store.state.timeSeriesDsei != undefined && this.$store.state.timeSeriesDsei.BRASIL != undefined) {
        var series                 = Object.assign({}, this.$store.state.timeSeriesApibPartial)
        series.brasil.sesai_deaths = this.$store.state.timeSeriesDsei.BRASIL.deaths

        return series
      }

      return null
    },
  },

  watch: {
    locality() {
      this.getOrUpdate()
    },

    timeSeries() {
      this.update()
    },

    redraw() {
      if (this.chart != null) {
        //this.chart.redraw()
        this.buildChart()
      }
    },
  },
}
