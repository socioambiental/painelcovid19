//import { mapState } from 'vuex'
import * as chart     from './chart.js'

export default {
  props  : {
    redraw: {
      type   : Boolean,
      default: false,
    },
    width: {
      type   : String,
      default: '100%',
    },
    height: {
      type   : String,
      default: '400px',
    },
    type: {
      type   : String,
      default: 'peoples',
    },
  },

  data: () => ({
    chart        : null,
    currentChart : null,
    categories   : {},
  }),

  mounted: function() {
    var self = this;

    self.getOrUpdate()

    // Rebuild graph on locale-transition event
    self.$root.$on('locale-transition', function(value) {
      if (self.totals != undefined != undefined) {
        self.$nextTick(function() {
          self.buildChart()
        });
      }
    })
  },

  methods: {
    destroyChart: chart.destroy,
    buildChart  : chart.build,

    getOrUpdate() {
      if (this.totals == undefined || this.totals[this.type] == undefined) {
        this.getData()
      }
      else {
        this.update()
      }
    },

    getData() {
      var self = this

      self.$store.dispatch('get', {
        url     : '/data/covid19/indigenas/apib/full/totals.json',
        key     : 'apib_totals_full',
        mutation: 'assign',
      })
    },

    update() {
      // Do not rebuild unnecessarily
      if (this.currentChart == this.type) {
        return
      }

      // Build category array just after the list is sorted
      for (var type in this.totals) {
        this.categories[type] = {}

        for (var item in this.totals[type]) {
          this.categories[type][item] = []

          for (var element in this.totals[type][item]) {
            this.categories[type][item].push(
              this.totals[type][item][element][0]
            )
          }
        }
      }

      if (this.totals != null && this.totals[this.type] != undefined) {
        this.buildChart()
      }
    },
  },

  computed: {
    style() {
      return 'width:' + this.width + '; height:' + this.height + ';'
    },

    //...mapState(['apib_totals_full']),
    totals() {
      var totals = {}

      if (this.$store.state.apib_totals_full != undefined) {
        for (var type in this.$store.state.apib_totals_full) {
          if (totals[type] == undefined) {
            totals[type] = {}
          }

          for (var category in this.$store.state.apib_totals_full[type]) {
            if (this.categories[type] == undefined) {
              this.categories[type] = []
            }

            for (var item in this.$store.state.apib_totals_full[type][category]) {
              if (totals[type][item] == undefined) {
                totals[type][item] = []
              }

              if (this.$store.state.apib_totals_full[type][category][item] > 0) {
                totals[type][item].push([ category, this.$store.state.apib_totals_full[type][category][item] ])
              }
            }
          }
        }

        function sortSeries(series) {
          return series.sort(function(a, b) {
            return b[1] - a[1]
          })
        }

        for (var type in totals) {
          for (var item in totals[type]) {
            totals[type][item] = sortSeries(totals[type][item])
          }
        }

        return totals
      }

      return null
    },
  },

  watch: {
    type() {
      this.getOrUpdate()
    },

    totals() {
      this.update()
    },

    redraw() {
      if (this.chart != null) {
        //this.chart.redraw()
        this.buildChart()
      }
    },
  },
}
