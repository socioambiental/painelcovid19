// Load Highcharts
var Highcharts = require('highcharts');

// Load regression module
//require('highcharts-regression')(Highcharts);

// Load the exporting module, and initialize it.
require('highcharts/modules/exporting')(Highcharts);
require('highcharts/modules/export-data')(Highcharts);

import { chartLang } from 'AppPlugins/highcharts-i18n/translate.js';

export function destroy() {
  if (this.chart != null) {
    this.chart.destroy();
    this.chart = null;
  }
}

export function build() {
  var self     = this;
  var type     = self.type;
  var selector = jQuery(this.$el).find('.apib-graph')[0]
  var typeName = this.type;

  /*
   Highcharts.setOptions({
     global: {
       useUTC: false
     }
   });
   */

  // Chart locale config
  Highcharts.setOptions({
    lang: chartLang(self),
  });

  // Ensure any existing chart is destroyed
  self.destroyChart();

  var series = [
    //{
    //  name  : self.$i18n.t('confirmed_plural'),
    //  // If we sort a time series, we loose the indexing to select points at buildCluster
    //  //data: sortSeries(self.totals[type].confirmed),
    //  data  : self.totals[type].confirmed,
    //},
    {
      name  : self.$i18n.t('indigenous_deaths_apib'),
      //data: sortSeries(self.totals[type].deaths),
      data  : self.totals[type].deaths,
      type  : 'column',
      color : 'black',
    },
    //{
    //  name  : self.$i18n.t('suspected'),
    //  //data: sortSeries(self.totals[type].suspected),
    //  data  : self.totals[type].suspected,
    //},
    //{
    //  name  : self.$i18n.t('discarded'),
    //  //data: sortSeries(self.totals[type].discarded),
    //  data  : self.totals[type].discarded,
    //},
    //{
    //  name  : self.$i18n.t('infected'),
    //  //data: sortSeries(self.totals[type].infected),
    //  data  : self.totals[type].infected,
    //},
    //{
    //  name  : self.$i18n.t('cured'),
    //  //data: sortSeries(self.totals[type].cured),
    //  data  : self.totals[type].cured,
    //},
  ];

  // Graph inside the control
  self.chart = Highcharts.chart(selector, {
    credits: {
      enabled: false,
    },
    chart: {
      type: 'line',
      zoomType: 'xy',

      // Chart spacing
      spacingBottom : 0,
      spacingTop    : 5,
      spacingLeft   : 0,
      spacingRight  : 0,
    },
    title: {
      //text: self.$i18n.t('type') + ': ' + typeName,
      //text: typeNametoUpperCase(),
      text: self.$i18n.t(typeName),
    },
    //tooltip: {
    //  crosshairs: [true, false],
    //},
    xAxis: {
      //type: 'datetime',
      //title: {
      //  text: self.$i18n.t('date'),
      //},
      categories: self.categories[type]['deaths'],
    },
    yAxis: {
      title: {
        text: self.$i18n.t('cases'),
      },
    },
    series: series,
  });

  self.currentChart = type;
}
