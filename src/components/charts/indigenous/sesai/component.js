import { mapState } from 'vuex'
import * as chart   from './chart.js'

export default {
  props  : {
    redraw: {
      type   : Boolean,
      default: false,
    },
    width: {
      type   : String,
      default: '100%',
    },
    height: {
      type   : String,
      default: '400px',
    },
  },

  data: () => ({
    dsei         : 'BRASIL',
    chart        : null,
    currentChart : null,
  }),

  mounted: function() {
    var self = this;

    self.getOrUpdate()

    // Rebuild graph on locale-transition event
    self.$root.$on('locale-transition', function(value) {
      if (self.timeSeriesDsei != undefined && self.timeSeriesDsei[self.dsei] != undefined) {
        self.$nextTick(function() {
          self.buildChart()
        });
      }
    })
  },

  methods: {
    destroyChart: chart.destroy,
    buildChart  : chart.build,

    getOrUpdate() {
      if (this.timeSeriesDsei == undefined || this.timeSeriesDsei[this.dsei] == undefined) {
        this.getData()
      }
      else {
        this.update()
      }
    },

    getDseis() {
      var self = this

      self.$store.dispatch('get', {
        url     : '/data/covid19/indigenas/sesai/compiled/dseis.json',
        key     : 'dseis',
        mutation: 'assign',
      })
    },

    getTimeSeries() {
      var self = this

      self.$store.dispatch('get', {
        url     : '/data/covid19/indigenas/sesai/compiled/timeseries/dsei/' + self.dsei + '.json',
        key     : 'timeSeriesDsei',
        index   : self.dsei,
        mutation: 'include',
      })
    },

    getData() {
      this.getDseis()
      this.getTimeSeries()
    },

    update() {
      // Do not rebuild unnecessarily
      if (this.currentChart == this.dsei) {
        return
      }

      if (this.timeSeriesDsei != undefined && this.timeSeriesDsei[this.dsei] != undefined) {
      //if (this.timeSeriesDsei != null && this.timeSeriesDsei[this.dsei] != null) {
        this.buildChart()
      }
    },
  },

  computed: {
    style() {
      return 'width:' + this.width + '; height:' + this.height + ';'
    },

    ...mapState(['dseis', 'timeSeriesDsei']),
  },

  watch: {
    dsei() {
      this.getOrUpdate()
    },

    dseis() {
      this.update()
    },

    timeSeriesDsei() {
      this.update()
    },

    redraw() {
      if (this.chart != null) {
        //this.chart.redraw()
        this.buildChart()
      }
    },
  },
}
