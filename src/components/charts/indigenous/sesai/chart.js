// Load Highcharts
var Highcharts = require('highcharts');

// Load regression module
//require('highcharts-regression')(Highcharts);

// Load the exporting module, and initialize it.
require('highcharts/modules/exporting')(Highcharts);
require('highcharts/modules/export-data')(Highcharts);

import { chartLang } from 'AppPlugins/highcharts-i18n/translate.js';

export function destroy() {
  if (this.chart != null) {
    this.chart.destroy();
    this.chart = null;
  }
}

export function build() {
  var self     = this;
  var dsei     = self.dsei;
  var selector = jQuery(this.$el).find('.sesai-graph')[0]
  var dseiName = this.dsei;

  /*
   Highcharts.setOptions({
     global: {
       useUTC: false
     }
   });
   */

  // Chart locale config
  Highcharts.setOptions({
    lang: chartLang(self),
  });

  // Ensure any existing chart is destroyed
  self.destroyChart();

  var series = [
    {
      name  : self.$i18n.t('confirmed_plural'),
      // If we sort a time series, we loose the indexing to select points at buildCluster
      //data: sortSeries(self.timeSeriesDsei[dsei].confirmed),
      data  : self.timeSeriesDsei[dsei].confirmed,

      //regression: true,
      //regressionSettings: {
      //  type : 'exponential',
      //  color: 'rgba(45, 83, 83, .9)',
      //  decimalPlaces: 4,
      //  //useAllSeries: true,
      //},
    },
    {
      name  : self.$i18n.t('deaths'),
      //data: sortSeries(self.timeSeriesDsei[dsei].deaths),
      data  : self.timeSeriesDsei[dsei].deaths,
      color : 'black',
    },
    {
      name   : self.$i18n.t('suspected'),
      //data : sortSeries(self.timeSeriesDsei[dsei].suspected),
      data   : self.timeSeriesDsei[dsei].suspected,
      visible: false,
    },
    {
      name   : self.$i18n.t('discarded'),
      //data : sortSeries(self.timeSeriesDsei[dsei].discarded),
      data   : self.timeSeriesDsei[dsei].discarded,
      visible: false,
    },
    {
      name   : self.$i18n.t('infected'),
      //data : sortSeries(self.timeSeriesDsei[dsei].infected),
      data   : self.timeSeriesDsei[dsei].infected,
      visible: false,
    },
    {
      name   : self.$i18n.t('cured'),
      //data : sortSeries(self.timeSeriesDsei[dsei].cured),
      data   : self.timeSeriesDsei[dsei].cured,
      visible: false,
    },
  ];

  if (self.timeSeriesDsei[dsei].confirmed_prediction_exponential != undefined) {
    series.push({
      name   : self.$i18n.t('confirmed_prediction_exponential'),
      //data : sortSeries(self.timeSeriesDsei[dsei].cured),
      data   : self.timeSeriesDsei[dsei].confirmed_prediction_exponential,
      color  : 'red',
      visible: false,
    })
  }

  if (dsei == 'BRASIL' && self.timeSeriesDsei[dsei].sir_model_infected != undefined) {
    series.push({
      name   : self.$i18n.t('sir_model_infected'),
      data   : self.timeSeriesDsei[dsei].sir_model_infected,
      visible: false,
    })
  }

  if (dsei == 'BRASIL' && self.timeSeriesDsei[dsei].sir_model_recovered != undefined) {
    series.push({
      name   : self.$i18n.t('sir_model_recovered'),
      data   : self.timeSeriesDsei[dsei].sir_model_recovered,
      visible: false,
    })
  }

  if (dsei == 'BRASIL' && self.timeSeriesDsei[dsei].sir_model_susceptible != undefined) {
    series.push({
      name   : self.$i18n.t('sir_model_susceptible'),
      data   : self.timeSeriesDsei[dsei].sir_model_susceptible,
      visible: false,
    })
  }

  // Graph inside the control
  self.chart = Highcharts.chart(selector, {
    credits: {
      enabled: false,
    },
    chart: {
      type: 'line',
      zoomType: 'xy',

      // Chart spacing
      spacingBottom : 0,
      spacingTop    : 5,
      spacingLeft   : 0,
      spacingRight  : 0,
    },
    title: {
      //text: self.$i18n.t('dsei') + ': ' + dseiName,
      text: dseiName,
    },
    //tooltip: {
    //  crosshairs: [true, false],
    //},
    xAxis: {
      type: 'datetime',
      //title: {
      //  text: self.$i18n.t('date'),
      //},
    },
    yAxis: {
      title: {
        text: self.$i18n.t('cases'),
      },
    },
    series: series,
  });

  self.currentChart = dsei;
}
