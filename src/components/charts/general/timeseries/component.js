import { mapState } from 'vuex'
import * as chart   from 'Covid19Lib/chart.js'

export default {
  props  : {
    locality: {
      type   : Number,
      default: 0,
    },
    width: {
      type   : String,
      default: '100%',
    },
    height: {
      type   : String,
      default: '400px',
    },
  },

  data: () => ({
    chart        : null,
    currentChart : null,
  }),

  mounted: function() {
    var self = this;

    self.getData()

    // Rebuild graph on locale-transition event
    self.$root.$on('locale-transition', function(value) {
      if (self.timeSeries != undefined && self.timeSeries[self.locality] != undefined) {
        self.$nextTick(function() {
          self.buildChart(self.$el)
        });
      }
    })
  },

  methods: {
    destroyChart: chart.destroy,
    buildChart  : chart.build,

    getLocalities() {
      var self = this

      self.$store.dispatch('get', {
        url     : '/data/covid19/localities.json',
        key     : 'localities',
        mutation: 'assign',
      })
    },

    getTimeSeries() {
      var self = this

      self.$store.dispatch('get', {
        url     : '/data/covid19/timeseries/locality/' + self.locality + '.json',
        key     : 'timeSeries',
        index   : self.locality,
        mutation: 'include',
      })
    },

    getData() {
      this.getLocalities()
      this.getTimeSeries()
    },

    update() {
      if (this.timeSeries != undefined && this.timeSeries[this.locality] != undefined) {
        this.buildChart(this.$el)
      }
    },
  },

  computed: {
    style() {
      return 'width:' + this.width + '; height:' + this.height + ';'
    },

    ...mapState(['localities', 'timeSeries']),
  },

  watch: {
    locality() {
      this.getData()
    },

    localities() {
      this.update()
    },

    timeSeries() {
      this.update()
    },
  },
}
