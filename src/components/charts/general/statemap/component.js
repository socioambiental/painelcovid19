import * as graph from './graph.js';

export default {
  props  : {
    type: {
      type   : String,
      default: 'confirmed',
    },
  },

  data: () => ({
    date  : null,
    chart : null,

    // State map
    series_state: {
      confirmed: [],
      deaths   : [],
    },
  }),

  mounted: function() {
    var self = this;

    self.$store.dispatch('get', {
      url     : '/data/covid19/totals.json',
      key     : 'totals',
      mutation: 'assign',
    })

    // Rebuild graphs on locale-transition event
    self.$root.$on('locale-transition', function(value) {
      if (self.totals.brazil != undefined) {
        self.$nextTick(function() {
          self.build();
        });
      }
    })
  },

  methods: {
    build  : graph.build,
    destroy: graph.destroy,
  },

  // We cannot use mapState as this.$store.state.totals is not initialized in the store
  //computed: mapState(['totals']),
  computed:  {
    totals() {
      if (this.$store.state.totals != undefined) {
        return this.$store.state.totals
      }

      return {}
    },
  },

  watch: {
    totals() {
      var self = this;

      if (self.totals.brazil == undefined) {
        return
      }

      var date  = new Date(self.totals.brazil.date);
      self.date = date.toLocaleDateString() + ' - ' + date.getHours() + ':' + date.getMinutes();

      for (var item in self.totals.states) {
        self.series_state.confirmed.push([
          'br-' + self.totals.states[item].state.toLowerCase(),
          self.totals.states[item].confirmed,
        ]);

        self.series_state.deaths.push([
          'br-' + self.totals.states[item].state.toLowerCase(),
          self.totals.states[item].deaths,
        ]);
      };

      self.build();
    },
  }
}
