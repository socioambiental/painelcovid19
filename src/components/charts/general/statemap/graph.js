// Load Highcharts and highmaps
var Highcharts = require('highcharts/highstock');
require('highcharts/modules/map')(Highcharts);

// Load the exporting module, and initialize it.
require('highcharts/modules/exporting')(Highcharts);
require('highcharts/modules/export-data')(Highcharts);

var map = require('@highcharts/map-collection/countries/br/br-all.geo.json');

import { chartLang } from 'AppPlugins/highcharts-i18n/translate.js';

export function destroy() {
  if (self.chart != null) {
    self.chart.destroy()
    self.chart = null
  }
}

export function build() {
  var self = this;
  var type = this.type;

  self.destroy()

  // Chart locale config
  Highcharts.setOptions({
    lang: chartLang(self),
  });

  // Create the chart
  self.chart = Highcharts.mapChart(self.$el, {
    credits: {
      enabled: false,
    },

    chart: {
      map: map,
    },

    title: {
      text: self.type == 'confirmed' ? self.$i18n.t('cases') + ' ' + self.$i18n.t('confirmed_plural') + ' ' + self.$i18n.t('by_state') : self.$i18n.t('deaths') + ' ' + self.$i18n.t('by_state'),
    },

    //subtitle: {
    //  text: 'Source map: <a href="http://code.highcharts.com/mapdata/countries/br/br-all.js">Brazil</a>'
    //},

    mapNavigation: {
      enabled: true,
      buttonOptions: {
        verticalAlign: 'bottom'
      }
    },

    colorAxis: {
      minColor: '#f6f685',
      maxColor: '#ff0000',
      type    : 'logarithmic',
    },

    series: [
      {
        data: self.type == 'confirmed' ? self.series_state.confirmed : self.series_state.deaths,
        name: self.type == 'confirmed' ? self.$i18n.t('confirmed_plural') : self.$i18n.t('deaths'),
        //name  : 'Estados',
        //color: '#E0E0E0',
        //enableMouseTracking: false
        states: {
          hover: {
            color: '#BADA55'
          }
        },
        dataLabels: {
          enabled: true,
          //format: '{point.name}'
        },
      },
      //{
      //  type: 'mapbubble',
      //  name: 'Confirmados',
      //  data: self.series_state.confirmed,
      //  minSize: 4,
      //  maxSize: '12%',
      //},
    ],
  });
}
