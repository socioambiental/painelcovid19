// Load Highcharts
var Highcharts = require('highcharts');

// Load the exporting module, and initialize it.
require('highcharts/modules/exporting')(Highcharts);

export default {
  props  : {
    width: {
      type   : String,
      default: '100%',
    },
    height: {
      type   : String,
      default: '400px',
    },
  },

  data: () => ({
    chart : null,
  }),

  mounted() {
    var self = this;

    self.$store.dispatch('get', {
      url     : '/data/covid19/quilombolas/locality_series.json',
      key     : 'quilombolas_locality_series',
      mutation: 'assign',
    })

    // Rebuild graph on locale-transition event
    self.$root.$on('locale-transition', function(value) {
      if (self.series != null) {
        self.$nextTick(function() {
          self.build()
        });
      }
    })
  },

  computed: {
    style() {
      return 'width:' + this.width + '; height:' + this.height + ';'
    },

    series() {
      if (this.$store.state.quilombolas_locality_series != undefined) {
        return this.$store.state.quilombolas_locality_series
      }

      return null
    },
  },

  watch: {
    series() {
      if (this.series != null) {
        this.build()
      }
    },
  },

  methods: {
    destroy() {
      if (this.chart != null) {
        this.chart.destroy()
        this.chart = null
      }
    },

    build() {
      this.destroy()

      this.chart = Highcharts.chart(this.$el, {
        credits: {
          enabled: false,
        },
        chart: {
          type: 'column',
        },
        title: {
          text: this.$i18n.t('quilombola_localities_by_state') + ' (' + this.$i18n.t('source') + ' IBGE 2020)',
        },
        xAxis: {
          type: 'category',
        },
        yAxis: {
          title: this.$i18n.t('localities'),
        },
        series: [{
          name: this.$i18n.t('localities'),
          data: this.series.state,
        }],
      });
    },
  },
}
