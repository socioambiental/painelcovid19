// See https://stackoverflow.com/questions/44118600/web-workers-how-to-import-modules
//importScripts('datahandler.js')

import * as datahandler from 'Covid19Lib/datahandler.js'

onmessage = function(e) {
  if (e.data[0] != undefined && e.data[0] == 'buildData') {
    var payload  = e.data[1]
    var messages = e.data[2]
    var lang     = e.data[3]

    //console.log('Worker: received builData request from main.')

    datahandler.buildDates(payload)
    datahandler.buildTimeSeries(payload, messages, lang)

    postMessage(['buildData', payload])
    //postMessage(['buildData', JSON.stringify(payload)])
  }
}
