export async function mounted() {
  var self = this

  self.$root.$on('cases-map', async function(isamap) {
    self.isamap = isamap

    self.stopPropagation()
    self.localeTransition()
    await self.buildData()
    //self.buildWidgets()

    self.isamap.controls.legend.addItems([
      {
        name  : 'covid19_by_locality',
        layer : null,
        legend: [
          {
            label: 'covid19_by_locality',
            html : '<i class="fa fa-heartbeat"></i>',
          },
        ],
      },
    ])
  })
}

//export function widgets() {
//  var self = this
//
//  //self.buildSlider()
//
//  // Give time in case the select still need to be built
//  self.$nextTick(function() {
//    self.chosen()
//
//    // Somehow it needs two ticks to be fully populated
//    self.$nextTick(function() {
//      self.chosenTrigger()
//    })
//  })
//}

export function stopPropagation() {
  L.DomEvent.disableScrollPropagation(this.$el)
  L.DomEvent.disableClickPropagation(this.$el)
  L.DomEvent.on(this.$el, 'wheel', L.DomEvent.stopPropagation)
  L.DomEvent.on(this.$el, 'click', L.DomEvent.stopPropagation)
}

export function localeTransition() {
  var self = this

  self.isamap.bus.$on('locale-transition', function(value) {
    self.locale = value

    if (self.initialize == true) {
      self.buildCluster()
      self.buildChart()
    }
  })
}

export function toggleControl() {
  if (this.state == 'closed') {
    this.state = 'opened'
  }
  else {
    this.state = 'closed'
  }

  // Give time in case the select still need to be built
  this.$nextTick(function() {
    this.chosen()
  })
}

export function toggleItem(event) {
  // Activate the current item
  jQuery(event.target).closest('li').toggleClass('active')

  // Activate it's target
  var target = jQuery(event.target).closest('li').attr('data-target-selector')
  jQuery(target).toggleClass('active')

  // Deactivate all other items
  jQuery(event.target).closest('li').siblings().removeClass('active')
  jQuery(event.target).closest('li').siblings().each(function () {
    jQuery(jQuery(this).attr('data-target-selector')).removeClass('active')
  })
}

export function hideControls() {
  if (this.state == 'opened') {
    this.toggleControl()
  }

  if (jQuery('#leaflet-layer-control-map').hasClass('leaflet-control-layers-expanded')) {
    jQuery('#leaflet-layer-control-map i.fa-window-close').click()
  }
}
