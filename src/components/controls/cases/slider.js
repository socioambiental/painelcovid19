import 'ion-rangeslider'

export function build() {
  var self = this

  // Build pretty slider values
  var values = [ ]
  Object.keys(this.dates).reverse().forEach(function(value) {
    values.push(self.formatDateLabel(value, self.isamap.methods.getLocale()))
  })

  // Initialize sliders
  jQuery('#covid19-date-slider').ionRangeSlider({
    //from     : values.length - 2,
    from       : values.length - 1,
    values     : values,
    skin       : 'isamap',
    //grid     : true,
    //onChange : this.changeSingleSlider,
    onFinish   : this.changeSingleSlider,
    //prettify : function(value) {
    //  return self.formatDateLabel(values[value], self.isamap.methods.getLocale())
    //},
  })

  // Save instance
  this.slider.instance = jQuery('#covid19-date-slider').data('ionRangeSlider')
}

export function changeSingleSlider(data) {
  // Convert date value from prettified to internal version
  var parsedDate = data.from_value.split('/')
  var date       = parsedDate[2] + '-' + parsedDate[1] + '-' + parsedDate[0]
  this.date      = date

  this.setLocality(0, false, false)
}

export async function update() {
  var dateLabel = this.formatDateLabel(this.date, this.isamap.methods.getLocale())
  var instance  = this.slider.instance
  var from      = instance.options.values.indexOf(dateLabel)

  this.slider.instance.update({
    from: from,
  })
}

export function prev() {
  var instance = this.slider.instance
  var min      = instance.result.min
  var from     = instance.result.from

  if (from > min) {
    instance.update({
      from: from -1,
    })
  }

  this.changeSingleSlider(instance.result)
}

export function next() {
  var instance = this.slider.instance
  var max      = instance.result.max
  var from     = instance.result.from

  if (from < max) {
    instance.update({
      from: from + 1,
    })
  }

  if (from == max || from + 1 == max) {
    // Pause when finish
    //this.sliderPause()

    // Infinite loop
    instance.update({
      from: 0,
    })
  }

  this.changeSingleSlider(instance.result)
}

export function play() {
  if (this.slider.play == undefined) {
    this.slider.play = window.setInterval(this.sliderNext, 500)
    this.slideIcon   = this.sliderPlayIcon()

    this.setLocality(0, false, false)
  }
}

export function pause() {
  if (this.slider.play != undefined) {
    window.clearInterval(this.slider.play)
    delete this.slider.play

    this.slideIcon = this.sliderPlayIcon()
  }
}

export function toggle() {
  if (this.slider.play != undefined) {
    this.sliderPause()
  }
  else {
    this.sliderPlay()
  }
}

export function icon() {
  var icon = 'fa fa-'

  if (this.slider.play == undefined) {
    icon += 'play'
  }
  else {
    icon += 'pause'
  }

  this.sliderIcon = icon
}
