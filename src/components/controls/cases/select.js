import 'chosen-js'
import 'chosen-js/chosen.css'
import { sortSeries } from './datahandler.js'

export function build() {
  var self = this

  // Give time in case the select still need to be built
  self.$nextTick(function() {
    self.chosen()

    // Somehow it needs two ticks to be fully populated
    self.$nextTick(function() {
      self.chosenTrigger()
    })
  })
}

export function init() {
  if (this.state == 'closed') {
    return
  }

  var self = this

  if (self.initialized == false) {
    jQuery('#' + self.selectId).chosen({
      'search_contains'                   : true,
      //'include_group_label_in_selected' : true,
      'no_results_text'                   : self.isamap.methods.message('no_results'),
      'width'                             : '100%',
    })

    self.chosenChange()

    self.initialized = true
  }

  //this.chosenTrigger()
}

export function clear() {
  // Remove stuff and set default state
  // Thanks https://stackoverflow.com/questions/11365212/how-do-i-reset-a-jquery-chosen-select-option-with-jquery#11365340
  //jQuery('#' + this.selectId).val('').trigger('chosen:updated')
  //jQuery('#' + this.selectId).trigger('chosen:open')
  jQuery('#' + this.selectId).val(0).trigger('chosen:updated')
}

export function trigger() {
  var self = this

  if (this.initialized == true) {
    // Keep chosen updated
    this.$nextTick(function() {
      //jQuery('#' + self.selectId).val(this.locality).trigger('chosen:updated')
      jQuery('#'   + self.selectId).trigger('chosen:updated')
    })

    return
  }

  // Activate chosen on toggle
  //jQuery('#' + selectId .chosen-search-input').focus()
  //jQuery('#' + selectId .chosen-search-input').text('')
  //jQuery('#' + this.selectId).trigger('chosen:activate')
  //jQuery('#' + this.selectId).trigger('chosen:open')
  //jQuery('#' + this.selectId).trigger('chosen:close')
}

export function change() {
  var self = this

  jQuery('#' + this.selectId).change(function() {
    jQuery('#' + this.selectId).trigger('chosen:close')

    var locality    = this.selectedOptions[0].value
    //var item      = self.localityItem(locality)
    //var timestamp = self.formatDate(self.date)

    if (locality != 0) {
      // Change to the first date where the locality appears in the dataset
      //if (self.markers[self.date][item] == undefined && self.localities[locality].firstDate != undefined) {
      if (self.dateCases[self.date][locality] == undefined && self.localities[locality].firstDate != undefined) {
        self.date = self.localities[locality].firstDate

        //var item = self.localityItem(locality)

        //for (var point in self.timeSeries[entry].confirmed) {
        //}
        //for (var date in self.dates) {
        //  if (self.markers[date][item] != undefined) {
        //    self.date = date
        //  }
        //}
      }
    }

    // Change locality only in the next tick, giving time to any needed date change
    self.$nextTick(function() {
      self.setLocality(locality)
    })

    // Make sure chosen is updated: it was observed that if chosen-drop element has
    // position set other than "absolute" then some elements might not be updated.
    //jQuery('#' + this.selectId).trigger('chosen:updated')
  })
}
