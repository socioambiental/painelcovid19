import * as datahandler from 'Covid19Lib/datahandler.js'

// We cannot use NProgress here because this script might be loaded from a Web Worker process
//import * as NProgress from 'nprogress'
//import 'nprogress/nprogress.css'

export async function buildData() {
  var self = this

  //self.isamap.methods.showOverlay({})

  self.$store.dispatch('get', {
    url     : '/data/covid19/dates.json',
    key     : 'dates',
    mutation: 'assign',
  })

  self.$store.dispatch('get', {
    url     : '/data/covid19/localities.json',
    key     : 'localities',
    mutation: 'assign',
  })

  self.$store.dispatch('get', {
    url     : '/data/covid19/last_full_date.json',
    key     : 'last_full_date',
    mutation: 'assign',
  })

  self.$store.dispatch('get', {
    url     : '/data/covid19/timeseries/locality/' + self.locality + '.json',
    key     : 'timeSeries',
    index   : self.locality,
    mutation: 'include',
  })

  // Check for document != undefined and window != undefined because this script might be loaded from a Web Worker process
  //if (document != undefined && window != undefined) {
  //  NProgress.start()
  //  NProgress.set(0.1)
  //}
  
  // WebWorkers approach
  //if (window.Worker) {
  //  this.buildWorker()
  //}
  //else {
  //  datahandler.buildDates(this)
  //  datahandler.buildTimeSeries(this, this.isamap.i18n.messages, this.locale)
  //  this.buildLocalities()
  //  this.buildWidgets()

  //  //if (document != undefined && window != undefined) {
  //  //  NProgress.done()
  //  //}
  //}

  // Standard approach
  //datahandler.buildDates(this)
  //datahandler.buildTimeSeries(this, this.isamap.i18n.messages, this.locale)
  //this.buildLocalities()
  //this.buildWidgets()

  //this.isamap.methods.unblockUI()
}

// See https://stackoverflow.com/questions/44118600/web-workers-how-to-import-modules
//export function buildWorker() {
//  var self       = this
//  var dataWorker = new Worker('/dist/worker.js', { type: 'module' })
//
//  if (this.dataWorker == null) {
//    this.dataWorker = dataWorker
//  }
//
//  this.dataWorker.onmessage = function(e) {
//    if (e.data[0] != undefined && e.data[0] == 'buildData') {
//      var payload   = e.data[1]
//      //var payload = JSON.parse(e.data[1])
//
//      //console.log('Main thread: received builData from worker')
//
//      for (var item in payload) {
//        self[item] = payload[item]
//      }
//
//      self.buildLocalities()
//      self.buildWidgets()
//      //if (document != undefined && window != undefined) {
//      //  NProgress.done()
//      //}
//    }
//  }
//
//  var payload = {
//    data      : this.data,
//    dates     : {},
//    date      : null,
//    timeSeries: {},
//    localities: {},
//  }
//
//  this.dataWorker.postMessage(['buildData', payload, this.isamap.i18n.messages, this.locale])
//}
