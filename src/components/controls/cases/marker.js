export async function contents(date, item) {
  var self     = this
  var name     = self.localityName(item)
  var content  = ''
  //content   += '<div id="covid19-popup-graph-' + item + '" style="width:200px; height:400px;"></div>'
  //content   += self.isamap.methods.htmlTable('covid19', self.data[item])
  content     += self.isamap.methods.htmlTable(name, {
    //'date'                         : self.formatDateLabel(self.data[item].date, self.isamap.methods.getLocale()),
    'date'                           : self.formatDateLabel(date, self.isamap.methods.getLocale()),
    'confirmed_plural'               : self.dateCases[date][item].confirmed,
    'deaths'                         : self.dateCases[date][item].deaths,
    'estimated_population_2019'      : self.dateCases[date][item].estimated_population_2019,
    'confirmed_per_100k_inhabitants' : self.dateCases[date][item].confirmed_per_100k_inhabitants,
    'death_rate'                     : self.dateCases[date][item].death_rate,
    'indigenous_population'          : self.dateCases[date][item].indigenous_population,
  })

  //var title = name + ' / ' + self.data[item].date + ' - Confirmados: ' + self.data[item].confirmed + ' - Mortes: ' + self.data[item].deaths,
  //var tooltip  = name + ' / ' + self.data[item].date + '<br />'
  var tooltip  = name + ' / ' + date + '<br />'
  tooltip += self.$i18n.t('confirmed') + ':' + self.dateCases[date][item].confirmed + '<br />'
  tooltip += self.$i18n.t('deaths')    + ':' + self.dateCases[date][item].deaths

  return {
    name   : name,
    //title: title,
    tooltip: tooltip,
    popup  : content,
    icon   : self.markerIcon(self.dateCases[date][item].confirmed),
  }
}

export async function build(date, item) {
  var self           = this
  var markerContents = await self.markerContents(date, item)
  var marker         = L.marker(
    [ self.dateCases[date][item].location.latitude, self.dateCases[date][item].location.longitude ],
    markerContents,
  )

  marker.bindPopup(markerContents.popup)
  marker.bindTooltip(markerContents.tooltip)

  //marker.bindPopup(content))
  marker.on('click', function(e) {
    //self.setLocality(self.dateCases[data][item].city_ibge_code, true, false)
    self.setLocality(item, true, false)
    //self.sliderPause()
    //self.buildChart(item)
  })

  return marker
}

export function icon(confirmed) {
  var confirmed = Number(confirmed)

  // Set color according to confirmed cases
  if (confirmed < 100) {
    var color = 'green'
  }
  else if (confirmed > 100 && confirmed < 1000) {
    var color = 'yellow'
  }
  else {
    var color = 'red'
  }

  return L.ExtraMarkers.icon({
    //icon      : 'fa-ambulance',
    icon        : 'fa-heartbeat',
    markerColor : color,
    shape       : 'square',
    prefix      : 'fa',
  })
}
