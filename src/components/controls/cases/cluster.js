import * as NProgress from 'nprogress'
import 'nprogress/nprogress.css'

export function create() {
  var self = this

  return L.markerClusterGroup({
    disableClusteringAtZoom: 9,
    chunkedLoading         : true,
    chunkInterval          : 50,
    chunkDelay             : 300,
    //chunkProgress        : function(processed, total, elapsed, layersArray) {
    //  NProgress.inc()
    //},
    iconCreateFunction     : function (cluster) {
      var childCount = cluster.getChildCount()
      var c          = ' marker-cluster-'

      if (childCount < 5) {
        c += 'small'
      } else if (childCount < 15) {
        c += 'medium'
      } else {
        c += 'large'
      }

      return new L.DivIcon({
        html      : '<div><i class="' + self.iconClassCluster + '"></i><span><strong>' + childCount + '</strong></span></div>',
        className : 'energy-cluster marker-cluster' + c,
        iconSize  : new L.Point(40, 40)
      })
    },
  })
}

export async function build(langSwitched = false) {
  var self = this

  // Progress bars and overlays need to be initialized outside because this is an async function
  //NProgress.start()
  //NProgress.set(0.1)
  //self.isamap.methods.showOverlay({})

  // Destroy the chart if it's not already the country chart
  if (this.currentChart != 0) {
    this.destroyChart()
  }

  this.hideAllClusters()

  if (self.cluster[self.date] == undefined) {
    self.cluster[self.date]   = self.createCluster()
    //self.markers[self.date] = {}
    self.markers[self.date]   = []

    //var count = 0
    //for (var item in self.data) {
    //  if (self.data[item].location.latitude != undefined && self.data[item].date == self.date) {
    for (var item in self.dateCases[self.date]) {
      if (self.dateCases[self.date][item].location.latitude != undefined) {
        var marker = await self.buildMarker(self.date, item)

        // Use a monotonic array
        //self.markers[self.date].push(marker)

        // Use an array with locality keys
        self.markers[self.date][item] = marker
        self.cluster[self.date].addLayer(marker)
      }

      // Handling NProgress this manner is too slow
      //if (count++ == 500) {
      //  NProgress.inc()
      //  count = 0
      //}
    }

    //NProgress.inc(0.3)

    // Wrap addLayers() inside an async function so we don't slow the browser
    //async function clusterAdd() {
    //  return new Promise(resolve => {
    //    // Bulk add markers into the cluster
    //    //
    //    // Note that the markers variable needs to be an array and it must
    //    // be monotonically ordered and that's why we built it using .push()
    //    self.cluster[self.date].addLayers(self.markers[self.date])
    //    resolve()
    //  })
    //}

    //self.cluster[self.date].addLayers(self.markers[this.date])
    //await clusterAdd()

    //self.cluster[this.date].on('clustermouseover', function (cluster) {
    //  console.log('cluster ' + a.layer.getAllChildMarkers().length)
    //})
  }

  self.isamap.map.addLayer(self.cluster[this.date])

  // Display the country chart by default
  // Do not rebuild if the country graph is already shown
  if (this.currentChart != 0 || langSwitched == true) {
    this.buildChart()
  }

  var item = Object.keys(self.dates).indexOf(self.date)

  // Select point in the graph
  self.chart.series[0].data[item].select(true, false)
  self.chart.series[0].data[item].setState('select', true)

  //self.isamap.methods.unblockUI()
  //NProgress.done()
}

// Remove any cluster in display
export function hideAll() {
  var self = this

  for (var date in this.dates) {
    if (self.cluster[date] != undefined) {
      self.isamap.map.removeLayer(self.cluster[date])
    }
  }
}
