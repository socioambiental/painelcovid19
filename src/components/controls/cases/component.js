import * as chart       from 'Covid19Lib/chart.js'
import * as datetime    from 'Covid19Lib/datetime.js'
import * as datahandler from './datahandler.js'
import * as ui          from './ui.js'
import * as marker      from './marker.js'
import * as cluster     from './cluster.js'
import * as slider      from './slider.js'
import * as select      from './select.js'
import * as locality    from './locality.js'

import * as NProgress from 'nprogress'
import 'nprogress/nprogress.css'

export default {
  data: () => ({
    // Interface
    name         : 'covid19-control',
    icon         : 'fa-heartbeat',
    iconCluster  : 'fa-university',

    // State
    state        : 'open',

    // Data
    date         : null,
    locality     : 0,

    // Map elements
    markers      : {},
    cluster      : {},

    // Chart
    chart        : null,
    currentChart : null,

    // Slider
    slider       : {},
    sliderIcon   : 'fa fa-play',

    // Chosen
    initialized  : false,

    // Standard isaMap properties
    isamap   : null,
    locale   : null,
    id       : null,
  }),
  mounted: ui.mounted,
  methods: {
    // Basic UI
    stopPropagation  : ui.stopPropagation,
    localeTransition : ui.localeTransition,
    toggleControl    : ui.toggleControl,
    toggleItem       : ui.toggleItem,
    hideControls     : ui.hideControls,
    //buildWidgets   : ui.widgets,

    // Data handling
    buildData        : datahandler.buildData,

    // Datetime
    formatDateInternal : datetime.formatInternal,
    formatDateLabel    : datetime.formatLabel,
    formatDate         : datetime.format,

    // Slider
    buildSlider        : slider.build,
    changeSingleSlider : slider.changeSingleSlider,
    sliderPrev         : slider.prev,
    sliderNext         : slider.next,
    sliderPlay         : slider.play,
    sliderPause        : slider.pause,
    sliderToggle       : slider.toggle,
    sliderPlayIcon     : slider.icon,
    sliderUpdate       : slider.update,

    // Locality
    localityName   : locality.name,
    //localityItem : locality.item,
    setLocality    : locality.set,

    // Marker
    markerContents : marker.contents,
    buildMarker    : marker.build,
    markerIcon     : marker.icon,

    // Cluster
    createCluster   : cluster.create,
    buildCluster    : cluster.build,
    hideAllClusters : cluster.hideAll,

    // Chart
    destroyChart : chart.destroy,
    buildChart   : chart.build,

    // Chosen
    buildSelect    : select.build,
    chosen         : select.init,
    clearSelection : select.clear,
    chosenTrigger  : select.trigger, 
    chosenChange   : select.change,

    updatedDateCases() {
      //console.log('Called update cluster')
      if (this.dateCases != null && this.locality != null && this.date != null && this.dateCases[this.date] != undefined) {
        //console.log('Updating cluster')
        this.buildCluster()
        this.sliderUpdate()
      }
    },

    updatedLocality() {
      if (this.timeSeries != null && this.locality != null && this.timeSeries[this.locality] != undefined) {
        this.chosenTrigger()
        this.buildChart()
      }
    },
  },
  computed: {
    containerClass() {
      return 'leaflet-control leaflet-small-widget leaflet-bar leaflet-' + this.name + ' ' + this.state + ' ' + this.name + '-initialized-' + this.initialized
      //return 'leaflet-control leaflet-small-widget leaflet-bar leaflet-' + this.name + ' ' + this.state + ' '
    },
    controlClass() {
      return 'leaflet-small-widget-toggle ' + this.state
    },
    iconClass() {
      return 'fa ' + this.icon
    },
    iconClassCluster() {
      return 'fa ' + this.iconCluster
    },
    selectId() {
      return 'covid19-locality-selector'
    },
    lastFullDate() {
      if (this.$store.state.last_full_date != undefined) {
        return this.$store.state.last_full_date
      }

      return null
    },
    dates() {
      if (this.$store.state.dates != undefined) {
        return this.$store.state.dates
      }

      return null
    },
    timeSeries() {
      if (this.$store.state.timeSeries != undefined) {
        return this.$store.state.timeSeries
      }

      return null
    },
    localities() {
      if (this.$store.state.localities != undefined) {
        var localities = {}

        for (var locality in this.$store.state.localities) {
          localities[locality]      = this.$store.state.localities[locality]
          localities[locality].name = this.localityName(locality)
        }

        return localities
      }

      return null
    },
    //localitiesSelector() {
    //  var selector = [
    //    this.$i18n.t('brazil'),
    //  ]

    //  for (var locality in this.localities) {
    //    selector[locality] = this.localities[locality].name
    //  }

    //  return selector
    //},
    dateCases() {
      if (this.$store.state.dateCases != undefined) {
        return this.$store.state.dateCases
      }

      return null
    },
  },
  watch: {
    dates() {
      if (this.dates != null) {
        this.buildSlider()
      }
    },
    lastFullDate() {
      if (this.date == null) {
        this.date = this.lastFullDate
      }
    },
    date: async function() {
      //console.log('Date changed to ' + this.date)

      var self = this

      if (self.dateCases == null || self.dateCases[self.date] == undefined) {
        //console.log('Requesting date cases')
        self.$store.dispatch('get', {
          url     : '/data/covid19/cases/date/' + self.date + '.json',
          key     : 'dateCases',
          index   : self.date,
          mutation: 'include',
        })
      }
      else {
        self.updatedDateCases()
      }
    },
    locality: async function() {
      //console.log('Locality have changed')

      var self = this

      if (self.timeSeries == null || self.timeSeries[self.locality] == undefined) {
        self.$store.dispatch('get', {
          url     : '/data/covid19/timeseries/locality/' + self.locality + '.json',
          key     : 'timeSeries',
          index   : self.locality,
          mutation: 'include',
        })
      }
      else {
        self.updatedLocality()
      }
    },
    localities() {
      if (this.localities != null) {
        this.buildSelect()
      }
    },
    timeSeries: {
      handler: function() {
        //console.log('Time series have changed')
        this.updatedLocality()
      },
      deep: true,
    },
    dateCases: {
      handler: function() {
        //console.log('Date cases have changed')
        this.updatedDateCases()

      },
      deep: true,
    },
  },
}
