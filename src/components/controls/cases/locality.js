export function name(locality, that, messages, lang) {
  if (that == undefined) {
    var that     = this
    var messages = that.isamap.i18n.messages
    var lang     = that.isamap.settings.lang
  }

  //var localities = that.localities
  var localities   = that.$store.state.localities

  if (localities[locality] != undefined) {
    var place      = localities[locality]
    var place_type = messages[lang][place.place_type]

    return place.city != null ? place_type + ': ' + place.city + ' (' + place.state + ')' : place_type + ': ' + place.state
  }
}

export function set(locality, pause = true, panTo = true) {
  var self      = this
  this.locality = locality
  //var item    = this.localityItem()

  if (panTo == true) {
    if (locality == 0) {
      this.isamap.methods.restoreDefaultBounds()
    }
    // Needs as self.markers[self.date] indexed by locality,
    // so it won't work if the array is just monotonically indexed
    else if (this.markers[this.date] != undefined && this.markers[this.date][locality] != undefined) {
    //else if (this.dateCases[this.date] != undefined && this.dateCases[this.date][locality] != undefined) {
      var latLng = L.latLng(
        this.dateCases[this.date][locality].location.latitude,
        this.dateCases[this.date][locality].location.longitude
      )

      this.isamap.map.panTo(latLng)
      this.isamap.map.setView(latLng, 8)

      // Needs as self.markers[self.date] indexed by locality,
      // so it won't work if the array is just monotonically indexed
      this.$nextTick(function() {
        self.markers[self.date][locality].openPopup()
      })
    }
  }

  if (pause) {
    this.sliderPause()
  }
}

//export function item(locality = null) {
//  locality = locality == null ? this.locality : locality
//
//  return locality == 0 ? 'country' : this.localities[locality].item
//}
