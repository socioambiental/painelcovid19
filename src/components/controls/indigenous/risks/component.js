export default {
  props: {
    href: {
      type   : String,
      default: null,
    },
    title: {
      type   : String,
      default: null,
    },
    containerClass: {
      type   : String,
      default: null,
    },
    idPrefix: {
      type   : String,
      default: null,
    },
    scales: {
      type   : Number,
      default: 0,
    },
  },
  data: () => ({
  }),
}
