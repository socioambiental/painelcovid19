export default {
  props  : {
    type: {
      type   : String,
      default: 'confirmed',
    },
  },

  data: () => ({
    deathRate : null,
    date      : null,
  }),

  mounted: function() {
    var self = this;

    self.$store.dispatch('get', {
      url     : '/data/covid19/totals.json',
      key     : 'totals',
      mutation: 'assign',
    })
  },

  // We cannot use mapState as this.$store.state.totals is not initialized in the store
  //computed: mapState(['totals']),
  computed:  {
    totals() {
      if (this.$store.state.totals != undefined) {
        return this.$store.state.totals
      }

      return {}
    },
  },

  watch: {
    totals() {
      var self = this;

      if (self.totals.brazil == undefined) {
        return
      }

      var date       = new Date(self.totals.brazil.date)
      self.date      = date.toLocaleDateString() + ' - ' + date.getHours() + ':' + date.getMinutes()
      self.deathRate = parseFloat(self.totals.brazil.deaths / self.totals.brazil.confirmed * 100).toFixed(2)
    },
  }
}
