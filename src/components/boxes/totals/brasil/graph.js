// Load Highcharts and highmaps
var Highcharts = require('highcharts/highstock');
require('highcharts/modules/map')(Highcharts);

var map = require('@highcharts/map-collection/countries/br/br-all.geo.json');

export function stateMapConfirmed() {
  var self = this;

  if (self.charts.confirmed != null) {
    self.charts.confirmed.destroy()
    self.charts.confirmed = null
  }

  // Create the chart
  self.charts.confirmed = Highcharts.mapChart('state-map-confirmed', {
    credits: {
      enabled: false,
    },

    chart: {
      map: map,
    },

    title: {
      text: self.$i18n.t('cases') + ' '+ self.$i18n.t('confirmed') + ' ' + self.$i18n.t('by_state'),
    },

    //subtitle: {
    //  text: 'Source map: <a href="http://code.highcharts.com/mapdata/countries/br/br-all.js">Brazil</a>'
    //},

    mapNavigation: {
      enabled: true,
      buttonOptions: {
        verticalAlign: 'bottom'
      }
    },

    colorAxis: {
      minColor: '#f6f685',
      maxColor: '#ff0000',
      type    : 'logarithmic',
    },

    series: [
      {
        data: self.series_state.confirmed,
        name: self.$i18n.t('confirmed_plural'),
        //name  : 'Estados',
        //color: '#E0E0E0',
        //enableMouseTracking: false
        states: {
          hover: {
            color: '#BADA55'
          }
        },
        dataLabels: {
          enabled: true,
          //format: '{point.name}'
        },
      },
      //{
      //  type: 'mapbubble',
      //  name: 'Confirmados',
      //  data: self.series_state.confirmed,
      //  minSize: 4,
      //  maxSize: '12%',
      //},
    ],
  });
}

export function stateMapDeaths() {
  var self = this;

  if (self.charts.deaths != null) {
    self.charts.deaths.destroy()
    self.charts.deaths = null
  }

  // Create the chart
  self.charts.deaths = Highcharts.mapChart('state-map-deaths', {
    credits: {
      enabled: false,
    },

    chart: {
      map: map,
    },

    title: {
      text: self.$i18n.t('deaths') + ' ' + self.$i18n.t('by_state'),
    },

    //subtitle: {
    //  text: 'Source map: <a href="http://code.highcharts.com/mapdata/countries/br/br-all.js">Brazil</a>'
    //},

    mapNavigation: {
      enabled: true,
      buttonOptions: {
        verticalAlign: 'bottom'
      }
    },

    colorAxis: {
      minColor: '#f6f685',
      maxColor: '#ff0000',
      type    : 'logarithmic',
    },

    series: [
      {
        data: self.series_state.deaths,
        name: self.$i18n.t('deaths'),
        //name  : 'Estados',
        //color: '#E0E0E0',
        //enableMouseTracking: false
        states: {
          hover: {
            color: '#BADA55'
          }
        },
        dataLabels: {
          enabled: true,
          //format: '{point.name}'
        },
      },
      //{
      //  type: 'mapbubble',
      //  name: 'Confirmados',
      //  data: self.series_state.deaths,
      //  minSize: 4,
      //  maxSize: '12%',
      //},
    ],
  });
}
