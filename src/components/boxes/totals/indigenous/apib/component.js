export default {
  props  : {
    type: {
      type   : String,
      default: 'confirmed',
    },
  },

  data: () => ({
    deathRate : null,
    date      : null,
  }),

  mounted: function() {
    var self = this;

    self.$store.dispatch('get', {
      url     : '/data/covid19/indigenas/sesai/compiled/totals.json',
      key     : 'sesai_totals',
      mutation: 'assign',
    })

    self.$store.dispatch('get', {
      url     : '/data/covid19/indigenas/apib/partial/totals.json',
      key     : 'apib_totals_partial',
      mutation: 'assign',
    })

    self.$store.dispatch('get', {
      url     : '/data/covid19/indigenas/apib/full/totals.json',
      key     : 'apib_totals_full',
      mutation: 'assign',
    })
  },

  // We cannot use mapState as this.$store.state.totals is not initialized in the store
  //computed: mapState(['totals']),
  computed:  {
    sesai_totals() {
      if (this.$store.state.sesai_totals != undefined) {
        return this.$store.state.sesai_totals
      }

      return {}
    },

    apib_totals_partial() {
      if (this.$store.state.apib_totals_partial != undefined) {
        return this.$store.state.apib_totals_partial
      }

      return {}
    },

    apib_totals_full() {
      if (this.$store.state.apib_totals_full != undefined) {
        return this.$store.state.apib_totals_full
      }

      return {}
    },

    totals() {
      if (this.apib_totals_partial.confirmed == undefined || this.sesai_totals.totals == undefined ||
          this.apib_totals_full.confirmed    == undefined) {
        return {}
      }
      else {
        var peoples = []

        // Exclude "No information" (SI) people type
        // Use the full dataset for people counter
        //for (var people in this.apib_totals_partial.peoples) {
        for (var people in this.apib_totals_full.peoples) {
          if (people != 'SI') {
            peoples.push(people)
          }
        }

        return {
          confirmed: Number(this.apib_totals_partial.confirmed) + Number(this.sesai_totals.totals.confirmed),
          deaths   : Number(this.apib_totals_partial.deaths)    + Number(this.sesai_totals.totals.deaths),
          //peoples: Object.keys(this.apib_totals_full.peoples).length,
          peoples  : peoples.length,
          date     : this.sesai_totals.date,
        }
      }
    }
  },

  watch: {
    totals() {
      var self = this;

      if (self.totals.confirmed == undefined) {
        return
      }

      var date       = new Date(self.totals.date)
      self.date      = date.toLocaleDateString() + ' - ' + date.getHours() + ':' + date.getMinutes()
      self.deathRate = parseFloat(self.totals.deaths / self.totals.confirmed * 100).toFixed(2)
    },
  }
}
