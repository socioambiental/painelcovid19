export default {
  props  : {
    type: {
      type   : String,
      default: 'confirmed',
    },
  },

  data: () => ({
    deathRate : null,
    date      : null,
  }),

  mounted: function() {
    var self = this;

    self.$store.dispatch('get', {
      url     : '/data/covid19/indigenas/sesai/compiled/totals.json',
      key     : 'sesai_totals',
      mutation: 'assign',
    })
  },

  // We cannot use mapState as this.$store.state.totals is not initialized in the store
  //computed: mapState(['totals']),
  computed:  {
    totals() {
      if (this.$store.state.sesai_totals != undefined) {
        return this.$store.state.sesai_totals
      }

      return {}
    },
  },

  watch: {
    totals() {
      var self = this;

      if (self.totals.totals == undefined) {
        return
      }

      var date       = new Date(self.totals.date)
      self.date      = date.toLocaleDateString() + ' - ' + date.getHours() + ':' + date.getMinutes()
      self.deathRate = parseFloat(self.totals.totals.deaths / self.totals.totals.confirmed * 100).toFixed(2)
    },
  }
}
