import * as map from './map.js';

export default {
  data: () => ({
    map       : null,
    mapDiv    : null,
    loadingDiv: null,
  }),

  mounted: function() {
    var self        = this
    self.mapDiv     = jQuery(self.$el).find('.city-map')
    self.loadingDiv = jQuery(self.$el).find('.city-map-loading')
    self.locale     = self.$i18n.locale

    // Register to the app locale-transition event
    self.$root.$on('locale-transition', function(value) {
      self.build()
    })

    self.build()
  },

  methods: {
    build: map.build,
  },
}
