// Using code from Brasil.IO's COVID19 project:
// https://github.com/turicas/brasil.io/blob/219dd8095004fdd6d2107c0c1fbdb42f7bd68bc3/covid19/static/js/covid19-map.js
// https://github.com/turicas/brasil.io/blob/219dd8095004fdd6d2107c0c1fbdb42f7bd68bc3/covid19/templates/dashboard.html
//
// Which is licensed under GPLv3:
// https://raw.githubusercontent.com/turicas/brasil.io/219dd8095004fdd6d2107c0c1fbdb42f7bd68bc3/LICENSE
//
// Changed to adapt as a VueJS component.

import L from 'IsaMap/map/leaflet.js'

var self

var dataURL = {
  cities       : "https://brasil.io/covid19/cities/cases",
  stateGeoJSON : "https://data.brasil.io/dataset/shapefiles-brasil/0.01/BR-UF.geojson",
  cityGeoJSON  : "https://brasil.io/covid19/cities/geo",
}

var cityData,
    cityGeoJSON,
    cityLayer,
    countryData,
    countryId,
    dataConfig,
    legendBins,
    map,
    placeDataControl,
    legendControl,
    selectedPlace,
    selectedVar,
    stateGeoJSON,
    stateLayer,
    varControl,
    maxValues,
    notesControl,
    mapDiv,
    loadingDiv,
    dataConfig

var legendBins    = 6
var countryId     = 0; // Brasil
var selectedPlace = countryId

function getPlaceData(place) {
  return place == countryId ? countryData : cityData[place]
}

function opacityFromValue(value, maxValue) {
  return Math.log2(value + 1) / Math.log2(maxValue + 1)
}

function valueFromOpacity(opacity, maxValue) {
  return parseInt(2 ** (opacity * Math.log2(maxValue + 1)) - 1)
}

function cityStyle(feature) {
  var value    = cityData[feature.id][selectedVar] || 0
  var maxValue = maxValues[selectedVar]
  var opacity  = opacityFromValue(value, maxValue)

  return {
    color       : "#000",
    fillColor   : dataConfig[selectedVar].color,
    fillOpacity : opacity,
    lineJoin    : "round",
    opacity     : 0,
    weight      : 1,
  }
}

function stateStyle(feature) {
  return {
    color       : "#000",
    fillColor   : "#FFF",
    fillOpacity : 0.1,
    lineJoin    : "round",
    opacity     : 1,
    weight      : 0.5
  }
}

function changeVar(newVar) {
  selectedVar = newVar
  cityLayer.setStyle(cityStyle)
  updateLegendControl()
}

function updateLegendControl() {
  var color         = dataConfig[selectedVar].color,
      div           = legendControl.getContainer(),
      labels        = [`<b>${dataConfig[selectedVar].displayText}</b>`, "<br><br>"],
      maxValue      = maxValues[selectedVar],
      zeroDisplay   = dataConfig[selectedVar].zeroText,
      displayValue,
      lastOpacity

  for (var counter = 0; counter <= legendBins; counter += 1) {
    var opacity   = counter / legendBins
    var value     = valueFromOpacity(opacity, maxValue)
    var lastValue = lastOpacity === undefined ? 0 : valueFromOpacity(lastOpacity, maxValue)

    if (lastOpacity === undefined || lastValue != value) {
      displayValue = lastOpacity === undefined ? zeroDisplay : `${lastValue} &mdash; ${value}`
      labels.push(`<span class="valign-wrapper"> <i style="background: ${color}; opacity: ${opacity}"></i> ${displayValue} </span>`)
    }

    lastOpacity = opacity
  }

  div.innerHTML = labels.join("")
}

function updatePlaceDataControl(placeData) {
  var div       = placeDataControl.getContainer()
  var dataLines = []

  Object.keys(dataConfig).forEach(function(item) {
    var value = Intl.NumberFormat("pt-BR").format(placeData[item])
    value     = item.endsWith("percent") ? `${value}%` : value
    dataLines.push(`<dt>${dataConfig[item].displayText}:</dt> <dd>${value}</dd>`)
  })

  div.innerHTML = `
    <b>${placeData.city}</b>
    <br>
    <dl>
    ${dataLines.join("\n")}
    </dl>
    `
}

function updateVarControl() {
  var div    = varControl.getContainer()
  var inputs = ["<b>" + self.$i18n.t('select_variable') + "</b>"]

  Object.keys(dataConfig).forEach(function(item) {
    inputs.push(`<label><input type="radio" class="radio-control" name="radio-var-control" value="${item}"><span>${dataConfig[item].displayText}</span></label>`)
  })

  div.innerHTML = inputs.join("<br>")

  jQuery(".radio-control").change(function() {
    changeVar(jQuery(this).val())
  })
}

function destroyMap() {
  if (map != undefined) {
    if (placeDataControl != undefined) {
      placeDataControl.remove()
      legendControl.remove()
      varControl.remove()
      notesControl.remove()
    }

    map.remove()

    cityLayer        = undefined
    countryId        = undefined
    dataConfig       = undefined
    map              = undefined
    placeDataControl = undefined
    legendControl    = undefined
    selectedPlace    = undefined
    selectedVar      = undefined
    stateLayer       = undefined
    varControl       = undefined
    notesControl     = undefined
    dataConfig       = undefined
    selectedPlace    = countryId
  }
}

function createMap() {
  // See https://github.com/Leaflet/Leaflet/issues/2302
  map = new L.map(mapDiv[0], {
    zoomSnap           : 0.25,
    zoomDelta          : 0.25,
    minZoom            : 4.5,
    maxZoom            : 9,
    attributionControl : false
  })

  map.setView([-15, -54], 4.75)
}

function hasToAddStateLayer() {
  return stateGeoJSON !== undefined && stateLayer === undefined
}

function hasToAddCityLayer() {
  return stateLayer !== undefined && cityGeoJSON !== undefined && cityLayer === undefined
}

function hasToAddLegendLayer() {
  return stateLayer !== undefined && cityLayer !== undefined
}

function mapHasLoaded() {
  return stateLayer !== undefined && cityLayer !== undefined
}

function updateMap() {
  if (hasToAddStateLayer()) {
    stateLayer = L.geoJSON(stateGeoJSON, {style: stateStyle}).addTo(map)
  }

  if (hasToAddCityLayer()) {
    cityGeoJSON.features = cityGeoJSON.features.filter(function (item) {
      var city = cityData[item.id]
      return city !== undefined
    })
    cityLayer = L.geoJSON(
      cityGeoJSON,
      {
        style         : cityStyle,
        onEachFeature : function (feature, layer) {
          layer.on("mouseover", function () {
            this.setStyle({opacity: 1})
            updatePlaceDataControl(getPlaceData(this.feature.id))
          })

          layer.on("mouseout", function () {
            this.setStyle({opacity: 0})
            updatePlaceDataControl(getPlaceData(countryId))
          })
        }
      }
    ).addTo(map)
  }

  if (hasToAddLegendLayer()) {
    notesControl       = L.control({position: "bottomleft"})
    notesControl.onAdd = function (map) {
      var div        = L.DomUtil.create("div", "info legend")
      div.innerHTML  = self.$i18n.t('source') + ': ' + self.$i18n.t('state_health_secretaries')
      div.innerHTML += '<br>' + self.$i18n.t('brasil_io_harvest_code')
      div.innetHTML += '<br>' + self.$i18n.t('map_imported_cases')

      return div
    }

    notesControl.addTo(map)

    legendControl       = L.control({position: "bottomleft"})
    legendControl.onAdd = function (map) {
      var div = L.DomUtil.create("div", "info legend")
      return div
    }

    legendControl.addTo(map)
    updateLegendControl()

    placeDataControl = L.control({position: "bottomright"})
    placeDataControl.onAdd = function (map) {
      var div = L.DomUtil.create("div", "info legend")
      return div
    }

    placeDataControl.addTo(map)
    updatePlaceDataControl(getPlaceData(selectedPlace))

    varControl       = L.control({position: "topright"})
    varControl.onAdd = function (map) {
      var div = L.DomUtil.create("div", "info legend")
      return div
    }

    varControl.addTo(map)
    updateVarControl()
  }

  if (mapHasLoaded()) {
    jQuery(loadingDiv).css("z-index", -999)
    jQuery(".radio-control:first").prop("checked", true).trigger("click")
  }
}

function retrieveData() {
  jQuery.getJSON(
    dataURL.cities,
    function (data) {
      cityData    = data.cities
      maxValues   = data.max
      countryData = data.total

      updateMap()
    }
  )

  jQuery.getJSON(
    dataURL.stateGeoJSON,
    function (data) {
      stateGeoJSON = data
      updateMap()
    }
  )

  jQuery.getJSON(
    dataURL.cityGeoJSON,
    function (data) {
      cityGeoJSON = data
      updateMap()
    }
  )
}

export function build() {
  self       = this
  mapDiv     = self.mapDiv
  loadingDiv = self.loadingDiv

  destroyMap()

  dataConfig = {
    "confirmed": {
      "color"       : "#00F",
      "displayText" : self.$i18n.t('confirmed_plural'),
      "zeroText"    : self.$i18n.t('none'),
    },
    "confirmed_per_100k_inhabitants": {
      "color"       : "#2B580C",
      "displayText" : self.$i18n.t('confirmed_per_100k_inhabitants'),
      "zeroText"    : self.$i18n.t('none'),
    },
    "deaths": {
      "color"       : "#F00",
      "displayText" : self.$i18n.t('deaths'),
      "zeroText"    : self.$i18n.t('none'),
    },
    "death_rate_percent": {
      "color"       : "#F08",
      "displayText" : self.$i18n.t('letality'),
      "zeroText"    : self.$i18n.t('none'),
    },
    "deaths_per_100k_inhabitants": {
      "color"       : "#F39",
      "displayText" : self.$i18n.t('deaths_per_100k_inhabitants'),
      "zeroText"    : self.$i18n.t('none'),
    }
  }

  selectedVar = Object.keys(dataConfig)[0]

  createMap()

  // Only fetch if its not available
  if (stateGeoJSON == undefined || cityGeoJSON == undefined || cityData == undefined) {
    retrieveData()
  }
  else {
    updateMap()
  }
}
