import { layerDefinitions } from './layers.js';
import * as mapHandler      from 'IsaMap/map/instance.js';
import { popup            } from './popup.js';
import { customMessages   } from 'AppConfig/app.messages.js';

export function params() {
  var self = this;

  // Map params
  //var params = getQueryString();
  var params = {};

  // Layers object
  var layers = {};

  var expanded = false;

  // URL layers
  if (params.layers == undefined) {
    params.layers = [];
  }

  // Custom map parameters
  params.debug                  = true;
  params.about                  = false;
  params.searchControl          = false;
  params.embedControl           = false;
  params.techNoteControl        = false;
  params.localeControl          = true;
  params.localeControlPosition  = 'topright';
  params.statusControlPosition  = 'bottomleft';
  params.legendControl          = true;
  params.printControl           = false;
  params.localeControl          = false;
  params.fileLayerControl       = false;
  params.layerDefinitionsExtend = false;
  params.layerControlPosition   = 'topright';
  params.layerDefinitionsExtend = true;
  params.urlChange              = false;
  params.scrollWheelZoom        = false;
  params.baseLayerExcluded      = [ 'base.topographic', 'base.satellite', 'base.gray', 'base.darkgray', 'base.imagery', 'base.terrain', ];
  params.groupExcluded          = [ 'ucs', 'infra', 'jurisdicao', 'ambiente', 'queimadas', 'desmatamento' ];
  //params.layerControlState    = this.fullsize ? 'closed' : 'opened';
  params.layerControlState      = 'opened';
  params.layerControlAttachTo   = 'layers';
  params.zoom                   = 4;
  params.customMessages         = customMessages;

  if (self.tiRisks) {
    // Avoid layer duplication
    params.layerExcluded = [ 'tis.limits' ];
    params.groupExcluded.push('quilombos');
  }

  if (self.yanomamiRisks) {
    // Avoid layer duplication
    params.layerExcluded = [ 'sesai.polobase' ];
    params.groupExcluded.push('quilombos');

    params.center = [ 2.22, -63.6 ];
    params.zoom   = 7;
  }

  if (self.quilomboCases) {
    expanded = true

    params.groupExcluded.push('limits');
    params.groupExcluded.push('tis');
    params.groupExcluded.push('sesai');

    //params.layers         = [ 'quilombos.quilombos', 'quilombos.communities', 'quilombos.localities' ];
    params.layers           = [ 'quilombos.localities_uf' ];
    params.additionalLegend = [
      {
        name  : 'deaths',
        layer : null,
        legend: [
          {
            label: 'deaths',
            html : '<i class="fa fa-heartbeat"></i>',
          },
        ],
      },
    ]
  }

  // Layer definitions
  params.layerDefinitions = layerDefinitions(self, expanded);

  // Additional map parameters that might be overriden by URL config
  params.baseLayer = params.baseLayer     != undefined ? params.baseLayer : 'base.community';
  params.layers    = params.layers.length  > 0         ? params.layers    : [ ];

  return params;
}

export function map() {
  var self   = this;
  var params = this.buildParams();

  // Initialize map
  self.isamap = mapHandler.add(self.$el, params);

  // Disable mouse wheel (this is now done via config)
  //self.isamap.map.scrollWheelZoom.disable();
  //if (!L.Browser.touch) {
  //  var div = L.DomUtil.get('#map');
  //
  //  L.DomEvent.disableClickPropagation(div);
  //  L.DomEvent.on(div, 'wheel',      L.DomEvent.stopPropagation);
  //  L.DomEvent.on(div, 'mousewheel', L.DomEvent.stopPropagation);
  //}

  // Override popup function with custom code
  self.isamap.methods.arpPopupContents = popup(self.isamap);

  // Register to the app locale-transition event
  self.$root.$on('locale-transition', function(value) {
    self.isamap.methods.localeTransition(value)
  })
}
