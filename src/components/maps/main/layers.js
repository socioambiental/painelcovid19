// Either one of these should be loaded so L becomes available
import { isaMap } from 'IsaMap/map/isamap.js';
//import L from '../map/leaflet.js';

var covidServices = 'https://geo.socioambiental.org/webadaptor1/rest/services/covid/AnaliseCovid/MapServer';
var garimpoServer = 'https://geo.socioambiental.org/webadaptor2/rest/services/PilotoGarimpo/garimpo_ilegal/MapServer';
var garimpoImages = 'https://mapa.eco.br/images/custom/garimpo/';

export var layerDefinitions = function(self, expanded = false) {
  return {
    limits: {
      groupName: 'localities',
      expanded: expanded,
      layers: {
        states: {
          description: 'states',
          layer: L.esri.dynamicMapLayer({
            disableCache: false,
            format: 'png32',
            url:     covidServices,
            //token:   apiKey,
            attribution: 'IBGE (2015)',
            layers:  [ 2 ],
            opacity: 1,
          }),
          identify: function (error, featureCollection) {
            if (error || featureCollection.features.length === 0) {
              return false;
            } else {
              var content = self.isamap.methods.htmlTable('state', featureCollection.features[0].properties);

              return content;
            }
          },
          legend: [
            {
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(0, 0, 0, 255)",
                "width"            : "10px",
                "height"           : "10px",
              },
            },
          ],
        },

        municipalities : {
          description: 'municipalities',
          layer: L.esri.dynamicMapLayer({
            disableCache: false,
            format: 'png32',
            url:     covidServices,
            //token:   apiKey,
            attribution: 'IBGE (2015)',
            layers:  [ 5 ],
            opacity: 1,
          }),
          identify: function (error, featureCollection) {
            if (error || featureCollection.features.length === 0) {
              return false;
            } else {
              var content = self.isamap.methods.htmlTable('municipality', featureCollection.features[0].properties);

              return content;
            }
          },
          legend: [
            {
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(204, 204, 204, 255)",
                "width"            : "10px",
                "height"           : "10px",
              },
            },
          ],
        },
      },
    },

    hospitals: {
      groupName: 'hospitals',
      expanded: expanded,
      layers: {
        uti: {
          description: 'uti',
          layer: L.esri.Cluster.featureLayer({
            url:          covidServices + '/1',
            //proxy:        self.isamap.settings.proxyUrl,
            //token:        apiKey,
            attribution   : 'CNES / Datasus',
            opacity:      0.8,
            polygonOptions: {
              color       : '#e57b08',
              weight      : 4,
              opacity     : 1,
              fillOpacity : 0.5
            },
            spiderfyOnMaxZoom: false,
            disableClusteringAtZoom: 9,
            iconCreateFunction: function (cluster) {
              var childCount = cluster.getChildCount();
              var c          = ' marker-cluster-';

              if (childCount < 10) {
                c += 'small';
              } else if (childCount < 100) {
                c += 'medium';
              } else {
                c += 'large';
              }

              return new L.DivIcon({
                html      : '<div><i class="fa fa-hospital-o"></i><span><strong>' + childCount + '</strong></span></div>',
                className : 'energy-cluster marker-cluster' + c,
                iconSize  : new L.Point(40, 40),
              });
            },
            pointToLayer: function (geojson, latlng) {
              var color = 'green';
              var icon  = L.ExtraMarkers.icon({
                icon        : 'fa-hospital-o',
                markerColor : color,
                shape       : 'square',
                prefix      : 'fa',
              });

              return L.marker(latlng, {
                icon: icon,
              });
            },
          }),
          click: function(e) {
            var attributes = e.layer.feature.properties;
            var content    = self.isamap.methods.htmlTable('uti', {
              //co_unidade : attributes.co_unidade,
              //co_cnes    : attributes.co_cnes,
              nome_munic   : attributes.nome_munic,
              //nome1      : attributes.nome1,
              nome_fanta   : attributes.nome_fanta,
              //co_leito1  : attributes.co_leito1,
              ds_leito1    : attributes.ds_leito1,
              //tp_leito1  : attributes.tp_leito1,
              qtd_exist1   : attributes.qtd_exist1,
              //qtd_contr1 : attributes.qtd_contr1,
              qtd_sus1     : attributes.qtdsus1,
              logradouro   : attributes.logradouro + ', ' + attributes.endereco1,
              //endereco1  : attributes.endereco1,
              //complement : attributes.complement,
              //bairro1    : attributes.bairro1,
              //cep1       : attributes.cep1,
              //regiao_sau : attributes.regiao_sau,
              //distrito_s : attributes.distrito_s,
              //telefone1  : attributes.telefone1,
              //fax1       : attributes.fax1,
            });

            e.layer.bindPopup(content);
            e.layer.openPopup();
          },
          legend: [
            {
              label: 'hospitals',
              html: '<i class="fa fa-hospital-o"></i>',
            },
          ],
        },

        //respirator: {
        //  description: 'respirator',
        //  layer: L.esri.Cluster.featureLayer({
        //    url:          covidServices + '/0',
        //    //proxy:        self.isamap.settings.proxyUrl,
        //    //token:        apiKey,
        //    attribution   : 'CNES / Datasus',
        //    opacity:      0.8,
        //    polygonOptions: {
        //      color       : '#e57b08',
        //      weight      : 4,
        //      opacity     : 1,
        //      fillOpacity : 0.5
        //    },
        //    spiderfyOnMaxZoom: false,
        //    disableClusteringAtZoom: 9,
        //    iconCreateFunction: function (cluster) {
        //      var childCount = cluster.getChildCount();
        //      var c          = ' marker-cluster-';

        //      if (childCount < 10) {
        //        c += 'small';
        //      } else if (childCount < 100) {
        //        c += 'medium';
        //      } else {
        //        c += 'large';
        //      }

        //      return new L.DivIcon({
        //        html      : '<div><i class="fa fa-h-square"></i><span><strong>' + childCount + '</strong></span></div>',
        //        className : 'energy-cluster marker-cluster' + c,
        //        iconSize  : new L.Point(40, 40),
        //      });
        //    },
        //    pointToLayer: function (geojson, latlng) {
        //      var color = 'green';
        //      var icon  = L.ExtraMarkers.icon({
        //        icon        : 'fa-h-square',
        //        markerColor : color,
        //        shape       : 'square',
        //        prefix      : 'fa',
        //      });

        //      return L.marker(latlng, {
        //        icon: icon,
        //      });
        //    },
        //  }),
        //  click: function(e) {
        //    var attributes = e.layer.feature.properties;
        //    var content    = self.isamap.methods.htmlTable('respirator', {
        //      nome_fanta : attributes.nome_fanta,
        //      co_unidade : attributes.co_unidade,
        //      //qtd_sus1 : attributes.qtd_sus1,
        //    });

        //    e.layer.bindPopup(content);
        //    e.layer.openPopup();
        //  },
        //  legend: [
        //    {
        //      label: 'respirator',
        //      html: '<i class="fa fa-h-square"></i>',
        //    },
        //  ],
        //},
      },
    },

    sesai: {
      groupName: 'indigenous_health',
      expanded: expanded,
      layers: {
        dsei: {
          description: 'dsei',
          layer: L.esri.dynamicMapLayer({
            disableCache: false,
            format: 'png32',
            url:     covidServices,
            //token:   apiKey,
            attribution: 'Sesai (2018)',
            layers:  [ 4 ],
            opacity: 1,
          }),
          identify: function (error, featureCollection) {
            if (error || featureCollection.features.length === 0) {
              return false;
            } else {
              var attributes = featureCollection.features[0].properties;
              var content    = self.isamap.methods.htmlTable('dsei', {
                'dsei'    : attributes.dsei,
                'cod_dsei': attributes.cod_dsei,
              });

              return content;
            }
          },
          legend: [
            {
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(255, 170, 0, 255)",
                "width"            : "10px",
                "height"           : "10px",
              },
            },
          ],
        },

        polobase: {
          description: 'polobase',
          layer: L.esri.dynamicMapLayer({
            disableCache: false,
            format: 'png32',
            url:     covidServices,
            //token:   apiKey,
            attribution: 'Sesai (2018)',
            layers:  [ 3 ],
            opacity: 1,
          }),
          identify: function (error, featureCollection) {
            if (error || featureCollection.features.length === 0) {
              return false;
            } else {
              var attributes = featureCollection.features[0].properties;
              var content    = self.isamap.methods.htmlTable('polo', {
                'dsei'    : attributes.dsei,
                'polo'    : attributes.polo,
                'cod_dsei': attributes.cod_dsei,
                'cod_polo': attributes.cod_polo,
              });

              return content;
            }
          },
          legend: [
            {
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(0, 112, 255, 255)",
                "width"            : "10px",
                "height"           : "10px",
              },
            },
          ],
        },
      },
    },

    tis: {
      layers: {
        isolated: {
          description: 'isolated',
          layer: L.esri.featureLayer({
            url:          covidServices + '/9',
            //proxy:      self.isamap.settings.proxyUrl,
            //token:      apiKey,
            attribution:  'Funai',
            opacity:      0.45,
            pointToLayer: function (geojson, latlng) {
              return L.marker(latlng, {
                icon: L.icon({
                  iconUrl: self.isamap.settings.imagesUrl + '/ti/ti_feather.png',
                  iconSize: [18, 18],
                }),
              });
            },
          }),
          click: function(e) {
            var attributes = e.layer.feature.properties;
            var content    = self.isamap.methods.htmlTable('isolated', {
              'name'         : attributes.Nome,
              'status'       : attributes.Sit_dez201,
              'ti'           : attributes.TI,
              'state'        : attributes.UF,
              //'UC_Fed'     : attributes.UC_Fed,
              //'UC_Est'     : attributes.UC_Est,
              //'Infraestr'  : attributes.Infraestr,
              //'Infra_PCH'  : attributes.Infra_PCH,
              //'Infra_Line' : attributes.Infra_Line,
              //'Garimpo'    : attributes.Garimpo,
              'source'       : attributes.fonte,
            });

            e.layer.bindPopup(content);
            e.layer.openPopup();
          },
          legend: [
            {
              //label: 'caves',
              //html: '<img src="' + self.isamap.settings.imagesUrl + '/misc/caverna.png">',
              html: '',
              style: {
                "opacity"          : "0.45",
                "background-image" : "url('/images/ti/ti_feather.png')",
                "width"            : "12px",
                "height"           : "11px"
              },
            },
          ],
        },
      },
    },

    quilombos: {
      groupName: 'quilombola_localities',
      expanded: expanded,
      layers: {
        //quilombos: {
        //  description: 'quilombos_layer',
        //  layer: L.esri.dynamicMapLayer({
        //    disableCache: false,
        //    format: 'png32',
        //    url:     covidServices,
        //    attribution: 'INCRA (2020)',
        //    //token:   apiKey,
        //    layers:  [ 6 ],
        //    opacity: 1,
        //  }),
        //  identify: function (error, featureCollection) {
        //    if (error || featureCollection.features.length === 0) {
        //      return false;
        //    } else {
        //      var attributes = featureCollection.features[0].properties;
        //      var content    = self.isamap.methods.htmlTable('quilombo_area', {
        //        'cd_sr'        : attributes.cd_sr,
        //        'nr_process'   : attributes.nr_process,
        //        'name'         : attributes.nm_comunid,
        //        'municipality' : attributes.nm_municip,
        //        'state'        : attributes.cd_uf,
        //        'area_ha'      : attributes.nr_area_ha,
        //        'esfera'       : attributes.esfera,
        //        'phase'        : attributes.fase,
        //        'responsave'   : attributes.responsave,
        //      });

        //      return content;
        //    }
        //  },
        //  legend: [
        //    {
        //      html: '',
        //      style: {
        //        "opacity"          : "1",
        //        "background-color" : "rgb(197, 0, 255, 255)",
        //        "width"            : "10px",
        //        "height"           : "10px",
        //      },
        //    },
        //  ],
        //},

        //communities: {
        //  description: 'quilombo_communities',
        //  /*
        //  layer: L.esri.dynamicMapLayer({
        //    disableCache: false,
        //    format: 'png32',
        //    url:     covidServices,
        //    attribution: 'Fundação Palmares (2017)',
        //    //token:   apiKey,
        //    layers:  [ 7 ],
        //    opacity: 1,
        //  }),
        //  identify: function (error, featureCollection) {
        //    if (error || featureCollection.features.length === 0) {
        //      return false;
        //    } else {
        //      var attributes = featureCollection.features[0].properties;
        //      var content    = self.isamap.methods.htmlTable('quilombo_community', {
        //        'name'         : attributes.NM_COMUN,
        //        'municipality' : attributes.NM_MUNIC,
        //        'state'        : attributes.CD_UF,
        //        'source'       : attributes.FONTE,
        //        'year'         : attributes.ANO_ATUALI,
        //      });

        //      return content;
        //    }
        //  },
        //  */
        //  /*
        //  layer: L.esri.featureLayer({
        //    url:          covidServices + '/7',
        //    //proxy:      self.isamap.settings.proxyUrl,
        //    //token:      apiKey,
        //    attribution: 'Fundação Palmares (2017)',
        //    opacity:      1,
        //    pointToLayer: function (geojson, latlng) {
        //      return L.marker(latlng, {
        //        icon: L.icon({
        //          iconUrl: '/images/icons/fcp.png',
        //          iconSize: [18, 18],
        //        }),
        //      });
        //    },
        //  }),
        //  */
        //  layer: L.esri.Cluster.featureLayer({
        //    url:          covidServices + '/7',
        //    //proxy:        self.isamap.settings.proxyUrl,
        //    //token:        apiKey,
        //    attribution   : 'Fundação Palmares (2017)',
        //    opacity       : 0.8,
        //    polygonOptions: {
        //      color       : '#e57b08',
        //      weight      : 4,
        //      opacity     : 1,
        //      fillOpacity : 0.5
        //    },
        //    spiderfyOnMaxZoom: false,
        //    disableClusteringAtZoom: 9,
        //    iconCreateFunction: function (cluster) {
        //      var childCount = cluster.getChildCount();
        //      var c          = ' marker-cluster-';

        //      if (childCount < 10) {
        //        c += 'small';
        //      } else if (childCount < 100) {
        //        c += 'medium';
        //      } else {
        //        c += 'large';
        //      }

        //      return new L.DivIcon({
        //        html      : '<div><img class="quilombo-cluster" src="/images/icons/fcp.png"></img><span><strong>' + childCount + '</strong></span></div>',
        //        className : 'marker-cluster' + c,
        //        iconSize  : new L.Point(40, 40),
        //      });
        //    },
        //    pointToLayer: function (geojson, latlng) {
        //      return L.marker(latlng, {
        //        icon: L.icon({
        //          iconUrl: '/images/icons/fcp.png',
        //          iconSize: [18, 18],
        //        }),
        //      });
        //    },
        //  }),

        //  click: function(e) {
        //    var attributes = e.layer.feature.properties;
        //    var content    = self.isamap.methods.htmlTable('quilombo_community', {
        //      'name'         : attributes.NM_COMUN,
        //      'municipality' : attributes.NM_MUNIC,
        //      'state'        : attributes.CD_UF,
        //      'source'       : attributes.FONTE,
        //      'year'         : attributes.ANO_ATUALI,
        //    });

        //    e.layer.bindPopup(content);
        //    e.layer.openPopup();
        //  },
        //  legend: [
        //    {
        //      html: '',
        //      style: {
        //        "opacity"          : "1",
        //        "background-color" : "rgb(60, 0, 156, 255)",
        //        "width"            : "10px",
        //        "height"           : "10px",
        //        "border-radius"    : "50%",
        //      },
        //    },
        //  ],
        //},

        localities: {
          //description: 'quilombo_localities',
          description: 'quilombola_layer_by_municipality',
          layer: L.esri.dynamicMapLayer({
            disableCache: false,
            format: 'png32',
            url:     covidServices,
            attribution: 'IBGE (2019)',
            //token:   apiKey,
            layers:  [ 8 ],
            opacity: 1,
            position: 'back',
          }),
          add: function() {
            if (self.isamap != null) {
              self.isamap.methods.hideLayer('quilombos', 'localities_uf');
            }
          },
          identify: function (error, featureCollection) {
            if (error || featureCollection.features.length === 0) {
              return false;
            } else {
              var attributes = featureCollection.features[0].properties;
              var content    = self.isamap.methods.htmlTable('municipality', {
                'municipality'              : attributes.NM_MUN,
                'state'                     : attributes.NM_UF,
                'estimated_population_2019' : attributes.E_POP_2019,
              });

              var totalLocalities = 0;

              // Extra info
              if (self.quilombolas_locality_group != null && self.quilombolas_locality_group[attributes.CD_MUN] != undefined) {
                //content += '<h4>Localidades quilombolas</h4>';

                //var categories = {
                //  '<strong>Categoria</strong>': '<strong>Quantidade</strong>',
                //};

                for (var item in self.quilombolas_locality_group[attributes.CD_MUN]) {
                  //var category                  = self.quilombolas_locality_group[attributes.CD_MUN][item];
                  //categories[category.NM_CATEG] = category.localities;

                  //content     += category.NM_CATEG + ': ' + category.localities + '<br>';
                  /*
                  content      += self.isamap.methods.htmlTable('', {
                    'category'   : category.NM_CATEG,
                    'localities' : category.localities,
                  });
                  */

                  totalLocalities++
                }

                //content += self.isamap.methods.htmlTable('Localidades quilombolas (IBGE 2020)', categories);
                //content += '<br>Fonte: IBGE (2020)';
              }

              // Data and compilation needs review before enabling this again
              //content += self.$i18n.t('quilombola_localities') + ' (IBGE 2020): ' + totalLocalities + '<br>';

              if (self.totals != null && self.totals.cities[attributes.CD_MUN] != undefined) {
                var totalDeaths = self.totals.cities[attributes.CD_MUN].deaths;
              }
              else {
                var totalDeaths = 0;
              }

              content += self.$i18n.t('deaths') + ': ' + totalDeaths + '<br>';

              if (self.quilombolas_deaths_by_locality != null && self.quilombolas_deaths_by_locality[attributes.CD_MUN] != undefined) {
                var quilombolasDeaths = self.quilombolas_deaths_by_locality[attributes.CD_MUN].length;
              }
              else {
                var quilombolasDeaths = 0;
              }

              content += self.$i18n.t('quilombolas_deaths')  + ': ' + quilombolasDeaths + '<br>';

              return content;
            }
          },
          legend: [
            {
              label: '0',
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(255, 255, 128, 255)",
                "width"            : "10px",
                "height"           : "10px",
                "border-radius"    : "50%",
              },
            },
            {
              label: '1 - 10',
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(250, 209, 85, 255)",
                "width"            : "10px",
                "height"           : "10px",
                "border-radius"    : "50%",
              },
            },
            {
              label: '11 - 30',
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(242, 167, 46, 255)",
                "width"            : "10px",
                "height"           : "10px",
                "border-radius"    : "50%",
              },
            },
            {
              label: '31 - 45',
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(173, 83, 19, 255)",
                "width"            : "10px",
                "height"           : "10px",
                "border-radius"    : "50%",
              },
            },
            {
              label: '45+',
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(107, 0, 0, 255)",
                "width"            : "10px",
                "height"           : "10px",
                "border-radius"    : "50%",
              },
            },
          ],
        },

        localities_uf: {
          //description: 'quilombo_localities',
          description: 'quilombola_layer_by_state',
          layer: L.esri.dynamicMapLayer({
            disableCache: false,
            format: 'png32',
            url:     covidServices,
            attribution: 'IBGE (2019)',
            //token:   apiKey,
            layers:  [ 10 ],
            opacity: 1,
            position: 'back',
          }),
          add: function() {
            if (self.isamap != null) {
              self.isamap.methods.hideLayer('quilombos', 'localities');
            }
          },
          identify: function (error, featureCollection) {
            if (error || featureCollection.features.length === 0) {
              return false;
            } else {
              var attributes = featureCollection.features[0].properties;
              var content    = self.isamap.methods.htmlTable('state', {
                'state'                               : attributes.NM_UF,
                'estimated_population_2019'           : attributes.E_POP_2019,
                'total_municipalities_in_the_state'   : attributes.QTD_MUN,
                'total_municipalities_with_quilombos' : attributes.QTD_MUN_LOC_QUI,
                'quilombola_localities_2020'          : attributes.ELOC_QUI,
              });

              var totalLocalities = 0;

              // Extra info
              //if (self.quilombolas_locality_group != null && self.quilombolas_locality_group[attributes.CD_UF] != undefined) {
              //  //content += '<h4>Localidades quilombolas</h4>';

              //  //var categories = {
              //  //  '<strong>Categoria</strong>': '<strong>Quantidade</strong>',
              //  //};

              //  for (var item in self.quilombolas_locality_group[attributes.CD_UF]) {
              //    //var category                  = self.quilombolas_locality_group[attributes.CD_UF][item];
              //    //categories[category.NM_CATEG] = category.localities;

              //    //content     += category.NM_CATEG + ': ' + category.localities + '<br>';
              //    /*
              //    content      += self.isamap.methods.htmlTable('', {
              //      'category'   : category.NM_CATEG,
              //      'localities' : category.localities,
              //    });
              //    */

              //    totalLocalities++
              //  }

              //  //content += self.isamap.methods.htmlTable('Localidades quilombolas (IBGE 2020)', categories);
              //  //content += '<br>Fonte: IBGE (2020)';
              //}

              //content += self.$i18n.t('quilombola_localities') + ' (IBGE 2020): ' + totalLocalities + '<br>';

              if (self.totals != null && self.totals.states[attributes.CD_UF] != undefined) {
                var totalDeaths = self.totals.states[attributes.CD_UF].deaths;
              }
              else {
                var totalDeaths = 0;
              }

              content += self.$i18n.t('deaths') + ': ' + totalDeaths + '<br>';

              var quilombolasDeaths = 0;

              if (self.quilombolas_deaths_by_locality != null) {
                for (var item in self.quilombolas_deaths_by_locality) {
                  if (item.substr(0, 2) == attributes.CD_UF) {
                    quilombolasDeaths += self.quilombolas_deaths_by_locality[item].length;
                  }
                }
              }

              // Data currently not available
              content += self.$i18n.t('quilombolas_deaths')  + ': ' + quilombolasDeaths + '<br>';

              return content;
            }
          },
          legend: [
            {
              label: '0',
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(223, 184, 230, 255)",
                "width"            : "10px",
                "height"           : "10px",
                "border-radius"    : "50%",
              },
            },
            {
              label: '1 - 100',
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(199, 141, 235, 255)",
                "width"            : "10px",
                "height"           : "10px",
                "border-radius"    : "50%",
              },
            },
            {
              label: '101 - 200',
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(172, 102, 237, 255)",
                "width"            : "10px",
                "height"           : "10px",
                "border-radius"    : "50%",
              },
            },
            {
              label: '201 - 800',
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(145, 62, 240, 255)",
                "width"            : "10px",
                "height"           : "10px",
                "border-radius"    : "50%",
              },
            },
            {
              label: '800+',
              html: '',
              style: {
                "opacity"          : "1",
                "background-color" : "rgb(112, 12, 242, 255)",
                "width"            : "10px",
                "height"           : "10px",
                "border-radius"    : "50%",
              },
            },
          ],
        },
      },
    },

    //garimpo: {
    //  groupName: 'garimpo',
    //  expanded: false,
    //  layers: {
    //    pontosMineriaIlegal: {
    //      description: 'garimpo_points',
    //      layer: L.esri.Cluster.featureLayer({
    //        //layer: L.esri.featureLayer({
    //        url:          garimpoServer + '/11',
    //        opacity:      0.8,
    //        attribution: 'RAISG',
    //        polygonOptions: {
    //          color       : '#e57b08',
    //          weight      : 4,
    //          opacity     : 1,
    //          fillOpacity : 0.5
    //        },
    //        spiderfyOnMaxZoom: false,
    //        disableClusteringAtZoom: 8,
    //        iconCreateFunction: function (cluster) {
    //          var childCount = cluster.getChildCount();
    //          var c          = ' marker-cluster-';

    //          if (childCount < 10) {
    //            c += 'small';
    //          } else if (childCount < 100) {
    //            c += 'medium';
    //          } else {
    //            c += 'large';
    //          }

    //          return new L.DivIcon({ html: '<div><img src="' + garimpoImages + 'pontos/garimpo.png"> <span><strong>' + childCount + '</strong></span></div>', className: 'energy-cluster marker-cluster' + c, iconSize: new L.Point(40, 40) });
    //        },
    //        pointToLayer: function (geojson, latlng) {
    //          if (geojson.properties['leyendagarimpo'] == 'activo') {
    //            var icon = 'ativo.png';
    //          }
    //          else if (geojson.properties['leyendagarimpo'] == 'inactivo') {
    //            var icon = 'inativo.png';
    //          }
    //          else if (geojson.properties['leyendagarimpo'] == 'activo (local. aproximada)') {
    //            var icon = 'aproximado-inativo.png';
    //          }
    //          else if (geojson.properties['leyendagarimpo'] == 'inactivo (local. aproximada)') {
    //            var icon = 'aproximado-inativo.png';
    //          }
    //          else if (geojson.properties['leyendagarimpo'] == 's.i') {
    //            var icon = 'na.png';
    //          }
    //          else if (geojson.properties['leyendagarimpo'] == 's.i (local. aproximada)') {
    //            var icon = 'aproximado-na.png';
    //          }

    //          return L.marker(latlng, {
    //            icon: L.icon({
    //              iconUrl: garimpoImages + 'pontos/' + icon,
    //              iconSize: [20, 20],
    //            }),
    //          });
    //        },
    //      }),
    //      click: function(e) {
    //        var attributes = e.layer.feature.properties;
    //        var content    = self.isamap.methods.htmlTable('garimpo_points', {
    //          //'country'             : attributes.país,
    //          //'name'                : attributes.nombre,
    //          //'description'         : attributes.descripción,
    //          'extraction_method'     : attributes.metodoexplotacion,
    //          'substance'             : attributes.substanciaminerio,
    //          //'contaminants'        : attributes.contaminantes,
    //          //'Ator'                : attributes.ator,
    //          'status'                : attributes.situación,
    //          'date'                  : attributes.fecha_situación,
    //          'source'                : attributes.fuenteinformación,
    //          'observation'           : attributes.observacion,
    //          'Instituición RAISG'    : attributes.instituicionraisg,
    //          //'link'                : attributes.link,
    //          //'leyend'              : attributes.leyendagarimpo,
    //        });

    //        e.layer.bindPopup(content);
    //        e.layer.openPopup();
    //      },
    //      legend: [
    //        {
    //          label: 'Agrupamiento',
    //          html: '<img src="' + garimpoImages + 'pontos/garimpo.png">',
    //        },
    //        {
    //          label: 'Activo',
    //          html: '<img src="' + garimpoImages + 'pontos/ativo.png">',
    //        },
    //        {
    //          label: 'Inactivo',
    //          html: '<img src="' + garimpoImages + 'pontos/inativo.png">',
    //        },
    //        {
    //          label: 'Sin información',
    //          html: '<img src="' + garimpoImages + 'pontos/na.png">',
    //        },
    //        {
    //          label: 'Activo - localización aproximada',
    //          html: '<img src="' + garimpoImages + 'pontos/aproximado-ativo.png">',
    //        },
    //        {
    //          label: 'Inactivo - localización aproximada',
    //          html: '<img src="' + garimpoImages + 'pontos/aproximado-inativo.png">',
    //        },
    //        {
    //          label: 'Sin información - localización aproximada',
    //          html: '<img src="' + garimpoImages + 'pontos/aproximado-na.png">',
    //        },
    //      ],
    //    },
    //    //linhasMineriaIlegal: {
    //    //  description: 'Ríos con minería ilegal',
    //    //  layer: L.esri.dynamicMapLayer({
    //    //    url:     garimpoServer,
    //    //    layers:  [ 12 ],
    //    //    opacity: 0.50,
    //    //  }),
    //    //  identify: function (error, featureCollection) {
    //    //    if (error || featureCollection.features.length === 0) {
    //    //      return false;
    //    //    } else {
    //    //      var attributes = featureCollection.features[0].properties;
    //    //      /**
    //    //       * Handy way to build the attribute array
    //    //       */
    //    //      /*
    //    //        for (var attr in attributes) {
    //    //          console.debug("'" + attr + "' : attributes['" + attr + "'],");
    //    //        }
    //    //        */

    //    //      // Avoid results without basic info
    //    //      //if (attributes['Status'] == null) {
    //    //      //  return;
    //    //      //}

    //    //      var content = self.isamap.methods.htmlTable('Linha de Minería Ilegal', {
    //    //        'País'                  : attributes['País'],
    //    //        'Nombre'                : attributes['Nombre'],
    //    //        'Descripción'           : attributes['Descripción'],
    //    //        'Metodo de Explotación' : attributes['MetodoExplotacion'],
    //    //        'Substancia'            : attributes['SubstanciaMinerio'],
    //    //        'Contaminantes'         : attributes['Contaminantes'],
    //    //        'Ator'                  : attributes['Ator'],
    //    //        'Situación'             : attributes['Situación'],
    //    //        'Fecha_Situación'       : attributes['Fecha_Situación'],
    //    //        'Fuente de Información' : attributes['FuenteInformación'],
    //    //        'Observacion'           : attributes['Observacion'],
    //    //        'instituicion Raisg'    : attributes['Instituicion Raisg'],
    //    //        'Link'                  : attributes['Link'],
    //    //        'Leyenda'               : attributes['Leyenda'],
    //    //      });

    //    //      return content;
    //    //    }
    //    //  },
    //    //  legend: [
    //    //    {
    //    //      html: '',
    //    //      style: {
    //    //        "opacity"          : "0.5",
    //    //        "background-color" : "#ff00ff",
    //    //        "width"            : "15px",
    //    //        "height"           : "5px"
    //    //      },
    //    //    }
    //    //  ],
    //    //},
    //  },
    //},
  }
}
