import { messages } from 'AppConfig/app.messages.js';

export function popup(self) {
  return function(feature, complete) {
    var globals = self.methods.getGlobals();

    if (feature.properties.id_arp == undefined) {
      return '';
    }

    var id    = feature.properties.id_arp;
    var table = {};

    if (globals.arps.tis[id] != undefined) {
      // Target blank is used mainly for ifram compatibility
      var name = globals.arps.tis[id].categoria + ' ' + globals.arps.tis[id].nome_arp;
      var link = '<a href="https://ti.socioambiental.org/' + self.settings.lang + '/terras-indigenas/' + id + '" target="_blank">' + name + '</a>';
      var type = 'tis';

      // Peoples
      // Commented due to performance concerns.
      //var povos = [];
      //if (globals.arps.tis[id].povo != undefined && globals.arps.tis[id].povos_residentes == undefined) {
      //  var lang = self.settings.lang == 'pt-br' ? 'pt' : self.settings.lang;

      //  self.methods.debug(globals.arps.tis[id].povo);
      //  for (var povo in globals.arps.tis[id].povo) {
      //    var povo_nome = globals.arps.tis[id].povo[povo].povo;
      //    povos.push('<a href="http://pib.socioambiental.org/' + lang + '/Povo:' + povo_nome + '" target="_blank">' + povo_nome + '</a>');
      //  }

      //  globals.arps.tis[id].povos_residentes = povos.join(',');
      //}
    }
    else if (globals.arps.ucs[id] != undefined) {
      // Target blank is used mainly for ifram compatibility
      //var name = globals.arps.ucs[id].nome_completo; // API SisARP v0 field name
      var name = globals.arps.ucs[id].nome_arp;
      var link = '<a href="https://uc.socioambiental.org/' + self.settings.lang + '/arp/' + id + '" target="_blank">' + name + '</a>';
      var type = 'ucs';
    }
    else {
      self.methods.debug('Info not available for ARP ' + id);
      return self.methods.message('no_info');
    }

    // Get fields along with i18 labels
    for (var field in globals.arps[type][id]) {
      if (messages[self.settings.lang][type + '.marker.' + field] != undefined) {
        table[messages[self.settings.lang][type + '.marker.' + field]] = globals.arps[type][id][field];
      }
    }

    if (complete == true || complete == undefined) {
      return '<div class="isamap-identify isamap-identify-arp">' + self.methods.htmlTable(link, table) + '</div>';
    }
    else {
      return name;
    }
  }
}
