//import * as NProgress from 'nprogress';
//import 'nprogress/nprogress.css';

export function get() {
  var self = this;

  self.$store.dispatch('get', {
    url     : '/data/covid19/indigenas/yanomami.json',
    key     : 'yanomami',
    mutation: 'assign',
  })
}

export function add() {
  var self = this;

  if (Object.keys(self.yanomami).length === 0) {
    return
  }

  var data   = self.yanomami;
  var suffix = '';

  for (var item in data) {
    //self.yanomamiIndicators[data[item].id_arp] = Number(data[item].risk);
    self.yanomamiIndicators[data[item].cod_polo] = data[item].risk;
    suffix += '_' + data[item].cod_polo;
  }

  self.suffix = suffix;
  var limits  = 'limits' + self.suffix;

  var ids   = Object.keys(self.yanomamiIndicators);
  var id    = null;
  var where = '';

  // Build query conditions
  for (var i = 0; i < ids.length; i++) {
    id     = ids[i];
    where += 'cod_polo=' + id + ' OR ';
  }

  // Remove trailing 'OR' from query builder
  where = where.substring(0, where.length - 3);

  self.poloBaseLayer = L.esri.featureLayer({
    url:         'https://geo.socioambiental.org/webadaptor1/rest/services/covid/AnaliseCovid/MapServer/3',
    where:       where,
    attribution: 'Sesai (2018)',
    style: function(feature) {
      var cod_polo = feature.properties.cod_polo;
      var risk     = self.yanomamiIndicators[cod_polo];
      var style    = {
        color      : self.yanomamiRiskColor(risk),
        fillOpacity: 0.8,
        weight     : 3,
        opacity    : 1,
      };

      return style;
    },
  });

  self.poloBaseLayer.on('mouseover', function(e) {
    var properties = e.layer.feature.properties;
    var content    = '';
    var cod_polo   = properties.cod_polo;
    var risk       = Number(self.yanomamiIndicators[cod_polo]);
    var scale      = self.yanomamiRiskScale(risk) + 1; // CSS scale is 1-based

    jQuery('.covid19-yanomami-risk-scale').addClass('v-card--outlined').removeClass('elevation-24');
    jQuery('#covid19-yanomami-risk-scale-' + scale).removeClass('v-card--outlined').addClass('elevation-24');

    content += '<h4>';
    content += self.$i18n.t('polo') + ':</emph> ' + properties.polo + '<br />';
    content += '</h4>';

    content += '<emph>' + self.$i18n.t('risk_index') + ':</emph> ' + risk.toLocaleString(self.$i18n.locale) + '<br />';
    content += '<br />';

    // See https://stackoverflow.com/questions/20109336/leaflet-mouseout-called-on-mouseover-event#21456231
    e.layer.bindPopup(content, {'offset': L.point(0, -20)});
    e.layer.openPopup();
  });

  self.poloBaseLayer.on('mouseout', function(e) {
    jQuery('.covid19-yanomami-risk-scale').addClass('v-card--outlined').removeClass('elevation-24');
    //e.layer.closePopup();
    //e.layer.unbindPopup();
  });

  self.poloBaseLayer.addTo(self.isamap.map);

  // We're done
  //if (!self.covidCases) {
  //  NProgress.done();
  //}
}

export function scale(value) {
  // Determine scale based on value
  if (value < 0.840139) {
    return 0;
  }
  else if (value < 0.868644) {
    return 1;
  }
  else if (value < 0.892608) {
    return 2;
  }
  else {
    return 3;
  }
}

export function color(value) {
  // Determine color based on value
  if (value < 0.840139) {
    return "#ffdad1";
  }
  else if (value < 0.868644) {
    return "#fca487";
  }
  else if (value < 0.892608) {
    return "#eb362a";
  }
  else {
    return "#67000d";
  }
}
