import * as NProgress from 'nprogress';
import 'nprogress/nprogress.css';

export function get() {
  var self = this;

  self.$store.dispatch('get', {
    url     : '/data/covid19/indigenas/risks.json',
    key     : 'risks',
    mutation: 'assign',
  })

  // Get Indigenous Lands indexes
  //jQuery.ajax({
  //  url: '/data/covid19/risks.json',
  //  // Thanks https://stackoverflow.com/questions/22502943/jquery-ajax-progress-via-xhr#22504560
  //  xhr: function () {
  //    var xhr = new window.XMLHttpRequest();

  //    xhr.addEventListener("progress", function (evt) {
  //      if (evt.lengthComputable) {
  //        var percentComplete = evt.loaded / evt.total;

  //        // Only show progress up to 25%
  //        NProgress.set(percentComplete / 4);
  //      }
  //    }, false);

  //    return xhr;
  //  }
  //}).done(function(data) {
  //  self.addTis();
  //});
}

export function add() {
  var self = this;

  if (Object.keys(self.risks).length === 0) {
    return
  }

  var data   = self.risks;
  var suffix = '';

  for (var item in data) {
    //self.indicators[data[item].id_arp] = Number(data[item].risk);
    self.indicators[data[item].id_arp] = data[item].risk;
    suffix += '_' + data[item].id_arp;
  }

  self.suffix = suffix;
  var limits  = 'limits' + self.suffix;

  var ids = Object.keys(self.indicators);

  self.isamap.methods.addArpsById({
    type     : 'limits',
    arps     : ids,
    show     : true,
    fitBounds: true,
    // We're binding the update method only for limits
    // because they're slower and to avoid the callback
    // to be fired twice.
    load     : function(feature, layer) {
      self.update();
    },
    click: function(e) {
      var id_arp = e.layer.feature.properties.id_arp;
      //var url    = self.tiUrl(id_arp);
      //var win    = window.open(url, '_blank');
      //win.focus();
    },
    mouseover: function(e) {
      var id_arp = e.layer.feature.properties.id_arp;
      var risk   = self.indicators[id_arp];
      var scale  = self.riskScale(risk) + 1; // CSS scale is 1-based

      jQuery('.covid19-ti-risk-scale').addClass('v-card--outlined').removeClass('elevation-24');
      jQuery('#covid19-ti-risk-scale-' + scale).removeClass('v-card--outlined').addClass('elevation-24');

      self.popup(e);
    },
    mouseout: function(e) {
      jQuery('.covid19-ti-risk-scale').addClass('v-card--outlined').removeClass('elevation-24');
      //e.layer.closePopup();
      //e.layer.unbindPopup();
    },
  });

  // We're done
  //if (!self.covidCases) {
  //  NProgress.done();
  //}
}

export function update() {
  // Check if data is available
  if (this.suffix == '') {
    return;
  }

  var self   = this;
  var size   = Object.keys(self.indicators).length;
  var layers = self.isamap.methods.getLayers();

  // Block the UI while we're working
  //self.block(i18n.messages[i18n.locale].loading);

  // Set marker icon depending on indicator value
  //var markers = 'markers' + this.suffix;
  //layers.arps.layers[markers].layer.eachFeature(function(layer) {
  //  console.log(layer.feature.properties);
  //});

  var count  = 1;
  var limits = 'limits' + this.suffix;
  layers.arps.layers[limits].layer.eachFeature(function(layer) {
    var value = self.indicators[layer.feature.properties.id_arp];

    // Convert to a number
    value = Number(value.replace(',', '.'));

    // Determine color based on value
    var color = self.riskColors[self.riskScale(value)];

    layers.arps.layers[limits].layer.setFeatureStyle(layer.feature.id, {
      color  : color,
      weight : 3,
      opacity: 1,
    });

    //if (count++ >= size) {
    //  // Ensure the interface is unblocked
    //  self.unblock();

    //  // Enable again any year and indicator select box
    //  jQuery('select.select-year').attr('disabled', false);
    //  jQuery('select.indicator').attr('disabled', false);
    //}
    //else {
    //  // Show loading progress
    //  var percent = Math.round((count / size) * 100);
    //  self.block(i18n.messages[i18n.locale].loading + ' ' + percent + '%');
    //}
  });
}

export function popup(e) {
  var self     = this;
  var globals  = self.isamap.methods.getGlobals();
  var id_arp   = e.layer.feature.properties.id_arp;
  var content  = '';

  content += '<h4>';
  content += globals.arps.tis[id_arp].descricao_categoria + ' ';
  content += globals.arps.tis[id_arp].nome_arp;
  content += '</h4>';

  content += '<emph>' + self.$i18n.t('sesai')      + ':</emph> ' + globals.arps.tis[id_arp].sesai + '<br />';
  content += '<emph>' + self.$i18n.t('risk_index') + ':</emph> ' + parseFloat(self.indicators[id_arp]).toFixed(2);
  content += '<br />';
  content += '<a target="_blank" href="https://terrasindigenas.org.br/pt-br/terras-indigenas/' + id_arp + '">' + self.$i18n.t('ti_details') + '</a>';

  // See https://stackoverflow.com/questions/20109336/leaflet-mouseout-called-on-mouseover-event#21456231
  e.layer.bindPopup(content, {'offset': L.point(0, -20)});
  e.layer.openPopup();
}

export function scale(value) {
  // Determine scale based on value
  if (value < 0.1319) {
    return 0;
  }
  else if (value < 0.4024) {
    return 1;
  }
  else if (value < 0.4646) {
    return 2;
  }
  else if (value < 0.5848) {
    return 3;
  }
  else {
    return 4;
  }
}

export function color(value) {
  // Determine color based on value
  if (value < 0.1319) {
    return '#f6f685';
  }
  else if (value < 0.4024) {
    return '#f9b964';
  }
  else if (value < 0.4646) {
    return '#fb7b42';
  }
  else if (value < 0.5848) {
    return '#fd3d21';
  }
  else {
    return '#ff0000';
  }
}
