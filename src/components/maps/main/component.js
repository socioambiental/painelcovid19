// Requirements
import * as init            from './init.js'
import * as tis             from './tis.js'
import * as quilombolas     from './quilombolas.js'
import * as yanomami        from './yanomami.js'
import * as mapHandler      from 'IsaMap/map/instance.js'
//import L                  from 'IsaMap/map/leaflet.js'
//import { getQueryString } from 'IsaMap/common/url_query.js'

import layerComponent       from 'IsaMap/map/controls/layer/component.vue'

import * as NProgress from 'nprogress'
import 'nprogress/nprogress.css'

// Custom COVID19 code
export default {
  props: {
    fullsize: {
      type   : Boolean,
      default: false,
    },
    tiRisks: {
      type   : Boolean,
      default: false,
    },
    yanomamiRisks: {
      type   : Boolean,
      default: false,
    },
    quilomboCases: {
      type   : Boolean,
      default: false,
    },
    covidCases: {
      type   : Boolean,
      default: false,
    },
  },
  data: () => ({
    isamap     : null,

    // TI Risks
    indicators : {},
    suffix     : null,
    riskColors : [ '#f6f685', '#f9b964', '#fb7b42', '#fd3d21', '#ff0000', ],

    // Yanomami
    yanomamiIndicators : {},
    poloBaseLayer      : null,

    // Quilombolas
    quilombolas_markers: [],

    //date     : null,
    //dates    : {},
    //cluster  : null,
  }),
  mounted() {
    var self   = this

    // Load map metadata
    mapHandler.load('https://mapa.eco.br')

    jQuery(document).on('isaMapLoaded', function() {
      //NProgress.start()

      self.initMap()

      if (self.tiRisks) {
        self.getTis()
      }

      if (self.yanomamiRisks) {
        self.getYanomami()
      }

      if (self.covidCases) {
        // Tell everyone which is the cases risk map
        self.$root.$emit('cases-map', self.isamap);
      }

      if (self.quilomboCases) {
        self.getQuilombolas()
      }
    })
  },

  methods: {
    // Common
    buildParams       : init.params,
    initMap           : init.map,

    // TI Risks
    getTis            : tis.get,
    addTis            : tis.add,
    update            : tis.update,
    popup             : tis.popup,
    riskScale         : tis.scale,
    riskColor         : tis.color,

    // Yanomami
    getYanomami       : yanomami.get,
    addYanomami       : yanomami.add,
    yanomamiRiskScale : yanomami.scale,
    yanomamiRiskColor : yanomami.color,
    //update           : tis.update,
    //popup            : tis.popup,

    // Quilombolas
    getQuilombolas    : quilombolas.get,
    addQuilombolas    : quilombolas.add,
    quilombolasMarker : quilombolas.marker,
    quilombolasContent: quilombolas.content,
  },

  computed: {
    containerClass() {
      return this.fullsize ? 'isamap isamap-fullsize covid19-map' : 'isamap covid19-map'
    },

    risks() {
      if (this.$store.state.risks != undefined) {
        return this.$store.state.risks
      }

      return {}
    },

    yanomami() {
      if (this.$store.state.yanomami != undefined) {
        return this.$store.state.yanomami
      }

      return {}
    },

    quilombolas_cases() {
      if (this.$store.state.quilombolas_cases != undefined) {
        return this.$store.state.quilombolas_cases
      }

      return null
    },

    quilombolas_deaths_by_locality() {
      if (this.$store.state.quilombolas_deaths_by_locality != undefined) {
        return this.$store.state.quilombolas_deaths_by_locality
      }

      return null
    },

    quilombolas_locality_group() {
      if (this.$store.state.quilombolas_locality_group != undefined) {
        return this.$store.state.quilombolas_locality_group
      }

      return null
    },

    totals() {
      if (this.$store.state.totals != undefined) {
        return this.$store.state.totals
      }

      return {}
    },
  },

  watch: {
    risks() {
      this.addTis()
    },

    yanomami() {
      this.addYanomami()
    },

    quilombolas_cases() {
      this.addQuilombolas()
    },

    quilombolas_deaths_by_locality() {
      this.addQuilombolas()
    },
  },
}
