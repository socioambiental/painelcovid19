export function get() {
  var self = this;

  self.$store.dispatch('get', {
    url     : '/data/covid19/quilombolas/cases.json',
    key     : 'quilombolas_cases',
    mutation: 'assign',
  })

  self.$store.dispatch('get', {
    url     : '/data/covid19/quilombolas/deaths/by_locality.json',
    key     : 'quilombolas_deaths_by_locality',
    mutation: 'assign',
  })

  self.$store.dispatch('get', {
    url     : '/data/covid19/quilombolas/locality_group.json',
    key     : 'quilombolas_locality_group',
    mutation: 'assign',
  })

  self.$store.dispatch('get', {
    url     : '/data/covid19/totals.json',
    key     : 'totals',
    mutation: 'assign',
  })
}

export function add() {
  if (this.quilombolas_cases != null && this.quilombolas_deaths_by_locality != null) {
    if (this.quilombolas_markers.length == 0) {
      for (var item in this.quilombolas_deaths_by_locality) {
        var element = this.quilombolasMarker(this.quilombolas_deaths_by_locality[item]);

        element.addTo(this.isamap.map)
        this.quilombolas_markers.push(element)
      }
    }
  }
}

export function marker(data) {
  var markerContents = this.quilombolasContent(data)
  var location       = data[0].location
  var marker         = L.marker(
    [ location.latitude, location.longitude ],
    markerContents,
  )

  marker.bindPopup(markerContents.popup)
  marker.bindTooltip(markerContents.tooltip)

  return marker
}

export function content(data) {
  var tooltip  = this.$i18n.t('local') + ': ' + data[0].municipio + ' / ' + data[0].uf
  var content  = '<h3>' + this.$i18n.t('deaths') + '</h3>';
  var deaths   = this.$i18n.t('deaths') + ': ' + data.length

  content += tooltip
  content += '<br>' + deaths
  tooltip += '<br>' + deaths

  //for (var item in data) {
  //  content += this.isamap.methods.htmlTable(data[item].nome + ' (' + data[item].idade + ' anos)', {
  //    //"name"   : data[item].nome + '(' + data[item].idade + ' anos)',
  //    "quilombo" : data[item].quilombo,
  //    "death"    : data[item].dia + '/' + data[item].mes + '/' + data[item].ano,
  //    "source"   : data[item].fonte,
  //  })
  //}

  var icon = L.ExtraMarkers.icon({
    icon          : 'fa-heartbeat',
    markerColor   : 'orange-dark',
    shape         : 'square',
    prefix        : 'fa',
  })

  return {
    name   : name,
    //title: title,
    tooltip: tooltip,
    popup  : content,
    icon   : icon,
  }
}
