#!/usr/bin/env node
/**
 * Aggregate data into static files.
 */

// Requirements
const aggregateBrasilIO    = require('../backend/aggregate/brasil.io.js');
const aggregateTis         = require('../backend/aggregate/tis.js');
const aggregateQuilombolas = require('../backend/aggregate/quilombolas.js');
const aggregateSesai       = require('../backend/aggregate/sesai.js');
const aggregateApib        = require('../backend/aggregate/apib.js');
const aggregateYanomami    = require('../backend/aggregate/yanomami.js');

// Dispatch
//aggregateBrasilIO();
//aggregateTis();
//aggregateQuilombolas();
//aggregateYanomami();
aggregateBrasilIO();
aggregateSesai();
aggregateApib('full');
aggregateApib('partial');
