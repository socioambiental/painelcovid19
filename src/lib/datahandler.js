//import * as locality from 'Covid19Components/tools/locality.js';
const datetime = require('./datetime.js');

var sortSeries = function(series) {
  return series.sort(function(a, b) {
    return a[0] - b[0];
  });
}

module.exports = {
  sortSeries: sortSeries,

  buildDates: function(that) {
    // Iterate over all items to build available dates
    for (var item in that.data) {
      // Build dates object
      var date         = that.data[item].date;
      that.dates[date] = date;
    }

    // Show only past dates
    // Delete the current date as it might be incomplete
    var today = datetime.formatInternal(new Date());
    delete that.dates[today];

    // Set the current date
    //if (that.date != undefined) {
    //  that.date = params.date
    //}
    //else {
    // Use the previous day by default
    //var count = 0;
    //for (var date in that.dates) {
    //  that.date = date;
    //
    //  if (count++ == 1) {
    //    break;
    //  }
    //}
    ////}

    // Use the latest day in the dataset
    for (var date in that.dates) {
      that.date = date;
      break;
    }
  },

  //buildTimeSeries: function(that, messages, lang) {
  buildTimeSeries: function(that) {
    // Helper to build timeSeries for the whole country
    var timeSeriesCountry = {};
    var localities        = {};
    var today             = datetime.formatInternal(new Date());

    // Iterate over all items to build timeseries
    for (var item in that.data) {
      // Skip today's data
      if (that.data[item].date == today) {
        continue;
      }

      // Build localities object
      // Assume that items are already in descending date sort
      if (localities[that.data[item].city_ibge_code] == undefined) {
        localities[that.data[item].city_ibge_code] = {
          //name    : locality.name(item, that, messages, lang),
          ///item   : item,
          city      : that.data[item].city,
          place_type: that.data[item].place_type,
          state     : that.data[item].state,
          firstDate : that.data[item].date,
        };
      }
      else {
        localities[that.data[item].city_ibge_code].firstDate = that.data[item].date;
      }

      // Build timeSeries object
      if (that.timeSeries[that.data[item].city_ibge_code] == undefined) {
        that.timeSeries[that.data[item].city_ibge_code] = {
          'confirmed' : [],
          'deaths'    : [],
        };
      }

      // Format date
      var date = datetime.format(that.data[item].date);

      // Add confirmed cases into timeSeries
      that.timeSeries[that.data[item].city_ibge_code].confirmed.push([
        date,
        that.data[item].confirmed,
      ]);

      // Add deaths into timeSeries
      that.timeSeries[that.data[item].city_ibge_code].deaths.push([
        date,
        that.data[item].deaths,
      ]);

      if (timeSeriesCountry[date] == undefined) {
        timeSeriesCountry[date] = {
          confirmed : 0,
          deaths    : 0,
          cities    : 0,
          states    : 0,
        };
      }

      // Use municipality data for the country series
      //if (that.data[item].place_type == 'city') {
      //  timeSeriesCountry[date].confirmed += that.data[item].confirmed;
      //  timeSeriesCountry[date].deaths    += that.data[item].deaths;
      //}

      // Use state data for the country series
      if (that.data[item].place_type == 'state') {
        timeSeriesCountry[date].confirmed += that.data[item].confirmed;
        timeSeriesCountry[date].deaths    += that.data[item].deaths;
      }

      if (that.data[item].place_type == 'city') {
        timeSeriesCountry[date].cities++;
      }
      else {
        timeSeriesCountry[date].states++;
      }
    }

    // Update cities object at once
    that.localities = localities;

    // IBGE code "0" does not exist, but we're making a convention to represent the whole country
    that.timeSeries[0] = {
      confirmed: [],
      deaths   : [],
      cities   : [],
      states   : [],
    }

    // Build country timeSeries
    for (var date in timeSeriesCountry) {
      // We need to cast date back to number since it was previously an object key (string)
      that.timeSeries[0].confirmed.push([
        Number(date),
        timeSeriesCountry[date].confirmed,
      ]);

      that.timeSeries[0].deaths.push([
        Number(date),
        timeSeriesCountry[date].deaths,
      ]);

      that.timeSeries[0].cities.push([
        Number(date),
        timeSeriesCountry[date].cities,
      ]);

      that.timeSeries[0].states.push([
        Number(date),
        timeSeriesCountry[date].states,
      ]);
    }

    // Do that on graph side
    //that.timeSeries[0].confirmed = sortSeries(that.timeSeries.country.confirmed);
    //that.timeSeries[0].deaths    = sortSeries(that.timeSeries.country.deaths);
    //that.timeSeries[0].cities    = sortSeries(that.timeSeries.country.cities);
    //that.timeSeries[0].states    = sortSeries(that.timeSeries.country.states);
  },
}
