// Load Highcharts
var Highcharts = require('highcharts');

// Load the exporting module, and initialize it.
require('highcharts/modules/exporting')(Highcharts);
require('highcharts/modules/export-data')(Highcharts);

import { sortSeries } from 'Covid19Lib/datahandler.js';
import { chartLang  } from 'AppPlugins/highcharts-i18n/translate.js';

// Remove any previous graph
export function destroy() {
  if (this.chart != null) {
    this.chart.destroy();
    this.chart = null;
  }
}

// Build a chart
export function build(selector) {
  var self     = this;
  var locality = self.locality;

  if (selector == undefined) {
    selector = 'covid19-graph';
  }

  if (locality == 0) {
    var localityName = self.$i18n.t('brazil');
  }
  else {
    var localityName = self.localities[locality].name;
  }

  /*
   Highcharts.setOptions({
     global: {
       useUTC: false
     }
   });
   */

  // Chart locale config
  Highcharts.setOptions({
    lang: chartLang(self),
  });

  // Ensure any existing chart is destroyed
  self.destroyChart();

  var series = [
    {
      name  : self.$i18n.t('confirmed'),
      // If we sort a time series, we loose the indexing to select points at buildCluster
      //data: sortSeries(self.timeSeries[locality].confirmed),
      data  : self.timeSeries[locality].confirmed,
    },
    {
      name  : self.$i18n.t('deaths'),
      //data: sortSeries(self.timeSeries[locality].deaths),
      data  : self.timeSeries[locality].deaths,
      color : 'black',
    }
  ];

  if (locality == 0) {
    series.push(
      {
        name  : self.$i18n.t('states'),
        //data: sortSeries(self.timeSeries[locality].states),
        data  : self.timeSeries[locality].states,
      }
    );

    series.push(
      {
        name  : self.$i18n.t('municipalities'),
        //data: sortSeries(self.timeSeries[locality].cities),
        data  : self.timeSeries[locality].cities,
      }
    );
  }

  //var buttons = Highcharts.getOptions().exporting.buttons.contextButton.menuItems;

  // Graph inside the control
  self.chart = Highcharts.chart(selector, {
    // Graph inside the popup
    //var char = Highcharts.chart('covid19-popup-graph-' + item, {
    credits: {
      enabled: false,
    },
    chart: {
      type: 'line',
      zoomType: 'xy',

      // Chart spacing
      spacingBottom : 0,
      spacingTop    : 5,
      spacingLeft   : 0,
      spacingRight  : 0,
    },
    title: {
      //text: self.$i18n.t('locality') + ': ' + localityName,
      text: localityName,
    },
    //tooltip: {
    //  crosshairs: [true, false],
    //},
    xAxis: {
      type: 'datetime',
      //title: {
      //  text: self.$i18n.t('date'),
      //},
    },
    yAxis: {
      title: {
        text: self.$i18n.t('cases'),
      },
    },
    series: series,
    plotOptions: {
      series: {
        cursor: 'pointer',
        events: {
          click: function(event) {
            // Control slider using the graph
            if (self.currentChart == 0) {
              var item     = event.point.x;
              var date     = new Date(item);
              var value    = self.formatDateLabel(self.formatDateInternal(date), self.isamap.methods.getLocale());
              var instance = self.slider.instance;
              var index    = instance.options.values.indexOf(value);

              // Select the clicked point
              event.point.select(true, false);
              event.point.setState('select', true);

              // Update slider instance
              instance.update({
                from: index,
              });

              // Update slider value
              self.changeSingleSlider({
                from_value: value,
              });
            }
          },
        },
      },
    },
    //// See https://api.highcharts.com/highcharts/exporting
    ////     https://www.highcharts.com/docs/export-module/export-module-overview
    ////     https://stackoverflow.com/questions/25901869/highcharts-how-to-get-the-default-export-options-on-a-chart
    ////     https://stackoverflow.com/questions/23944451/custom-highcharts-context-menu-button-appearing-in-every-chart-on-page
    //exporting: {
    //  buttons: {
    //    contextButton: {
    //      menuItems: buttons,
    //    },
    //  },
    //},
  });

  self.currentChart = locality;
}
