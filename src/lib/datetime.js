module.exports = {
  // See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/UTC
  // Note that month goes from 0 to 11
  format: function(value) {
    var parsedDate = value.split('-');
    var date       = Date.UTC(parsedDate[0], parsedDate[1] - 1, parsedDate[2]);

    return date;
  },

  // Return date in the internal format
  formatInternal: function(date) {
    var day    = date.getDate();
    var month  = date.getMonth() + 1;
    var year   = date.getFullYear();
    day        = day < 10 ? '0' + day : day;
    month      = month < 10 ? '0' + month : month;

    return year + '-' + month + '-' + day;
  },

  formatLabel: function(value, locale) {
    var parsedDate = value.split('-');
    var date       = new Date(parsedDate[0], parsedDate[1] - 1, parsedDate[2]);
    //var label    = date.toLocaleString(this.isamap.methods.getLocale(), { dateStyle: 'short', timeStyle: 'short', }).replace(' 00:00:00', '');
    var label      = date.toLocaleString(locale, { dateStyle: 'short', timeStyle: 'short', }).replace(' 00:00:00', '');

    return label;
  },
}
