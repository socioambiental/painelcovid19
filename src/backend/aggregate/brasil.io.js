const output        = __dirname + '/../../../web/data/';
const fs            = require('fs');
const fetch         = require('node-fetch');
const config        = require('../../../config/api.js');
const buildUrl      = require('../../../vendor/mapa/src/backend/api/url.js');
const fsWriteError  = require('../../../vendor/mapa/src/backend/fs/error.js');

const geocoder      = require('../../plugins/geocoder/geocoder.js')

const datetime      = require('../../lib/datetime.js');
const formatDate    = datetime.formatInternal;
const datahandler   = require('../../lib/datahandler.js');

const geocodingFile = output + 'covid19/geocoding.json';

var indigenousPopulation = {};

// Chain procedure that gets data from Brasil.IO by traversing through all "next" entries
async function getNext(url, results) {
  var response = await fetch(url, {
                                    headers: {
                                      'Authorization': 'Token ' + config.brasilIoKey,
                                    },
                                  }).catch(err => console.error(err));;
  var data     = await response.json();

  for (var item in data.results) {
    console.log('Processing city ' + item + ' (' + data.results[item].city_ibge_code + ')...');

    data.results[item].location = await geocoder.get(
      data.results[item].city_ibge_code,
      data.results[item].city,
      data.results[item].state,
    );

    // Add information on indigenous population
    if (indigenousPopulation[data.results[item].city_ibge_code] != undefined) {
      data.results[item].indigenous_population = indigenousPopulation[data.results[item].city_ibge_code];
    }
    else {
      data.results[item].indigenous_population = null;
    }

    results.push(data.results[item]);
  }

  if (data.next != undefined && data.next != null) {
    await getNext(data.next, results);
  }

  return results;
}

// Retrieve Brasil.IO resultset
async function retrieveResults() {
  // Main dataset
  //var url         = 'https://brasil.io/api/dataset/covid19/caso/data?format=json';
  var url           = 'https://api.brasil.io/v1/dataset/covid19/caso/data';
  var urlIndigenous = 'https://api.socioambiental.org/static/covid19/indigenous/siasi/2020/populations_ibge_code.json'
  var results       = [];

  // Indigenous population
  //var indigenous         = JSON.parse(fs.readFileSync(output + 'covid19/indigenous.json', 'utf8'));
  var indigenousResource   = await fetch(urlIndigenous + '?apikey=' + config.apikey).catch(err => console.error(err));;
  var indigenous           = await indigenousResource.json();

  // Re-index indigenous population
  for (var item in indigenous) {
    indigenousPopulation[indigenous[item].cod_ibge] = indigenous[item].populacao;
  }

  geocoder.init(geocodingFile);

  // Begin recursion
  results = await getNext(url, results);

  return results;
}

// Aggregator for COVID-19
module.exports = async function () {
  var cases     = await retrieveResults();
  var totals    = {
    'brazil': {
      'confirmed': 0,
      'deaths'   : 0,
    },
    'states': {
    },
    'cities': {
    },
  };

  // Calculate yesterday's date
  var time = new Date();
  time.setDate(time.getDate() - 1);
  var yesterday = time.getTime();
  //var yesterday = formatDate(time);

  // Calculate current date
  var time  = new Date();
  var now = time.getTime();
  //var now = formatDate(time);

  // Build totals
  for (var item in cases) {
    // Totals based on yesterday date
    //var date = yesterday
    //if (cases[item].date == yesterday && cases[item].place_type == 'state') {

    // Totals based on is_last field
    var date = now;
    if (cases[item].is_last == true && cases[item].place_type == 'state') {
      totals.brazil.date       = date;
      totals.brazil.confirmed += cases[item].confirmed;
      totals.brazil.deaths    += cases[item].deaths;

      totals.states[cases[item].city_ibge_code] = cases[item];
    }
    else if (cases[item].is_last == true) {
      totals.cities[cases[item].city_ibge_code] = cases[item];
    }
  }

  // Write output
  fs.writeFile(output   + "covid19/cases.json",     JSON.stringify(cases             ), fsWriteError);
  //fs.writeFile(output + "covid19/cases.json",     JSON.stringify(results,   null, 2), fsWriteError);
  //fs.writeFile(output + "covid19/geocoding.json", JSON.stringify(geocoding, null, 2), fsWriteError);
  fs.writeFile(output   + "covid19/totals.json",    JSON.stringify(totals,    null, 2), fsWriteError);

  // Save geocoding data
  geocoder.save();

  // Now build dates, localities, and timeseries
  var payload = {
    data      : cases,
    dates     : {},
    date      : null,
    timeSeries: {},
    localities: {},
  };

  datahandler.buildDates(payload);
  datahandler.buildTimeSeries(payload);

  // Write dates, localities and timeseries to disk
  fs.writeFile(output + "covid19/last_full_date.json", JSON.stringify(payload.date,       null, 2), fsWriteError);
  fs.writeFile(output + "covid19/dates.json",          JSON.stringify(payload.dates,      null, 2), fsWriteError);
  fs.writeFile(output + "covid19/localities.json",     JSON.stringify(payload.localities, null, 2), fsWriteError);
  fs.writeFile(output + "covid19/timeseries.json",     JSON.stringify(payload.timeSeries, null, 2), fsWriteError);

  // Write a separate timeseries for each place
  for (var locality in payload.timeSeries) {
    fs.writeFile(output + "covid19/timeseries/locality/" + locality + ".json", JSON.stringify(payload.timeSeries[locality], null, 2), fsWriteError);
  }

  var dateCases = {};

  for (var item in cases) {
    var date = cases[item].date;
    var code = cases[item].city_ibge_code;

    if (dateCases[date] == undefined) {
      dateCases[date] = {};
    }

    dateCases[date][code] = cases[item];
  }

  // Write separate case files for each date
  for (var date in dateCases) {
    fs.writeFile(output + "covid19/cases/date/" + date + ".json", JSON.stringify(dateCases[date], null, 2), fsWriteError);
  }
}
