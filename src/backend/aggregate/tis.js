const output       = __dirname + '/../../../web/data/';
const fs           = require('fs');
const fetch        = require('node-fetch');
const config       = require('../../../config/api.js');
const buildUrl     = require('../../../vendor/mapa/src/backend/api/url.js');
const fsWriteError = require('../../../vendor/mapa/src/backend/fs/error.js');

// Aggregator for COVID-19
module.exports = async function () {
  // Build TIs file
  //var risksFile = output + 'covid19/risks.json';
  var tis       = [];
  var time      = new Date();
  var now       = time.getTime();

  try {
    //if (fs.existsSync(risksFile)) {
      //var url    = buildUrl('sisarp/v2/ti');
      var url      = 'https://mapa.eco.br/data/sisarp/v2/tis.json';
      var response = await fetch(url).catch(err => console.error(err));;
      var info     = await response.json();

      // Risks file
      //var risks    = JSON.parse(fs.readFileSync(risksFile, 'utf8'));
      var urlRisks = 'https://api.socioambiental.org/static/covid19/indigenous/risks.json'
      var risksResource   = await fetch(urlRisks + '?apikey=' + config.apikey).catch(err => console.error(err));;
      var risks           = await risksResource.json();

      for (var item in risks) {
        var id_arp = risks[item].id_arp;
        var risk   = risks[item].risk;

        if (info.tis[id_arp] != undefined) {
          var ti = info.tis[id_arp];
        }
        else {
          var ti = {};
        }

        ti.risco_covid19      = risk;
        ti.ultima_atualizacao = now;
        tis.push(ti);

        function riskSort(a, b) {
          return b.risco_covid19 - a.risco_covid19;
        }

        tis = tis.sort(riskSort);
      }
    //}
  } catch(err) {
    var tis = [];
  }

  // Write output
  fs.writeFile(output + "covid19/indigenas/tis.json",   JSON.stringify(tis,   null, 2), fsWriteError);
  fs.writeFile(output + "covid19/indigenas/risks.json", JSON.stringify(risks, null, 2), fsWriteError);
}
