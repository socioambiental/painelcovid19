const output        = __dirname + '/../../../web/data/';
const fs            = require('fs');
const fetch         = require('node-fetch');
const config        = require('../../../config/api.js');
const fsWriteError  = require('../../../vendor/mapa/src/backend/fs/error.js');

const geocoder      = require('../../plugins/geocoder/geocoder.js')
const ibge          = require('../../plugins/ibge/ibge.js')

const parse         = require('csv-parse/lib')

const deathsCsv     = config.conaqCasesCsv;
const casesCsv      = config.conaqDeathsCsv;

const geocodingFile = output + 'covid19/geocoding.json';

var sortSeries = function(series) {
  return series.sort(function(a, b) {
    return b[1] - a[1];
  });
}

async function getData(headers, url, file) {
  var response    = await fetch(url).catch(err => console.error(err));;
  var data        = await response.text();
  var results     = [ ];
  var table       = [ ];
  var hasLocation = false;
  var byLocality  = { };

  parse(data)
    .on('data', (data) => results.push(data))
    .on('end', async () => {
      var time = new Date();
      var now  = time.getTime();

      for (var result in results) {
        var col = {};

        for (var item in headers) {
          col[headers[item]] = results[result][item];
        }

        col.ultima_atualizacao = now;

        table.push(col);
      }

      // Discard the table header
      table.shift();

      for (var item in table) {
        // Now add locality data
        if (table[item].codigo_ibge != '') {
          var locality = ibge.locality(table[item].codigo_ibge);

          if (locality != null) {
            hasLocation          = true;
            table[item].location = await geocoder.get(
              table[item].codigo_ibge,
              locality['Nome_Município'],
              locality['Nome_UF'],
            );
          }
          //else {
          //  table[item].location = {};
          //}
        }

        // Delete names
        if (table[item].nome != undefined) {
          delete table[item].nome;
        }
      }

      fs.writeFile(output + "covid19/quilombolas/" + file + ".json", JSON.stringify(table, null, 2), fsWriteError);

      if (hasLocation) {
        for (var item in table) {
          var ibge_code = table[item].codigo_ibge;

          if (ibge_code == null || ibge_code == '' || table[item].location == undefined) {
            continue;
          }

          if (byLocality[ibge_code] == undefined) {
            byLocality[ibge_code] = [];
          }

          byLocality[ibge_code].push(table[item]);
        }

        fs.writeFile(output + "covid19/quilombolas/" + file + "/by_locality.json", JSON.stringify(byLocality, null, 2), fsWriteError);
      }
    });
}

async function buildLocalities() {
  // Localities
  var urlLocalities      = 'https://api.socioambiental.org/static/quilombos/localities.json'
  var localitiesResource = await fetch(urlLocalities + '?apikey=' + config.apikey).catch(err => console.error(err));;
  var localities         = await localitiesResource.json();

  var localityCount = {
    'state': {},
    'city' : {},
  }

  var localityFreq = {
    'state': {
      0: 0,
      1: 0,
      2: 0,
      3: 0,
      4: 0,
    },
    'city' : {
      0: 0,
      1: 0,
      2: 0,
      3: 0,
      4: 0,
    },
  }

  var localityGroup = {}
  var places        = {
    state: {},
    city : {},
  }

  for (var locality in localities) {
    var place = localities[locality];
    var state = place.CD_UF;
    var city  = place.CD_MUN;

    // Ensure we have a populated list of states and cities
    places.state[state] = place.sigla_UF;
    places.city[city]   = place.NM_MUN;

    if (localityCount.state[state] == undefined) {
      localityCount.state[state] = 0;
    }

    if (localityCount.city[city] == undefined) {
      localityCount.city[city] = 0;
    }

    //if (localityCount.state[place.sigla_UF] == undefined) {
    //  localityCount.state[place.sigla_UF] = 0;
    //}

    //if (localityCount.city[place.NM_MUN] == undefined) {
    //  localityCount.city[place.NM_MUN] = 0;
    //}

    if (localityGroup[city] == undefined) {
      localityGroup[city] = {};
    }

    // Compile a list of localities by city and by category
    if (localityGroup[city][place.CD_CATEG] == undefined) {
      localityGroup[city][place.CD_CATEG]            = place;
      localityGroup[city][place.CD_CATEG].localities = 0;

      // Do not use specific locality info
      delete localityGroup[city].CD_LOCALIDADE;
      delete localityGroup[city].NM_LOCALIDADE;
    }

    //localityCount.state[place.sigla_UF]++;
    //localityCount.city[place.NM_MUN]++;
    localityCount.state[state]++;
    localityCount.city[city]++;
    localityGroup[city][place.CD_CATEG].localities++;
  }

  // State frequencies
  for (var item in localityCount.state) {
    var value = localityCount.state[item];

    if (value == 0) {
      localityFreq.state[0]++;
    }
    else if (value <= 20) {
      localityFreq.state[1]++;
    }
    else if (value <= 100) {
      localityFreq.state[2]++;
    }
    else if (value <= 500) {
      localityFreq.state[3]++;
    }
    else {
      localityFreq.state[4]++;
    }
  }

  // City frequencies
  for (var item in localityCount.city) {
    var value = localityCount.city[item];

    if (value == 0) {
      localityFreq.city[0]++;
    }
    else if (value <= 10) {
      localityFreq.city[1]++;
    }
    else if (value <= 30) {
      localityFreq.city[2]++;
    }
    else if (value <= 45) {
      localityFreq.city[3]++;
    }
    else {
      localityFreq.city[4]++;
    }
  }

  var localitySeries = {
    state: [],
    city : [],
  }

  // Build locality data series
  //for (var type in places) {
  //  for (var item in localityCount[type]) {
  //    localitySeries[type].push([
  //      places[type][item],
  //      localityCount[type][item],
  //    ]);
  //  }
  //}

  // Build state data series
  for (var item in localityCount.state) {
    localitySeries.state.push([
      places.state[item],
      localityCount.state[item],
    ]);
  }

  // Build item data series
  for (var item in localityFreq.city) {
    localitySeries.city.push([
      item,
      localityFreq.city[item],
    ]);
  }

  // Sort
  localitySeries.state = sortSeries(localitySeries.state);
  localitySeries.city  = sortSeries(localitySeries.city);

  fs.writeFile(output + "covid19/quilombolas/localities.json",            JSON.stringify(localities,     null, 2), fsWriteError);
  fs.writeFile(output + "covid19/quilombolas/locality_count.json",        JSON.stringify(localityCount,  null, 2), fsWriteError);
  fs.writeFile(output + "covid19/quilombolas/locality_series.json",       JSON.stringify(localitySeries, null, 2), fsWriteError);
  fs.writeFile(output + "covid19/quilombolas/locality_group.json",        JSON.stringify(localityGroup,  null, 2), fsWriteError);
  fs.writeFile(output + "covid19/quilombolas/locality_frequencies.json",  JSON.stringify(localityFreq,   null, 2), fsWriteError);
}

module.exports = async function () {
  var deathsHeaders = ['nome', 'idade', 'quilombo', 'cd_quilombo', 'processo_incra', 'cd_geo', 'municipio', 'uf', 'codigo_ibge', 'dia', 'mes', 'ano', 'fonte', 'observacao'];
  var casesHeaders  = ['monitorados', 'confirmados', 'óbitos', 'dia', 'mes', 'ano', 'fonte', 'referencia' ];

  await ibge.init();
  geocoder.init(geocodingFile);

  getData(deathsHeaders, deathsCsv, 'deaths');
  getData(casesHeaders,  casesCsv,  'cases');

  buildLocalities();

  geocoder.save();
}
