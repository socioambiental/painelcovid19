const output       = __dirname + '/../../../web/data/';
const fs           = require('fs');
const fetch        = require('node-fetch');
const config       = require('../../../config/api.js');
const buildUrl     = require('../../../vendor/mapa/src/backend/api/url.js');
const fsWriteError = require('../../../vendor/mapa/src/backend/fs/error.js');

// Aggregator for COVID-19
module.exports = async function () {
  try {
      var url      = 'https://mapa.eco.br/data/sisarp/v2/tis.json';
      var response = await fetch(url).catch(err => console.error(err));;
      var info     = await response.json();

      // Risks file
      var urlRisks      = 'https://api.socioambiental.org/static/covid19/indigenous/yanomami.json'
      var risksResource = await fetch(urlRisks + '?apikey=' + config.apikey).catch(err => console.error(err));;
      var risks         = await risksResource.json();
  } catch(err) {
    var tis = [];
  }

  // Write output
  fs.writeFile(output + "covid19/indigenas/yanomami.json", JSON.stringify(risks, null, 2), fsWriteError);
}
