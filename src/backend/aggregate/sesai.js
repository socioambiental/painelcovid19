const output       = __dirname + '/../../../web/data/';
const fs           = require('fs');
const fetch        = require('node-fetch');
const config       = require('../../../config/api.js');
const buildUrl     = require('../../../vendor/mapa/src/backend/api/url.js');
const fsWriteError = require('../../../vendor/mapa/src/backend/fs/error.js');
const path         = require('path')

const compiledPath = output + 'covid19/indigenas/sesai/compiled/'
const folderPath   = output + 'covid19/indigenas/sesai/raw/'

//const ExponentialRegression = require('ml-regression-exponential');
const regression = require('regression');

const SIRjs      = require('sir.js')

// Aggregate COVID-19 data from Sesai
module.exports = async function() {
  function processItem(item) {
    var year  = item[0]
    var month = item[1]
    var day   = item[2]
    var hour  = item[3]

    // Ignore already processed days
    if (aggregated[year] != undefined && aggregated[year][month] != undefined && aggregated[year][month][day] != undefined) {
      return
    }

    var date = new Date()

    // Ignore the current date: we're only interested on previous data
    // You might want to check for current hour if running this script hourly, as new data everyday after 20:00
    //if (date.getDate() == day && date.getMonth() == month - 1 && date.getFullYear() == year && date.getHours() < 21) {
    if (date.getDate() == day && date.getMonth() == month - 1 && date.getFullYear() == year) {
      return
    }

    if (latest.length == 0) {
      latest = item
    }

    try {
      var fileContents = JSON.parse(fs.readFileSync(folderPath + item[4] + '.json', 'utf8'));
    } catch(err) {
      return
    }

    var desc    = fileContents[0]
    var score   = JSON.parse(fileContents[1])
    var details = JSON.parse(fileContents[2])

    if (aggregated[year] == undefined) {
      aggregated[year] = {}
    }

    if (aggregated[year][month] == undefined) {
      aggregated[year][month] = {}
    }

    // Add labels into totals
    totals = {
      suspected : score[0],
      confirmed : score[1],
      discarded : score[2],
      infected  : score[3],
      cured     : score[4],
      deaths    : score[5],
    }

    var detailsLabeled = []

    // Add labels into details
    for (var item in details) {
      // Table model until web/data/covid19/indigenas/sesai/raw/2020-08-17-111702.json
      if (Number(''.concat(year, month, day, hour)) <= 20200817111702) {
        var detail = {
          dsei      : details[item][0],
          suspected : details[item][1],
          confirmed : details[item][2],
          discarded : details[item][3],
          infected  : details[item][4],
          cured     : details[item][5],
          deaths    : details[item][6],
          population: dseiPopulations[details[item][0]],
        }
      }
      // Table model from web/data/covid19/indigenas/sesai/raw/2020-08-17-121702.json onwards
      else {
        var detail = {
          dsei      : details[item][1],
          suspected : details[item][2],
          confirmed : details[item][3],
          discarded : details[item][4],
          infected  : details[item][5],
          cured     : details[item][6],
          deaths    : details[item][7],
          population: dseiPopulations[details[item][1]],
        }
      }

      detailsLabeled.push(detail)
    }

    // Use only the first item as fileList is already sorted
    if (aggregated[year][month][day] == undefined) {
      // Aggregated data
      aggregated[year][month][day] = {
        desc    : desc,
        totals  : totals,
        details : detailsLabeled,
      }

      function getStatuses() {
        return {
          suspected : [],
          confirmed : [],
          discarded : [],
          infected  : [],
          cured     : [],
          deaths    : [],
        }
      }

      // Use the same day as the data set
      var date = parseDate(year, month, day)

      // Use the day before in the dataset
      //var date = parsePreviousDate(year, month, day)

      // Useful to build the CSV version of the timeseries
      if (dates.indexOf(date) === -1) {
        dates.push(date)
      }

      // Convert date to a timestamp
      date = date.getTime();

      // Ensure the global timeseries exists
      if (timeSeries.BRASIL == undefined) {
        timeSeries.BRASIL = getStatuses()
      }

      // Populate global timeseries
      for (var status in getStatuses()) {
        timeSeries.BRASIL[status].push([
          date,
          totals[status] != null ? Number(totals[status]) : Number(0),
        ])
      }

      // Time series for each DSEI
      for (var item in detailsLabeled) {
        var dsei = detailsLabeled[item].dsei

        // Workaround to avoiding including the table header
        if (dsei == 'Dsei') {
          continue
        }

        if (timeSeries[dsei] == undefined) {
          timeSeries[dsei] = getStatuses()
        }

        for (var status in getStatuses()) {
          timeSeries[dsei][status].push([
            date,
            detailsLabeled[item][status] != null ? Number(detailsLabeled[item][status]) : Number(0),
          ])
        }
      }

      // Build DSEIs list if already not built
      if (dseis.length == 0) {
        dseis.push('BRASIL')

        for (var item in detailsLabeled) {
          dseis.push(detailsLabeled[item].dsei)
        }
      }
    }
    else {
      return
    }
  }

  async function fetchSesai() {
    var urlSesai      = 'https://api.socioambiental.org/sisarp/v2/sesai'
    var sesaiResource = await fetch(urlSesai + '?apikey=' + config.apikey).catch(err => console.error(err));;
    var sesai         = await sesaiResource.json();
    var data          = {}

    // Add normalized name
    for (var item in sesai.data) {
      var dsei   = sesai.data[item].nome.replace(/ \(.*\)$/, '').toUpperCase()
      data[dsei] = sesai.data[item]
    }

    return data
  }

  async function fetchDseiPopulations() {
    var urlDseiPopulations = 'https://api.socioambiental.org/static/covid19/indigenous/siasi/2020/populations_dsei.json'
    var indigenousResource = await fetch(urlDseiPopulations + '?apikey=' + config.apikey).catch(err => console.error(err))
    var response           = await indigenousResource.json()

    var results = {
      BRASIL: 0,
    }

    for (var result in response) {
      results[response[result].name]  = Number(response[result].population)
      results.BRASIL                 += Number(response[result].population)
    }

    return results
  }

  var folderContents  = fs.readdirSync(folderPath)
  var fileList        = []
  var aggregated      = {}
  var timeSeries      = {}
  var dates           = []
  var dseis           = []
  var latest          = []
  var sesai           = await fetchSesai()
  var dseiPopulations = await fetchDseiPopulations()

  for (var file in folderContents) {
    var fileBase = folderContents[file].replace(/.json$/, '')
    var fileInfo = fileBase.split('-')

    // Populate filelist
    fileInfo.push(fileBase)
    fileList.push(fileInfo)
  }

  // Sort filelist, descending, so latest data comes first
  fileList = fileList.sort(function(a, b) {
    var first  = a[0] + a[1] + a[2] + a[3]
    var second = b[0] + b[1] + b[2] + b[3]

    return Number(second) - Number(first)
  })

  // Populate data
  for (var item in fileList) {
    processItem(fileList[item])
  }

  function sortSeries(series) {
    return series.sort(function(a, b) {
      return a[0] - b[0];
    });
  }

  // Exponential regression
  for (var place in timeSeries) {
    for (var type in timeSeries[place]) {
      // Copy a sorted series
      var series = sortSeries(timeSeries[place][type]);

      // Remove all zeroed y-values so we can fit an exponential
      for (var item in series) {
        if (series[item][1] == 0) {
          series.splice(item, 1);
        }
      }

      const daysAhead  = 20;
      const secondsDay = 86400000;
      const offset     = series[0][0]

      // Build the series with normalized x-axis values
      //
      // Remove the initial UNIX timestamp (series[0][0]) and divide by secondsDay
      // so curve fitting does not yeilds to NaN due to large x-values
      var series = series.map(function (item) {
        return [ (item[0] - series[0][0]) / secondsDay, item[1] ];
      });

      // Build an array with the normalized x-values
      var x = series.map(function (item) {
        return item[0];
      });

      // Build an array with y-values
      //var y = series.map(function (item) {
      //  return item[1];
      //});

      // Adjust using ml-regression-exponential
      //var adjust = new ExponentialRegression(x, y);

      // Adjust using regression-js
      var adjust = regression.exponential(series, { precision: 2 });

      // Debug
      //if (type == 'confirmed') {
      //  console.log(series)
      //  console.log(place)
      //  console.log(adjust.string)
      //  console.log(adjust.equation)
      //  //break;
      //}

      // Ignore unsucessful regressions
      if (isNaN(adjust.equation[0]) || isNaN(adjust.equation[1])) {
        continue;
      }

      timeSeries[place][type + '_prediction_exponential'] = [];

      var last = x.length - 1;

      // Expand the prediction for some more days
      for (var i = 1; i <= daysAhead; i++) {
        x.push(x[last] + i);
      }

      for (var item in x) {
        //timeSeries[place][type + '_prediction_exponential'].push(adjust.predict(x[item]));
        //timeSeries[place][type + '_prediction_exponential'].push([
        //  (item + offset) * secondsDay,
        //  adjust.predict(item),
        //]);

        var predicted = adjust.predict(x[item]);

        timeSeries[place][type + '_prediction_exponential'].push([
          (predicted[0] * secondsDay) + offset,
          predicted[1],
        ]);
      }
    }
  }

  var sir = {}

  // SIR
  for (var place in timeSeries) {
    var infectedSeries  = sortSeries(timeSeries[place]['confirmed']);
    var recoveredSeries = sortSeries(timeSeries[place]['cured']);
    var I               = 0
    var t0              = 0
    var timeOffset      = 0
    const iterations    = 20
    const beta          = 0.8
    const gamma         = 0.4
    const secondsDay    = 86400000;

    // Find the first reported value
    for (var item in infectedSeries) {
      if (Number(infectedSeries[item][1]) != 0) {
        timeOffset = Number(infectedSeries[item][0])
        I          = Number(infectedSeries[item][1])
        t0         = item

        break
      }
    }

    // Do not run if there's no data
    if (timeOffset == 0) {
      continue
    }

    // Get recovered, susceptible values and then solve
    var R        = recoveredSeries[t0] != undefined ? Number(recoveredSeries[t0][1]) : 0
    var S        = Number(dseiPopulations[place]) - R
    var solution = SIRjs.solve({ S0: S, I0: I, R0: R, t: 1, N: iterations, beta: beta, gamma: gamma });

    //sir[place] = {
    //  'susceptible': [],
    //  'infected'   : [],
    //  'recovered'  : [],
    //}
    timeSeries[place].sir_model_susceptible = []
    timeSeries[place].sir_model_infected    = []
    timeSeries[place].sir_model_recovered   = []

    // Build SIR series
    for (var item in solution) {
      //sir[place]['susceptible'].push([ solution[item].S, item * secondsDay + timeOffset ])
      //sir[place]['infected'].push(   [ solution[item].I, item * secondsDay + timeOffset ])
      //sir[place]['recovered'].push(  [ solution[item].R, item * secondsDay + timeOffset ])
      var timestamp = item * secondsDay + timeOffset

      timeSeries[place]['sir_model_susceptible'].push([ timestamp, solution[item].S ])
      timeSeries[place]['sir_model_infected'].push(   [ timestamp, solution[item].I ])
      timeSeries[place]['sir_model_recovered'].push(  [ timestamp, solution[item].R ])
    }
  }

  // Write output
  fs.writeFile(compiledPath + 'dseis.json',      JSON.stringify(dseis,      null, 2), fsWriteError);
  fs.writeFile(compiledPath + 'aggregated.json', JSON.stringify(aggregated, null, 2), fsWriteError);
  fs.writeFile(compiledPath + 'timeseries.json', JSON.stringify(timeSeries, null, 2), fsWriteError);
  //fs.writeFile(compiledPath + 'sir.json',      JSON.stringify(sir,        null, 2), fsWriteError);

  // Write a totals file only with the latest data
  var date                                         = new Date();
  aggregated[latest[0]][latest[1]][latest[2]].date = date.getTime();
  fs.writeFile(compiledPath + 'totals.json', JSON.stringify(aggregated[latest[0]][latest[1]][latest[2]], null, 2), fsWriteError);

  // Write a file for each series
  for (var dsei in timeSeries) {
    fs.writeFile(compiledPath + 'timeseries/dsei/' + dsei + '.json', JSON.stringify(timeSeries[dsei], null, 2), fsWriteError);
  }

  // Write a file for each SIR series
  //for (var dsei in sir) {
  //  fs.writeFile(compiledPath + 'sir/dsei/' + dsei + '.json', JSON.stringify(sir[dsei], null, 2), fsWriteError);
  //}

  // Parse current date
  function parseDate(year, month, day) {
    var date = new Date(Number(year), Number(month) - 1, Number(day))

    return date;
  }

  // Parse a date, returning the previous day
  // Use the day before in the dataset, as it always point to the data from the previous day
  function parsePreviousDate(year, month, day) {
    var date = new Date(Number(year), Number(month) - 1, Number(day))
    date.setDate(date.getDate() - 1);

    return date;
  }

  // Return a date in the CSV format
  function csvDate(date) {
    var day   = date.getDate()
    var month = date.getMonth() + 1
    var year  = date.getFullYear().toString().substr(-2);

    return ',' + month + '/' + day + '/' + year
  }

  // Convert time series to CSV
  function buildCsv(type) {
    var csv = [ ]
    //csv[0]  = 'DSEI'
    csv[0]  = '"Province/State","Country/Region","Lag","Long"'

    // Clone dates, otherwise we might be reversing the same data multiple times
    var times = dates.slice(0)

    // CSV dates as table cols
    for (var date in times.reverse()) {
      csv[0] += csvDate(times[date])
    }

    for (var dsei in timeSeries) {
      var row = '"",' + '"' + dsei + '",' + '"", ""'

      for (var item in timeSeries[dsei][type].reverse()) {
        row += ',' + timeSeries[dsei][type][item][1]
      }

      csv.push(row)
    }

    return csv.join('\r\n')
  }

  // Confirmed timeseries in the CSV format
  var timeSeriesConfirmedCsv = buildCsv('confirmed')
  fs.writeFile(compiledPath + 'timeseries/confirmed/timeseries.csv', timeSeriesConfirmedCsv, fsWriteError);

  // Deaths timeseries in the CSV format
  var timeSeriesDeathsCsv = buildCsv('deaths')
  fs.writeFile(compiledPath + 'timeseries/deaths/timeseries.csv', timeSeriesDeathsCsv, fsWriteError);
}
