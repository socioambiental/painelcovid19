const output       = __dirname + '/../../../web/data/';
const fs           = require('fs');
const fetch        = require('node-fetch');
const config       = require('../../../config/api.js');
const buildUrl     = require('../../../vendor/mapa/src/backend/api/url.js');
const fsWriteError = require('../../../vendor/mapa/src/backend/fs/error.js');

const compiledPath = output + 'covid19/indigenas/apib/'

const { GoogleSpreadsheet } = require('google-spreadsheet');

// Aggregator for COVID-19
module.exports = async function(type) {
  const doc = new GoogleSpreadsheet(config.apibSheet);

  doc.useApiKey(config.apibSheetKey);
  await doc.loadInfo();

  var totals = {
    'confirmed': 0,
    'deaths'   : 0,
    'peoples'  : {},
    'states'   : {},
    'cities'   : {},
  }

  var timeSeries = {
    'brasil': {
      //'confirmed': [],
      'deaths'   : [],
    }
  }

  var casesCount  = {};
  var deathsCount = {};

  // Cases
  const casesSheet = doc.sheetsById[config.apibSheetCasesId]
  var   casesRows  = await casesSheet.getRows()
  var   cases      = []

  for (var row in casesRows) {
    //var item = casesRows[row]._rawData
    var item = Object.assign({}, casesRows[row])

    // Ignore non-confirmed entries
    //
    // Methodology: Para número de confirmados: Contabilizar a partir de um
    // filtro, da coluna STATUS (Confirmado).
    if (item['STATUS'] != 'Confirmado') {
      continue;
    }

    // Ignore itens without a proper number
    var total = Number(item['QUANTIDADE CONFIRMADA DE INFECTADOS'])

    if (isNaN(total)) {
      continue
    }

    // Remove unneeded metadata: usually properties starting with underscore
    for (var el in item) {
      if (el.match(/^_/)) {
        delete item[el]
      }
    }

    // Initialize people entry
    if (totals.peoples[item['POVOS']] == undefined) {
      totals.peoples[item['POVOS']] = {
        confirmed: 0,
        deaths   : 0,
      };
    }

    // Initialize state entry
    if (totals.states[item['RESIDÊNCIA (Estado)']] == undefined) {
      totals.states[item['RESIDÊNCIA (Estado)']] = {
        confirmed: 0,
        deaths   : 0,
      };
    }

    // Initialize city entry
    if (totals.cities[item['RESIDÊNCIA (Município)']] == undefined) {
      totals.cities[item['RESIDÊNCIA (Município)']] = {
        confirmed: 0,
        deaths   : 0,
      };
    }

    // Totals
    totals.confirmed                                        += total
    totals.peoples[item['POVOS']].confirmed                 += total
    totals.states[item['RESIDÊNCIA (Estado)']].confirmed    += total
    totals.cities[item['RESIDÊNCIA (Município)']].confirmed += total

    // Ignore fields without dates
    if (item['DATA DA CONFIRMAÇÃO'] == '' || item['DATA DA CONFIRMAÇÃO'] == undefined) {
      continue;
    }

    // Time series
    var date_death = item['DATA DA CONFIRMAÇÃO'].split('/');
    var date       = new Date(date_death[2], Number(date_death[1]) - 1, date_death[0])

    // Use the day before in the dataset, as it always point to the data from the previous day
    //date.setDate(date.getDate() - 1);

    date = date.getTime();

    if (isNaN(date)) {
      continue;
    }

    if (casesCount[date] == undefined) {
      casesCount[date] = {
        brasil    : 0,
        //gender_f: 0,
        //gender_m: 0,
      };
    }

    casesCount[date].brasil += total

    cases.push(item)
  }

  // Deaths
  const deathsSheet = doc.sheetsById[config.apibSheetDeathsId]
  var   deathsRows  = await deathsSheet.getRows()
  var   deaths      = []

  for (var row in deathsRows) {
    //var item = deathsRows[row]._rawData.slice()
    var item = Object.assign({}, deathsRows[row])

    if (type == 'partial') {
      // Ignore non-confirmed entries
      // Methodology:
      //
      // Para número de óbitos: Contabilizar a partir de dois filtros, um da
      // coluna STATUS (Confirmado) e outro da coluna CONTABILIZADO PELA SESAI (N).
      //
      // Como o contador soma os números do Comitê e os números da SESAI,
      // precisamos retirar da planilha do comitê apenas os casos confirmados e
      // aqueles que não foram contabilizados pela SESAI, de modo a reduzir o
      // sombreamento o tanto quanto podemos.
      if (item['STATUS'] != 'Confirmado' || item['CONTABILIZADO PELA SESAI'] != 'N') {
        continue;
      }
    }
    else {
      // Ignore non-confirmed entries
      //
      // Methodology: Para número de óbitos: Contabilizar a partir de um
      // filtro, da coluna STATUS (Confirmado).
      if (item['STATUS'] != 'Confirmado') {
        continue;
      }
    }

    // Remove unneeded metadata: usually properties starting with underscore
    for (var el in item) {
      if (el.match(/^_/)) {
        delete item[el]
      }
    }

    // Remove unneeded data
    //delete item[4]
    //item.splice(4, 1)
    delete item.NOME

    // Initialize people entry
    if (totals.peoples[item['POVOS']] == undefined) {
      totals.peoples[item['POVOS']] = {
        confirmed: 0,
        deaths   : 0,
      };
    }

    // Initialize state entry
    if (totals.states[item['FALECIMENTO (Estado)']] == undefined) {
      totals.states[item['FALECIMENTO (Estado)']] = {
        confirmed: 0,
        deaths   : 0,
      };
    }

    // Initialize city entry
    if (totals.cities[item['FALECIMENTO (Município)']] == undefined) {
      totals.cities[item['FALECIMENTO (Município)']] = {
        confirmed: 0,
        deaths   : 0,
      };
    }

    // Totals
    totals.deaths++
    totals.peoples[item['POVOS']].deaths++;
    totals.states[item['FALECIMENTO (Estado)']].deaths++;
    totals.cities[item['FALECIMENTO (Município)']].deaths++;

    // Ignore fields without dates
    if (item['DATA DO ÓBITO'] == '' || item['DATA DO ÓBITO'] == undefined) {
      continue;
    }

    // Time series
    var date_death = item['DATA DO ÓBITO'].split('/');
    var date       = new Date(Number(date_death[2]), Number(date_death[1]) - 1, Number(date_death[0]));

    // Use the day before in the dataset, as it always point to the data from the previous day
    //date.setDate(date.getDate() - 1);

    date = date.getTime();

    if (isNaN(date)) {
      continue;
    }

    if (deathsCount[date] == undefined) {
      deathsCount[date] = {
        brasil    : 0,
        //gender_f: 0,
        //gender_m: 0,
      };
    }

    deathsCount[date].brasil++;

    deaths.push(item)
  }

  // Build time series
  //for (item in casesCount) {
  //  timeSeries.brasil.confirmed.push([
  //    item,
  //    casesCount[item].brasil,
  //  ]);
  //}

  function sortSeries(series) {
    return series.sort(function(a, b) {
      return a[0] - b[0];
    });
  }

  for (item in deathsCount) {
    timeSeries.brasil.deaths.push([
      Number(item),
      Number(deathsCount[item].brasil),
    ]);
  }

  timeSeries.brasil.deaths = sortSeries(timeSeries.brasil.deaths)

  // Integrate
  for (n = 1; n < timeSeries.brasil.deaths.length; n++) {
    timeSeries.brasil.deaths[n][1] += timeSeries.brasil.deaths[n-1][1];
  }

  // Write output
  fs.writeFile(compiledPath + type + '/' + 'cases.json',      JSON.stringify(cases,      null, 2), fsWriteError);
  fs.writeFile(compiledPath + type + '/' + 'deaths.json',     JSON.stringify(deaths,     null, 2), fsWriteError);
  fs.writeFile(compiledPath + type + '/' + 'totals.json',     JSON.stringify(totals,     null, 2), fsWriteError);
  fs.writeFile(compiledPath + type + '/' + 'timeseries.json', JSON.stringify(timeSeries, null, 2), fsWriteError);
}
