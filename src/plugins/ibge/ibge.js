const output        = __dirname + '/../../../web/data/';
//const fs          = require('fs');
const fetch         = require('node-fetch');
const config        = require('../../../config/api.js');
const buildUrl      = require('../../../vendor/mapa/src/backend/api/url.js');

module.exports = {
  codes: {},

  init: async function() {
    //var codesFile = output + 'ibge.json';
    var urlCodes = 'https://api.socioambiental.org/static/ibge/localidades.json'

    try {
      var codesResource = await fetch(urlCodes + '?apikey=' + config.apikey).catch(err => console.error(err));;
      var codes         = await codesResource.json();

      //if (fs.existsSync(codesFile)) {
      //  var codes = JSON.parse(fs.readFileSync(codesFile, 'utf8'));
      //
        for (var code in codes) {
          // City code
          this.codes[codes[code]['Código Município Completo']] = codes[code];

          // State code
          this.codes[codes[code].UF] = codes[code]['Nome_UF'];
        }
      //}
    } catch(err) {
      this.codes = {};
    }
  },

  locality: function(ibge_code) {
    return this.codes[ibge_code] != undefined ? this.codes[ibge_code] : null;
  },
}
