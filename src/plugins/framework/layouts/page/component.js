import config from 'AppConfig/app.config.js'

var iframer = require('iframe-resizer')

export default {
  props: {
    drawerOpened: {
      type   : Boolean,
      default: true,
    },
    pageTitle: {
      type   : String,
      default: config.title,
    },
  },

  data: () => ({
    drawer     : null,
    title      : null,
    locale     : null,
    footerColor: null,
    locales    : [],
  }),

  created () {
    if (config.theme != undefined && config.theme == 'dark') {
      this.$vuetify.theme.dark = true;
    }

    this.drawer      = this.drawerOpened && document.documentElement.clientWidth > 992 ? true : false;
    this.title       = this.pageTitle;
    this.footerColor = config.footer.color;
  },

  mounted() {
    this.locale = this.$i18n.locale;

    // Build locales
    for (var lang in this.$i18n.availableLocales) {
      this.locales.push({
        text:  this.$i18n.t(this.$i18n.availableLocales[lang]),
        value: this.$i18n.availableLocales[lang],
      });
    }

    // Dynamically resize all iframes
    iframer.iframeResize({
      //heightCalculationMethod: 'taggedElement',
      //log                    : true
    }, 'iframe.autosize');

    //iframer.iframeResize({
    //  log: true
    //}, 'iframe.autosize-not-tagged');
  },

  computed: {
    inIframe() {
      // Thanks https://stackoverflow.com/questions/326069/how-to-identify-if-a-webpage-is-being-loaded-inside-an-iframe-or-directly-into-t#326076
      try {
        return window.self !== window.top;
      } catch (e) {
        return true;
      }
    },

    footerIframeSrc() {
      var sponsors = (config.footer.sponsors != null) ? '&sponsors=' + config.footer.sponsors : '';
      var partners = (config.footer.partners != null) ? '&partners=' + config.footer.partners : '';

      return 'https://rodape.socioambiental.org/?site=' + config.name + sponsors + partners + '&lang=' + this.$i18n.locale;
    },
  },
  watch: {
    pageTitle: function() {
      this.title = this.pageTitle;
    },

    locale: function() {
      if (this.locale != this.$i18n.locale) {
        this.$i18n.locale = this.locale
        this.$root.$emit('locale-transition', this.locale)

        if (this.locale == 'pt-br') {
          this.$vuetify.lang.current = 'pt'
        }
        else {
          this.$vuetify.lang.current = this.locale
        }
      }
    },
  },
}
