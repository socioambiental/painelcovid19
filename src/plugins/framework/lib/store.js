import Vue  from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // Used to track pending requests, which avoids duplicate requests
    status: {
    },
  },

  mutations: {
    // Create an entry
    create(state, payload) {
      if (state[payload.key] == undefined) {
        // Unset any pending state
        state.status[payload.key] = undefined

        Vue.set(state, payload.key, null)
      }
    },

    // Assigm an item in the datastore
    assign(state, payload) {
      if (state[payload.key] == undefined) {
        // Unset any pending state
        state.status[payload.key] = undefined

        Vue.set(state, payload.key, payload.value)
      }
    },

    // Same as assign, but meant for always overwriting it's content
    overwrite(state, payload) {
      // Unset any pending state
      state.status[payload.key] = undefined

      Vue.set(state, payload.key, payload.value)
    },

    // Concatenate data into an item in the datastore
    concat(state, payload) {
      // Unset any pending state
      //state.status[payload.key] = undefined

      if (state[payload.key] = undefined) {
        Vue.set(state, payload.key, payload.value)
      }
      else {
        var data = state[payload.key]

        if (Array.isArray(data[payload.key])) {
          for (var item in payload.value) {
            data.push(payload.value)
          }
        }
        else {
          for (var item in payload.value) {
            data[item] = payload.value
          }
        }

        // Ugly hack to force reactivity!
        Vue.set(state, payload.key, null)

        Vue.set(state, payload.key, data)
      }
    },

    // Apend data into an item in the datastore
    append(state, payload) {
      // Unset any pending state
      state.status[payload.key][payload.index] = undefined

      var data = { }

      if (state[payload.key] != undefined) {
        data = state[payload.key]
      }

      data[payload.index] = payload.value

      // Ugly hack to force reactivity!
      Vue.set(state, payload.key, null)

      Vue.set(state, payload.key, data)
    },

    // Same of append, but only if the index does not exist
    include(state, payload) {
      // Unset any pending state
      state.status[payload.key][payload.index] = undefined

      var data = { }

      if (state[payload.key] != undefined) {
        data = state[payload.key]
      }

      if (data[payload.index] == undefined) {
        //console.log('Setting ' + payload.index + ' at ' + payload.key)
        data[payload.index] = payload.value

        // Ugly hack to force reactivity!
        Vue.set(state, payload.key, null)

        Vue.set(state, payload.key, data)
      }
    },
  },

  actions: {
    get(context, options) {
      var url      = options.url
      var key      = options.key
      var mutation = options.mutation != undefined ? options.mutation : 'assign'

      // Check if data should be fetched
      if (context.state[key] != undefined) {
        // Assign mutation is intended to set value only once
        if (mutation == 'assign') {
          return
        }
        // Check if data is already there
        else if (mutation == 'include') {
          if (context.state[key][options.index] != undefined) {
            return
          }
        }
      }

      // Check for pending requests
      if (mutation != 'concat') {
        if (options.index != undefined) {
          if (context.state.status[key]              != undefined  &&
            context.state.status[key][options.index] != undefined  &&
            context.state.status[key][options.index] == 'pending') {
            return
          }
          else {
            if (context.state.status[key] == undefined) {
              context.state.status[key] = {}
            }

            context.state.status[key][options.index] = 'pending'
          }
        }
        else {
          if (context.state.status[key] != undefined  &&
            context.state.status[key] == 'pending') {
            return
          }
          else {
            context.state.status[key] = 'pending'
          }
        }
      }

      // Fetch data
      fetch(url)
        .then((resp) => resp.json())
        .then(function(data) {
          context.commit(mutation, { key: key, index: options.index, value: data })
        })
    }
  },
})
