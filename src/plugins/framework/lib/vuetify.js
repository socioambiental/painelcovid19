// Vuetify dependencies
import Vue     from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

//import i18n from './i18n';

// i18n messages
import pt from 'vuetify/es5/locale/pt'
import en from 'vuetify/es5/locale/en'
import es from 'vuetify/es5/locale/es'

Vue.use(Vuetify)

const opts = {
  lang: {
    //t: (key, ...params) => i18n.t(key, params),
    locales: { pt, en, es },
    current: 'pt',
  },
};

export default new Vuetify(opts)
