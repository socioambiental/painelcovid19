import Vue     from 'vue';
import vuetify from 'AppFramework/vuetify.js';
import i18n    from 'AppFramework/i18n.js';
import store   from 'AppFramework/store.js';
import config  from 'AppConfig/app.config.js'
import VueGtag from "vue-gtag";

if (config.analytics.google_apikey != undefined && config.google_apikey != null) {
  Vue.use(VueGtag, {
    config: { id: config.analytics.google_apikey }
  });
}

// App loader
export default function(app) {
  var vm = new Vue({
    i18n,
    store,
    vuetify,
    el    : '#app',
    render: h => h(app),
  });

  return vm;
}
