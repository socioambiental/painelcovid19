import Vue     from 'vue'
import VueI18n from 'vue-i18n'

// i18n messages
import { messages } from 'AppConfig/app.messages.js';

// Extend
//messages.pt = messages['pt-br'];

Vue.use(VueI18n)

export default new VueI18n({
  //locale: 'pt',
  locale: 'pt-br',
  messages: messages,
});
