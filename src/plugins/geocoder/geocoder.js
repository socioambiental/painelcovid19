const output        = __dirname + '/../../../web/data/';
const fs            = require('fs');
const fsWriteError  = require('../../../vendor/mapa/src/backend/fs/error.js');

const NodeGeocoder  = require('node-geocoder');
const options       = { provider: 'openstreetmap', };
const geocoder      = NodeGeocoder(options);

module.exports = {
  geocoding: {},

  geocodingFile: null,

  init: function(geocodingFile) {
    try {
      if (fs.existsSync(geocodingFile)) {
        this.geocodingFile = geocodingFile;
        this.geocoding     = JSON.parse(fs.readFileSync(geocodingFile, 'utf8'));
      }
    } catch(err) {
      this.geocoding = {};
    }
  },

  get: async function(ibge_code, city, state) {
    if (this.geocoding[ibge_code] == undefined) {
      console.log('Getting geocode data...');

      this.geocoding[ibge_code] = await geocoder.geocode({
        city    : city,
        state   : state,
        country : 'Brazil',
      });

      // Do not make many requests at a time
      // See https://operations.osmfoundation.org/policies/nominatim/
      // Thanks https://stackoverflow.com/a/49139664
      await new Promise(resolve => setTimeout(resolve, 1500));
    }

    if (this.geocoding[ibge_code][0] != undefined) {
      return {
        latitude  : this.geocoding[ibge_code][0].latitude,
        longitude : this.geocoding[ibge_code][0].longitude,
      }
    }
    else {
      return { };
    }
  },

  save: function() {
    fs.writeFile(this.geocodingFile, JSON.stringify(this.geocoding), fsWriteError);
  },
}
