# Use http image
# https://hub.docker.com/_/httpd/
#FROM httpd:2.4
#COPY . /usr/local/apache2/htdocs/

# Use the latest debian stable image
# https://hub.docker.com/_/debian/
FROM debian:stable

# Install apache
RUN apt-get update && apt-get install -y apache2 libapache2-mod-php && apt-get clean && rm -rf /var/lib/apt/lists/*

# Apache custom config
# See https://bz.apache.org/bugzilla/show_bug.cgi?id=33089
RUN a2enmod include
RUN a2enmod php7.3
RUN echo -n "<Directory /var/www/html>\nAllowOverride All\n</Directory>\n" > /etc/apache2/conf-available/allow_override.conf
RUN echo -n "<Directory /var/www/html>\nOptions +Includes\n</Directory>\n" > /etc/apache2/conf-available/options_includes.conf
RUN echo "XBitHack on"                                                     > /etc/apache2/conf-available/xbithack.conf
RUN echo "ServerName painelcovid19"                                        > /etc/apache2/conf-available/servername.conf
RUN a2enconf allow_override
RUN a2enconf options_includes
RUN a2enconf xbithack
RUN a2enconf servername

# Copy our application
COPY web /var/www/html

# Set the environment
ENV APACHE_RUN_USER  www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR   /var/log/apache2

# Open port 80
EXPOSE 80

# Start apache
ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
