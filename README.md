# Painel: COVID-19 / Instituto Socioambiental

Este repositório concentra rotinas de obtenção e exibição de dados sobre a pandemia de COVID-19.

## Fontes utilizadas

* Casos gerais via [Brasil.IO](https://brasil.io/covid19/).
* Casos indígenas direto da [Sesai](http://www.saudeindigena.net.br/coronavirus/mapaEp.php) raspados para [deste endpoint](https://covid19.socioambiental.org/sites/default/files/data.json).
* Casos indígenas levantados pelo [Comitê Nacional de Vida e Memória Indígena](http://quarentenaindigena.info/).

## Fontes e estudos adicionais

* https://ciis.fmrp.usp.br/covid19/
* https://covid19br.github.io/
* https://www.lagomdata.com.br/coronavirus
* https://covid-19-risk.github.io/map/brazil/pt/
* http://www.saudeindigena.net.br/coronavirus
* http://covid19.fct.unesp.br/coronavirus/
* https://quilombolas-ibgedgc.hub.arcgis.com/
* https://brasil.io/dataset/covid19/obito_cartorio/ / https://transparencia.registrocivil.org.br/especial-covid
* https://covidgoias.ufg.br
* https://covid-amazonas-idsm.mamiraua.org.br/ / https://mamiraua.org.br/covid-amazonas
* http://quarentenaindigena.info/casos-indigenas/
* https://public.tableau.com/profile/covid19estadosbr#!/vizhome/TendnciadosEstados_15872349572950/PainelBrasil
* [Dados da ESRI (Johns Hopkins CSE?)](https://services1.arcgis.com/0MSEUqKaxRlEPj5g/ArcGIS/rest/services/Coronavirus_2019_nCoV_Cases/FeatureServer/2/query?where=Country_Region%3D%27Brazil%27&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&resultType=standard&distance=0.0&units=esriSRUnit_Meter&returnGeodetic=false&outFields=Confirmed%2C+Deaths%2C+Recovered&returnGeometry=true&featureEncoding=esriDefault&multipatchOption=xyFootprint&maxAllowableOffset=&geometryPrecision=&outSR=&datumTransformation=&applyVCSProjection=false&returnIdsOnly=false&returnUniqueIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&returnQueryGeometry=false&returnDistinctValues=false&cacheHint=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&having=&resultOffset=&resultRecordCount=&returnZ=false&returnM=false&returnExceededLimitFeatures=true&quantizationParameters=&sqlFormat=none&f=html&token=).

## Embarcando conteúdo

O conteúdo desta plataforma é todo embarcável:

    <!-- Used for responsive iframes -->
    <script
      src="https://unpkg.com/iframe-resizer@4.2.11/js/iframeResizer.min.js"
      integrity="sha384-fTp2I0abasco+1TIQhjTbS/v5ihiE09Kbng/klOF2y/DtZCcieGzK+ierZKtNBBx"
      crossorigin="anonymous">
    </script>

    <script type="text/javascript">
      jQuery(document).ready(function() {
        iFrameResize({ log: false }, '.iframe-resizer');
      });
    </script>

    <iframe
      class="iframe-resizer"
      allowfullscreen=""
      sandbox="allow-same-origin
      allow-scripts
      allow-popups"
      src="https://painelcovid19.socioambiental.org/indigenas/apib/serie/"
      scrolling="no"
      width="100%"
      height="430px"
      frameborder="0"
    </iframe>

## Licenças e créditos específicos

* Utiliza [logotipo da Fundação Cultural Palmares](http://www.palmares.gov.br/?page_id=37200) em marcador de mapa.
