# TODO

## Ciclo 4

### Dados

* [ ] Nota técnica / instrução de uso dos dados (onde estão, licença, etc) e
      link para o código fonte.

### Funcionalidades

* [ ] Mapa:
  * [ ] Casos indígenas (Sesai e APIB / Comitê).
  * [ ] Quilombolas: óbitos por munícipio. Avaliar esforço.
* [ ] Gráfico:
  * [ ] Quilombolas: gráficos das tabelas.
* [ ] Tabela:
    * [ ] Componentes de tabela com exportação de dados (JSON/CSV).
          Possibilidade: [vue-json-excel](https://www.npmjs.com/package/vue-json-excel).
* [ ] Caixa:
    * [ ] Placar por código IBGE.
* [ ] Outros:
    * [ ] Quilombolas:
        * [ ] Outras saídas que possam ser solicitadas.

### Correções

* [ ] Geral:
    * [ ] Textos:
        * [ ] Tradução do título das páginas em `<app-page>`.
        * [ ] Nota sobre irregularidade na compilação de dados, que pode fazer
              uma série "despencar" temporariamente.
        * [ ] Nota sobre uso de dados oficiais e subnotificação, o que pode
              levar aos dados a não representar os números reais.
        * [ ] Ortografica: checar crase em algumas chamadas.
    * [ ] Portar melhorias implementadas no componente
          `<chart-indigenous-sesai>` para outros componentes, como o atributo
          `redraw` e reconstrução de widget apenas se necessário e não sempre que um dado
          armazenado é modificado.
    * [ ] Lista de "menu" (navegação) completa só é exibida na página inicial.
          Mudar mecânica para que a barra apareça colapsada nas outras páginas mas
          não quando a página estiver embarcada via iframe.
    * [ ] Links da barra de navegação só estão sendo ativados a partir do
          segundo clique.
    * [ ] Consolidar funções `sortSeries` num único local, lidando com
          particularidades.
* [ ] Quilombolas:
    * [ ] Relatos de que a somatória de casos do quadro de estados (quadro
          geral) tem dado um número diferente do dado do placar (cômputo geral).
          Pode ser problema no script ou falta de preenchimento da coluna de localização
          (código IBGE).
     * [ ] [Correção de cômputos de quilombos em
           municípios](https://gitlab.com/socioambiental/painelcovid19/-/issues/5#note_531775656).
* [ ] APIB / Comitê:
    * [x] Checagem contagem de povos.
    * [x] Outras correções menores que possam ser solicitadas.
    * [ ] Atualizar links para o painel do site da APIB / Quarentena Indígena.

### Framework

* [x] Atualizar bibliotecas (`npm audit fix`).
* [ ] Incorporar melhorias de framework desenvolvidas nos ciclos
      [1](https://gitlab.com/socioambiental/alertas/-/issues/1) e
      [2](https://gitlab.com/socioambiental/alertas/-/issues/2).
