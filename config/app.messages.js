// Base messages
import { messages as mapMessages   }     from 'IsaMap/map/messages.js';
import { messages as chartMessages }     from 'AppPlugins/highcharts-i18n/messages.js';
import { messages as interfaceMessages } from 'AppFramework/messages.js';

// Custom messages
export var customMessages = {
  'en': {
    // Quilombolas
    'quilombola_localities'                : 'Quilombola localities',
    'quilombola_localities_by_state'       : 'Quilombola localities by state',
    'quilombola_layer_by_municipality'     : 'By municipality (IBGE)',
    'quilombola_layer_by_state'            : 'By state (IBGE)',
    'quilombolas_deaths'                   : 'Quilombolas deaths',
    'quilombola_localities_2020'           : 'Quilombola localities (IBGE 2020)',
    'total_municipalities_in_the_state'    : 'Municipalities in the state',
    'total_municipalities_with_quilombos'  : 'Municipalities with quilombola localities',

    // Mining
    'garimpo'                              : 'Illegal mining',
    'garimpo_points'                       : 'Points of illegal mining',
    'country'                              : 'Country',
    'extraction_method'                    : 'Extraction method',
    'contaminants'                         : 'Contaminants',

    // COVID-19 / Data messages
    'covid19'                              : 'COVID-19',
    'apib'                                 : 'APIB',
    'apib_source'                          : 'Comitê Nacional de Vida e Memória Indígena',
    'apib_credits'                         : 'Organizações indígenas de base da APIB, frentes de enfrentamento à COVID-19, SESAI, Secretarias Municipais e Estaduais de Saúde e Ministério Público Federal.',
    'totals'                               : 'Totals',
    'peoples'                              : 'Affected peoples',
    'suspected'                            : 'Suspected',
    'discarded'                            : 'Discarded',
    'infected'                             : 'Infected',
    'cured'                                : 'Cured',
    'local'                                : 'Place',
    'covid19_by_locality'                  : 'COVID-19: status by locality',
    'state_health_secretaries'             : 'State health departments',
    'brasil_io_harvest_code'               : 'Collection, analysis and code: <a href="https://brasil.io/">Brasil.IO</a> / <a title="CC-BY-SA" href="https://creativecommons.org/licenses/by-sa/4.0/legalcode" target="_blank">CC-BY-SA</a> / <a title="GPLv3" target="_blank" href="https://github.com/turicas/brasil.io/blob/219dd8095004fdd6d2107c0c1fbdb42f7bd68bc3/LICENSE">GPLv3</a>.',
    'map_imported_cases'                   : 'Nota: casos importados não estão representados no mapa.',
    'state_code'                           : 'State code',
    'state_acronym'                        : 'State acronym',
    'municipality_code'                    : 'Municipality code',
    'locality_code'                        : 'Locality code',
    'category_code'                        : 'Category code',
    'age'                                  : 'Age',
    'monitored'                            : 'Monitored',
    'by_state'                             : 'by state',
    'by_municipality'                      : 'by municipality',
    'by_place'                             : 'by place',
    'sesai'                                : 'Sesai',
    'ti_details'                           : 'Details about this Indigenous Land',
    'risk_index'                           : 'Risk index',
    'risk_indigenous_lands'                : 'Risk on Indigenous Lands',
    'risk_yanomami'                        : 'Risk for Yanomamis People',
    'daily_cases'                          : 'Daily cases',
    'quilombolas'                          : 'Quilombolas',
    'indigenous'                           : 'Indigenous',
    'yanomami'                             : 'Yanomami',
    'populations'                          : 'Populations',
    'letality'                             : 'Letality',
    'mortality'                            : 'Mortality',
    'last_info'                            : 'Last information',
    'isolated'                             : 'Isolated peoples',
    'hospitals'                            : 'Hospitals',
    'respirator'                           : 'With respirators',
    'uti'                                  : 'With ICU',
    'dsei'                                 : 'Health Districts (DSEIs)',
    'polo'                                 : 'Pole',
    'cod_dsei'                             : 'DSEI Code',
    'cod_polo'                             : 'Pole code',
    'indigenous_health'                    : 'Indigenous health',
    'indigenous_population'                : 'Indigenous population',
    'indigenous_deaths'                    : 'Indigenous killed by COVID-19',
    'indigenous_deaths_apib'               : 'Indigenous killed by COVID-19 (Committee)',
    'polobase'                             : 'Base pole',
    'city'                                 : 'City',
    'city_ibge_code'                       : 'IBGE Code',
    'confirmed'                            : 'Confirmed',
    'confirmed_plural'                     : 'Confirmed',
    'confirmed_prediction_exponential'     : 'Confirmed prediction (expon.)',
    'confirmed_per_100k_inhabitants'       : 'Confirmed by 100k inhabitants',
    'sir_model_infected'                   : 'SIR Model: Infected',
    'sir_model_recovered'                  : 'SIR Model: Recovered',
    'sir_model_susceptible'                : 'SIR Model: Susceptible',
    'deaths_per_100k_inhabitants'          : 'Deaths by 100k inhabitants',
    'death_rate'                           : 'Death rate',
    'death'                                : 'Death',
    'deaths'                               : 'Confirmed deaths',
    'estimated_population_2019'            : 'Estimated population in 2019',
    'is_last'                              : 'Is last?',
    'place_type'                           : 'Place type',
    'select_date'                          : 'Select a date',
    'cases'                                : 'Cases',
    'cases_map'                            : 'Cases Map',
    'brazil'                               : 'Brazil',
    'cities'                               : 'Cities',
    'municipalities'                       : 'Municipalities',
    'states'                               : 'States',
    'co_unidade'                           : 'Unit code',
    'co_cnes'                              : 'CNES code',
    'nome_munic'                           : 'Municipality',
    'nome1'                                : 'Name',
    'nome_fanta'                           : 'Hospital name',
    'co_leito1'                            : 'Bed code',
    'ds_leito1'                            : 'Bed description',
    'tp_leito1'                            : 'Bed type',
    'qtd_exist1'                           : 'Quantity',
    'qtd_contr1'                           : 'Contracted quantity',
    'qtd_sus1'                             : 'SUS',
    'logradouro'                           : 'Address',
    'endereco1'                            : 'Number',
    'complement'                           : 'Complement',
    'bairro1'                              : 'Neighborhood',
    'cep1'                                 : 'ZIP Code',
    'regiao_sau'                           : 'Health district',
    'distrito_s'                           : 'District',
    'telefone1'                            : 'Telephone',
    'fax1'                                 : 'Fax',
    'risks'                                : 'Risks',
    'risks_map'                            : 'Risks map',
    //'cod_muni_1'                         : 'City code',
    //'cod_muni_2'                         : '',
  },
  'pt-br': {
    // Quilombolas
    'quilombola_localities'                : 'Localidades quilombolas',
    'quilombola_localities_by_state'       : 'Localidades quilombolas por estado',
    'quilombola_layer_by_municipality'     : 'Por município (IBGE)',
    'quilombola_layer_by_state'            : 'Por estado (IBGE)',
    'quilombolas_deaths'                   : 'Óbitos de quilombolas',
    'quilombola_localities_2020'           : 'Localidades quilombolas (IBGE 2020)',
    'total_municipalities_in_the_state'    : 'Municípios no estado',
    'total_municipalities_with_quilombos'  : 'Municípios com localidades quilombolas',

    // Mining
    'garimpo'                              : 'Garimpo',
    'garimpo_points'                       : 'Pontos de garimpo',
    'country'                              : 'País',
    'extraction_method'                    : 'Método de extração',
    'contaminants'                         : 'Contaminantes',

    // COVID-19 / Data messages
    'covid19'                              : 'COVID-19',
    'peoples'                              : 'Povos afetados',
    'apib'                                 : 'APIB',
    'apib_source'                          : 'Comitê Nacional de Vida e Memória Indígena',
    'apib_credits'                         : 'Organizações indígenas de base da APIB, frentes de enfrentamento à COVID-19, SESAI, Secretarias Municipais e Estaduais de Saúde e Ministério Público Federal.',
    'totals'                               : 'Totais',
    'suspected'                            : 'Suspeitos',
    'discarded'                            : 'Descartados',
    'infected'                             : 'Infectados',
    'cured'                                : 'Curados',
    'local'                                : 'Local',
    'covid19_by_locality'                  : 'COVID-19: situação por localidade',
    'state_health_secretaries'             : 'Secretarias Estaduais de Saúde.',
    'brasil_io_harvest_code'               : 'Coleta, análise e código: <a href="https://brasil.io/">Brasil.IO</a> / <a title="CC-BY-SA" href="https://creativecommons.org/licenses/by-sa/4.0/legalcode" target="_blank">CC-BY-SA</a> / <a title="GPLv3" target="_blank" href="https://github.com/turicas/brasil.io/blob/219dd8095004fdd6d2107c0c1fbdb42f7bd68bc3/LICENSE">GPLv3</a>.',
    'map_imported_cases'                   : 'Nota: casos importados não estão representados no mapa.',
    'state_code'                           : 'Código da UF',
    'state_acronym'                        : 'Sigla da UF',
    'municipality_code'                    : 'Código do município',
    'locality_code'                        : 'Código da localidade',
    'category_code'                        : 'Código da categoria',
    'age'                                  : 'Idade',
    'monitored'                            : 'Monitorados',
    'by_state'                             : 'por estado',
    'by_municipality'                      : 'por município',
    'by_place'                             : 'por local',
    'sesai'                                : 'Sesai',
    'ti_details'                           : 'Detalhes desta Terra Indígena',
    'risk_index'                           : 'Índice de Risco',
    'risk_indigenous_lands'                : 'Risco nas Terras Indígenas',
    'risk_yanomami'                        : 'Risco ao Povo Yanomami',
    'daily_cases'                          : 'Casos diários',
    'quilombolas'                          : 'Quilombolas',
    'indigenous'                           : 'Indígenas',
    'yanomami'                             : 'Yanomami',
    'populations'                          : 'Populações',
    'letality'                             : 'Letalidade',
    'mortality'                            : 'Mortalidade',
    'last_info'                            : 'Última informação',
    'isolated'                             : 'Povos isolados',
    'hospitals'                            : 'Hospitais',
    'respirator'                           : 'Com respiradores',
    'uti'                                  : 'Com UTI',
    'dsei'                                 : 'Distritos de Saúde (DSEIs)',
    'polo'                                 : 'Polo',
    'cod_dsei'                             : 'Código DSEI',
    'cod_polo'                             : 'Código do Polo',
    'indigenous_health'                    : 'Saúde indígena',
    'indigenous_population'                : 'População indígena',
    'indigenous_deaths'                    : 'Indígenas mortos pela COVID-19',
    'indigenous_deaths_apib'               : 'Indígenas mortos pela COVID-19 (Comitê)',
    'polobase'                             : 'Polo base',
    'city'                                 : 'Cidade',
    'city_ibge_code'                       : 'Código IBGE',
    'confirmed'                            : 'Confirmado',
    'confirmed_plural'                     : 'Confirmados',
    'confirmed_prediction_exponential'     : 'Previsão de confirmados (expon.)',
    'confirmed_per_100k_inhabitants'       : 'Confirmado por 100 mil habitantes',
    'sir_model_infected'                   : 'Modelo SIR: Infectados',
    'sir_model_recovered'                  : 'Modelo SIR: Recuperados',
    'sir_model_susceptible'                : 'Modelo SIR: Suscetíveis',
    'deaths_per_100k_inhabitants'          : 'Óbitos por 100 mil habitantes',
    'death_rate'                           : 'Taxa de mortalidade',
    'death'                                : 'Falecimento',
    'deaths'                               : 'Óbitos confirmados',
    'estimated_population_2019'            : 'População estimada em 2019',
    'is_last'                              : 'Último?',
    'place_type'                           : 'Tipo de local',
    'select_date'                          : 'Selecione uma data',
    'cases'                                : 'Casos',
    'cases_map'                            : 'Mapa de Casos',
    'brazil'                               : 'Brasil',
    'cities'                               : 'Cidades',
    'municipalities'                       : 'Municípios',
    'states'                               : 'Estados',
    'co_unidade'                           : 'Código da unidade',
    'co_cnes'                              : 'Código CNES',
    'nome_munic'                           : 'Município',
    'nome1'                                : 'Nome',
    'nome_fanta'                           : 'Nome do hospital',
    'co_leito1'                            : 'Código do leito',
    'ds_leito1'                            : 'Descrição do leito',
    'tp_leito1'                            : 'Tipo de leito',
    'qtd_exist1'                           : 'Quantidade',
    'qtd_contr1'                           : 'Quantidade contratada',
    'qtd_sus1'                             : 'SUS',
    'logradouro'                           : 'Endereço',
    'endereco1'                            : 'Número',
    'complement'                           : 'Complemento',
    'bairro1'                              : 'Bairro',
    'cep1'                                 : 'CEP',
    'regiao_sau'                           : 'Distrito de saúde',
    'distrito_s'                           : 'Distrito',
    'telefone1'                            : 'Telefone',
    'fax1'                                 : 'Fax',
    'risks'                                : 'Riscos',
    'risks_map'                            : 'Mapa de Riscos',
    //'cod_muni_1'                         : 'City code',
    //'cod_muni_2'                         : '',
  },
  'es': {
    // Quilombolas
    'quilombola_localities'                : 'Localidades quilombolas',
    'quilombola_localities_by_state'       : 'Localidades quilombolas por estado',
    'quilombola_layer_by_municipality'     : 'Por municipio (IBGE)',
    'quilombola_layer_by_state'            : 'Por estado (IBGE)',
    'quilombolas_deaths'                   : 'Muertes de quilombolas',
    'quilombola_localities_2020'           : 'Localidades quilombolas (IBGE 2020)',
    'total_municipalities_in_the_state'    : 'Municípios en el estado',
    'total_municipalities_with_quilombos'  : 'Municípios con localidades quilombolas',

    // Mining
    'garimpo'                              : 'Minería Ilegal',
    'garimpo_points'                       : 'Puntos de minería ilegal',
    'country'                              : 'País',
    'extraction_method'                    : 'Metodo de extración',
    'contaminants'                         : 'Contaminantes',

    // COVID-19 / Data messages
    'covid19'                              : 'COVID-19',
    'peoples'                              : 'Pueblos afectados',
    'apib'                                 : 'APIB',
    'apib_source'                          : 'Comitê Nacional de Vida e Memória Indígena',
    'apib_credits'                         : 'Organizações indígenas de base da APIB, frentes de enfrentamento à COVID-19, SESAI, Secretarias Municipais e Estaduais de Saúde e Ministério Público Federal.',
    'totals'                               : 'Totales',
    'suspected'                            : 'Sospechos',
    'discarded'                            : 'Descartados',
    'infected'                             : 'Infectados',
    'cured'                                : 'Curados',
    'local'                                : 'Local',
    'covid19_by_locality'                  : 'COVID-19: situación por localidad',
    'state_health_secretaries'             : 'Secretarias Estaduais de Saúde.',
    'brasil_io_harvest_code'               : 'Coleta, análisis y codigo: <a href="https://brasil.io/">Brasil.IO</a> / <a title="CC-BY-SA" href="https://creativecommons.org/licenses/by-sa/4.0/legalcode" target="_blank">CC-BY-SA</a> / <a title="GPLv3" target="_blank" href="https://github.com/turicas/brasil.io/blob/219dd8095004fdd6d2107c0c1fbdb42f7bd68bc3/LICENSE">GPLv3</a>.',
    'map_imported_cases'                   : 'Nota: casos importados não estão representados no mapa.',
    'state_code'                           : 'Codigo del estado',
    'state_acronym'                        : 'Sigla de la UF',
    'municipality_code'                    : 'Codigo del município',
    'locality_code'                        : 'Codigo de la localidad',
    'category_code'                        : 'Codigo de la categoría',
    'age'                                  : 'Idad',
    'monitored'                            : 'Monitorados',
    'by_state'                             : 'por estado',
    'by_municipality'                      : 'por municipio',
    'by_place'                             : 'por lugar',
    'sesai'                                : 'Sesai',
    'ti_details'                           : 'Detalhes desta Tierra Indigena',
    'risk_index'                           : 'Indice de Riesgo',
    'risk_indigenous_lands'                : 'Riesgo en las Tierras Indigenas',
    'risk_yanomami'                        : 'Risco ao Pueblo Yanomami',
    'daily_cases'                          : 'Casos diarios',
    'quilombolas'                          : 'Quilombolas',
    'indigenous'                           : 'Indigenas',
    'yanomami'                             : 'Yanomami',
    'populations'                          : 'Poblaciones',
    'letality'                             : 'Letalidad',
    'mortality'                            : 'Mortalidad',
    'last_info'                            : 'Ultima información',
    'isolated'                             : 'Povos isolados',
    'hospitals'                            : 'Hospitais',
    'respirator'                           : 'Con respiradores',
    'uti'                                  : 'Con UTI',
    'dsei'                                 : 'Distritos de Salud (DSEIs)',
    'polo'                                 : 'Polo',
    'cod_dsei'                             : 'Codigo DSEI',
    'cod_polo'                             : 'Codigo del Polo',
    'indigenous_health'                    : 'Salud indigena',
    'indigenous_population'                : 'Población indigena',
    'indigenous_deaths'                    : 'Indígenas muertos por la COVID-19',
    'indigenous_deaths_apib'               : 'Indígenas muertos por la COVID-19 (Comité)',
    'polobase'                             : 'Polo base',
    'city'                                 : 'Ciudad',
    'city_ibge_code'                       : 'Codigo IBGE',
    'confirmed'                            : 'Confirmado',
    'confirmed_plural'                     : 'Confirmados',
    'confirmed_prediction_exponential'     : 'Previsión de confirmados (expon.)',
    'confirmed_per_100k_inhabitants'       : 'Confirmado por 100 mil habitantes',
    'sir_model_infected'                   : 'Modelo SIR: Infectados',
    'sir_model_recovered'                  : 'Modelo SIR: Recuperados',
    'sir_model_susceptible'                : 'Modelo SIR: Suscetibles',
    'deaths_per_100k_inhabitants'          : 'Óbitos por 100 mil habitantes',
    'death_rate'                           : 'Taxa de letalidad',
    'death'                                : 'Muerte',
    'deaths'                               : 'Muertes confirmadas',
    'estimated_population_2019'            : 'Población estimada en 2019',
    'is_last'                              : 'Ultimo?',
    'place_type'                           : 'Tipo de localidad',
    'select_date'                          : 'Selecione una fecha',
    'cases'                                : 'Casos',
    'cases_map'                            : 'Mapa de Casos',
    'brazil'                               : 'Brasil',
    'cities'                               : 'Ciudades',
    'municipalities'                       : 'Municipios',
    'states'                               : 'Estados',
    'co_unidade'                           : 'Codigo de la unidade',
    'co_cnes'                              : 'Codigo CNES',
    'nome_munic'                           : 'Municipalidade',
    'nome1'                                : 'Nombre',
    'nome_fanta'                           : 'Nombre del hospital',
    'co_leito1'                            : 'Codigo del leito',
    'ds_leito1'                            : 'Descripción del leito',
    'tp_leito1'                            : 'Tipo de leito',
    'qtd_exist1'                           : 'Quantidad',
    'qtd_contr1'                           : 'Quantidad contratada',
    'qtd_sus1'                             : 'SUS',
    'logradouro'                           : 'Fecha',
    'endereco1'                            : 'Numero',
    'complement'                           : 'Complemento',
    'bairro1'                              : 'Barrio',
    'cep1'                                 : 'ZIP Code',
    'regiao_sau'                           : 'Districto de salud',
    'distrito_s'                           : 'Districto',
    'telefone1'                            : 'Teléfono',
    'fax1'                                 : 'Fax',
    'risks'                                : 'Riesgos',
    'risks_map'                            : 'Mapa de Riesgos',
    //'cod_muni_1'                         : 'City code',
    //'cod_muni_2'                         : '',
  },
}

// Additions and overrides
export var messages = {
  'en': {
    ...mapMessages.en,
    ...customMessages.en,
    ...chartMessages.en,
    ...interfaceMessages.en,
  },
  'pt-br': {
    ...mapMessages['pt-br'],
    ...customMessages['pt-br'],
    ...chartMessages['pt-br'],
    ...interfaceMessages['pt-br'],
  },
  'es': {
    ...mapMessages.es,
    ...customMessages.es,
    ...chartMessages.es,
    ...interfaceMessages.es,
  },
}
