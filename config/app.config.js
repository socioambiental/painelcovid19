module.exports = {
  'name' : 'painelcovid19',
  'title': 'Painel COVID-19',
  'theme': 'light',

  'analytics': {
    // Google Analytics API key
    'google_apikey': "UA-720697-54",
  },

  'footer': {
    'color'   : '#fff',
    'sponsors': 'moore,noruega,ue,goodenergies,rfn',
    'partners': 'csr,conaq,apib',
  },
}
