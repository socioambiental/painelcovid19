const webpack           = require("webpack");
const path              = require('path');
const VueLoaderPlugin   = require('vue-loader/lib/plugin');
const TerserPlugin      = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackCdnPlugin  = require('webpack-cdn-plugin');

const rules = [
  {
    test: /\.vue$/,
    loader: 'vue-loader',
  },
  {
    test: /\.css$/i,
    use: ['vue-style-loader', 'style-loader', 'css-loader'],
  },
  {
    test: /\.(png|jpg|gif)$/i,
    use: [
      {
        loader: 'url-loader',
        options: {
          //limit: 8192,
        },
      },
    ],
  },
  {
    test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
    use: [
      {
        loader: 'url-loader',
        options: {
          //limit: 8192,
        },
      },
    ],
  },
  {
    test: /\.svg$/i,
    use: [
      {
        loader: 'url-loader',
        options: {
          encoding: false,
        },
      },
    ],
  },
  // Vuetify requirement, see https://vuetifyjs.com/en/getting-started/quick-start/
  {
    test: /\.s(c|a)ss$/,
    use: [
      'vue-style-loader',
      'css-loader',
      {
        loader: 'sass-loader',
        // Requires sass-loader@^7.0.0
        options: {
          implementation: require('sass'),
          fiber: require('fibers'),
          indentedSyntax: true // optional
        },
        // Requires sass-loader@^8.0.0
        options: {
          implementation: require('sass'),
          sassOptions: {
            fiber: require('fibers'),
            indentedSyntax: true // optional
          },
        },
      },
    ],
  },
  {
    test: /\.less$/,
    use: [
      {
        loader: 'style-loader', // creates style nodes from JS strings
      },
      {
        loader: 'css-loader', // translates CSS into CommonJS
      },
      {
        loader: 'less-loader', // compiles Less to CSS
      },
    ],
  },
  {
    test: /\.thtml$/i,
    loader: 'html-loader',
  },
];

const optimization = {
  minimize : true,
  minimizer: [new TerserPlugin({
    test: /\.min\.js(\?.*)?$/i,
  })],
};

const watchOptions = {
  aggregateTimeout : 300,
  poll             : 1000,
  ignored          : [ /node_modules/ ],
};

const mod = {
  rules: rules,
};

const output = {
  //path: path.resolve(__dirname, '../dist'),
  path: path.resolve(__dirname, '../web/js'),
  filename: '[name].js'
};

const alias = {
  // vue.js
  'vue$'            : 'vue/dist/vue.esm.js',
  'vue-i18n$'       : 'vue-i18n/dist/vue-i18n.esm.js',

  // Mapa.eco.br
  IsaMap            : path.resolve(__dirname, '../vendor/mapa/src'),

  // Common app code (microframework)
  AppConfig         : path.resolve(__dirname, '../config'),
  AppPlugins        : path.resolve(__dirname, '../src/plugins'),
  AppFramework      : path.resolve(__dirname, '../src/plugins/framework/lib'),
  AppLayouts        : path.resolve(__dirname, '../src/plugins/framework/layouts'),

  // Custom Painel COVID-19 Code
  // Using these aliases helps to track dependencies on custom code
  Covid19Components : path.resolve(__dirname, '../src/components'),
  Covid19Pages      : path.resolve(__dirname, '../src/pages'),
  Covid19Scripts    : path.resolve(__dirname, '../src/scripts'),
  Covid19Backend    : path.resolve(__dirname, '../src/backend'),
  Covid19Lib        : path.resolve(__dirname, '../src/lib'),
};

const resolve = {
  alias: alias,
};

module.exports = {
  mode         : 'development',
  module       : mod,
  resolve      : resolve,
  optimization : optimization,
  output       : output,
  watchOptions : watchOptions,
  plugins: [
    new VueLoaderPlugin(),
    // Add jQuery everywhere
    new webpack.ProvidePlugin({
      'jQuery'   : 'jquery',
      //'Vue'    : ['vue/dist/vue.esm.js',        'default'],
      //'VueI18n': ['vue-i18n/dist/vue-i18n.esm', 'default'],
    }),

    new HtmlWebpackPlugin({
      // The html-webpack-template plugin requires inject to be false,
      // but we need it to be true so we can use the webpack-cdn-plugin:
      // https://github.com/jaketrent/html-webpack-template/issues/61
      inject    : false,
      cdnModule : 'default',
      template  : require('html-webpack-template'),
      mobile    : true,
      hash      : false,
      title     : 'Painel: COVID-19 no Brasil',
      favicon   : './web/images/icons/isa.ico',
      chunks    : [ 'index' ],
      filename  : '../index.html',
      links     : [
        // Needed by Vuetify, see https://vuetifyjs.com/en/getting-started/quick-start/
        "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900",
        "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css",
      ],
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
      // Comment this when setting inject to true
      scripts: [
        {
          src: '/js/index.js',
          type: 'module'
        },
        {
          src        : "https://unpkg.com/iframe-resizer@4.2.10/js/iframeResizer.min.js",
          integrity  : "sha384-2SLwonW5yBpif141hQ3+4T86vXbJL8Ge/uZLpdKkd6s0o2C0t6XuJor6D66Z6gkj",
          crossorigin: "anonymous",
        },
      ],
      bodyHtmlSnippet: '<div id="app"></div>',
    }),

    new HtmlWebpackPlugin({
      // The html-webpack-template plugin requires inject to be false,
      // but we need it to be true so we can use the webpack-cdn-plugin:
      // https://github.com/jaketrent/html-webpack-template/issues/61
      inject    : false,
      cdnModule : 'default',
      template  : require('html-webpack-template'),
      mobile    : true,
      hash      : false,
      title     : 'Painel: COVID-19 no Brasil - Casos Diários',
      favicon   : './web/images/icons/isa.ico',
      chunks    : [ 'cases' ],
      filename  : '../casos/index.html',
      links     : [
        // Needed by Vuetify, see https://vuetifyjs.com/en/getting-started/quick-start/
        "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900",
        "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css",
      ],
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
      // Comment this when setting inject to true
      scripts: [
        {
          src: '/js/cases.js',
          type: 'module'
        },
        {
          src        : "https://unpkg.com/iframe-resizer@4.2.10/js/iframeResizer.min.js",
          integrity  : "sha384-2SLwonW5yBpif141hQ3+4T86vXbJL8Ge/uZLpdKkd6s0o2C0t6XuJor6D66Z6gkj",
          crossorigin: "anonymous",
        },
      ],
      bodyHtmlSnippet: '<div id="app"></div>',
    }),

    new HtmlWebpackPlugin({
      // The html-webpack-template plugin requires inject to be false,
      // but we need it to be true so we can use the webpack-cdn-plugin:
      // https://github.com/jaketrent/html-webpack-template/issues/61
      inject    : false,
      cdnModule : 'default',
      template  : require('html-webpack-template'),
      mobile    : true,
      hash      : false,
      title     : 'Painel: COVID-19 e o Risco a Indígenas no Brasil',
      favicon   : './web/images/icons/isa.ico',
      chunks    : [ 'indigenous' ],
      filename  : '../indigenas/index.html',
      links     : [
        // Needed by Vuetify, see https://vuetifyjs.com/en/getting-started/quick-start/
        "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900",
        "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css",
      ],
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
      // Comment this when setting inject to true
      scripts: [
        {
          src: '/js/indigenous.js',
          type: 'module'
        },
        {
          src        : "https://unpkg.com/iframe-resizer@4.2.10/js/iframeResizer.min.js",
          integrity  : "sha384-2SLwonW5yBpif141hQ3+4T86vXbJL8Ge/uZLpdKkd6s0o2C0t6XuJor6D66Z6gkj",
          crossorigin: "anonymous",
        },
      ],
      bodyHtmlSnippet: '<div id="app"></div>',
    }),

    new HtmlWebpackPlugin({
      // The html-webpack-template plugin requires inject to be false,
      // but we need it to be true so we can use the webpack-cdn-plugin:
      // https://github.com/jaketrent/html-webpack-template/issues/61
      inject    : false,
      cdnModule : 'default',
      template  : require('html-webpack-template'),
      mobile    : true,
      hash      : false,
      title     : 'Painel: COVID-19 e o Risco a Indígenas no Brasil',
      favicon   : './web/images/icons/isa.ico',
      chunks    : [ 'indigenous_cases' ],
      filename  : '../indigenas/casos/index.html',
      links     : [
        // Needed by Vuetify, see https://vuetifyjs.com/en/getting-started/quick-start/
        "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900",
        "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css",
      ],
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
      // Comment this when setting inject to true
      scripts: [
        {
          src: '/js/indigenous_cases.js',
          type: 'module'
        },
        {
          src        : "https://unpkg.com/iframe-resizer@4.2.10/js/iframeResizer.min.js",
          integrity  : "sha384-2SLwonW5yBpif141hQ3+4T86vXbJL8Ge/uZLpdKkd6s0o2C0t6XuJor6D66Z6gkj",
          crossorigin: "anonymous",
        },
      ],
      bodyHtmlSnippet: '<div id="app"></div>',
    }),

    new HtmlWebpackPlugin({
      // The html-webpack-template plugin requires inject to be false,
      // but we need it to be true so we can use the webpack-cdn-plugin:
      // https://github.com/jaketrent/html-webpack-template/issues/61
      inject    : false,
      cdnModule : 'default',
      template  : require('html-webpack-template'),
      mobile    : true,
      hash      : false,
      title     : 'Painel: COVID-19 e o Risco a Indígenas no Brasil',
      favicon   : './web/images/icons/isa.ico',
      chunks    : [ 'indigenous_risks' ],
      filename  : '../indigenas/riscos/index.html',
      links     : [
        // Needed by Vuetify, see https://vuetifyjs.com/en/getting-started/quick-start/
        "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900",
        "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css",
      ],
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
      // Comment this when setting inject to true
      scripts: [
        {
          src: '/js/indigenous_risks.js',
          type: 'module'
        },
        {
          src        : "https://unpkg.com/iframe-resizer@4.2.10/js/iframeResizer.min.js",
          integrity  : "sha384-2SLwonW5yBpif141hQ3+4T86vXbJL8Ge/uZLpdKkd6s0o2C0t6XuJor6D66Z6gkj",
          crossorigin: "anonymous",
        },
      ],
      bodyHtmlSnippet: '<div id="app"></div>',
    }),

    new HtmlWebpackPlugin({
      // The html-webpack-template plugin requires inject to be false,
      // but we need it to be true so we can use the webpack-cdn-plugin:
      // https://github.com/jaketrent/html-webpack-template/issues/61
      inject    : false,
      cdnModule : 'default',
      template  : require('html-webpack-template'),
      mobile    : true,
      hash      : false,
      title     : 'Painel: COVID-19 e o Risco a Yanomamis no Brasil',
      favicon   : './web/images/icons/isa.ico',
      chunks    : [ 'indigenous_risks' ],
      filename  : '../indigenas/yanomami/index.html',
      links     : [
        // Needed by Vuetify, see https://vuetifyjs.com/en/getting-started/quick-start/
        "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900",
        "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css",
      ],
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
      // Comment this when setting inject to true
      scripts: [
        {
          src: '/js/indigenous_yanomami.js',
          type: 'module'
        },
        {
          src        : "https://unpkg.com/iframe-resizer@4.2.10/js/iframeResizer.min.js",
          integrity  : "sha384-2SLwonW5yBpif141hQ3+4T86vXbJL8Ge/uZLpdKkd6s0o2C0t6XuJor6D66Z6gkj",
          crossorigin: "anonymous",
        },
      ],
      bodyHtmlSnippet: '<div id="app"></div>',
    }),

    new HtmlWebpackPlugin({
      // The html-webpack-template plugin requires inject to be false,
      // but we need it to be true so we can use the webpack-cdn-plugin:
      // https://github.com/jaketrent/html-webpack-template/issues/61
      inject    : false,
      cdnModule : 'default',
      template  : require('html-webpack-template'),
      mobile    : true,
      hash      : false,
      title     : 'Painel: COVID-19 - APIB - Totais',
      favicon   : './web/images/icons/isa.ico',
      chunks    : [ 'indigenous_apib_totals' ],
      filename  : '../indigenas/apib/totais/index.html',
      links     : [
        // Needed by Vuetify, see https://vuetifyjs.com/en/getting-started/quick-start/
        "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900",
        "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css",
      ],
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
      // Comment this when setting inject to true
      scripts: [
        {
          src: '/js/indigenous_apib_totals.js',
          type: 'module'
        },
        {
          src        : "https://unpkg.com/iframe-resizer@4.2.10/js/iframeResizer.min.js",
          integrity  : "sha384-2SLwonW5yBpif141hQ3+4T86vXbJL8Ge/uZLpdKkd6s0o2C0t6XuJor6D66Z6gkj",
          crossorigin: "anonymous",
        },
      ],
      bodyHtmlSnippet: '<div id="app"></div>',
    }),

    new HtmlWebpackPlugin({
      // The html-webpack-template plugin requires inject to be false,
      // but we need it to be true so we can use the webpack-cdn-plugin:
      // https://github.com/jaketrent/html-webpack-template/issues/61
      inject    : false,
      cdnModule : 'default',
      template  : require('html-webpack-template'),
      mobile    : true,
      hash      : false,
      title     : 'Painel: COVID-19 - APIB - Série temporal',
      favicon   : './web/images/icons/isa.ico',
      chunks    : [ 'indigenous_apib_series' ],
      filename  : '../indigenas/apib/serie/index.html',
      links     : [
        // Needed by Vuetify, see https://vuetifyjs.com/en/getting-started/quick-start/
        "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900",
        "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css",
      ],
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
      // Comment this when setting inject to true
      scripts: [
        {
          src: '/js/indigenous_apib_series.js',
          type: 'module'
        },
        {
          src        : "https://unpkg.com/iframe-resizer@4.2.10/js/iframeResizer.min.js",
          integrity  : "sha384-2SLwonW5yBpif141hQ3+4T86vXbJL8Ge/uZLpdKkd6s0o2C0t6XuJor6D66Z6gkj",
          crossorigin: "anonymous",
        },
      ],
      bodyHtmlSnippet: '<div id="app"></div>',
    }),

    new HtmlWebpackPlugin({
      // The html-webpack-template plugin requires inject to be false,
      // but we need it to be true so we can use the webpack-cdn-plugin:
      // https://github.com/jaketrent/html-webpack-template/issues/61
      inject    : false,
      cdnModule : 'default',
      template  : require('html-webpack-template'),
      mobile    : true,
      hash      : false,
      title     : 'Painel: COVID-19 - APIB - Povos afetados',
      favicon   : './web/images/icons/isa.ico',
      chunks    : [ 'indigenous_apib_peoples' ],
      filename  : '../indigenas/apib/povos/index.html',
      links     : [
        // Needed by Vuetify, see https://vuetifyjs.com/en/getting-started/quick-start/
        "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900",
        "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css",
      ],
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
      // Comment this when setting inject to true
      scripts: [
        {
          src: '/js/indigenous_apib_peoples.js',
          type: 'module'
        },
        {
          src        : "https://unpkg.com/iframe-resizer@4.2.10/js/iframeResizer.min.js",
          integrity  : "sha384-2SLwonW5yBpif141hQ3+4T86vXbJL8Ge/uZLpdKkd6s0o2C0t6XuJor6D66Z6gkj",
          crossorigin: "anonymous",
        },
      ],
      bodyHtmlSnippet: '<div id="app"></div>',
    }),

    new HtmlWebpackPlugin({
      // The html-webpack-template plugin requires inject to be false,
      // but we need it to be true so we can use the webpack-cdn-plugin:
      // https://github.com/jaketrent/html-webpack-template/issues/61
      inject    : false,
      cdnModule : 'default',
      template  : require('html-webpack-template'),
      mobile    : true,
      hash      : false,
      title     : 'Painel: COVID-19 - APIB - Estados',
      favicon   : './web/images/icons/isa.ico',
      chunks    : [ 'indigenous_apib_states' ],
      filename  : '../indigenas/apib/estados/index.html',
      links     : [
        // Needed by Vuetify, see https://vuetifyjs.com/en/getting-started/quick-start/
        "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900",
        "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css",
      ],
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
      // Comment this when setting inject to true
      scripts: [
        {
          src: '/js/indigenous_apib_states.js',
          type: 'module'
        },
        {
          src        : "https://unpkg.com/iframe-resizer@4.2.10/js/iframeResizer.min.js",
          integrity  : "sha384-2SLwonW5yBpif141hQ3+4T86vXbJL8Ge/uZLpdKkd6s0o2C0t6XuJor6D66Z6gkj",
          crossorigin: "anonymous",
        },
      ],
      bodyHtmlSnippet: '<div id="app"></div>',
    }),

    new HtmlWebpackPlugin({
      // The html-webpack-template plugin requires inject to be false,
      // but we need it to be true so we can use the webpack-cdn-plugin:
      // https://github.com/jaketrent/html-webpack-template/issues/61
      inject    : false,
      cdnModule : 'default',
      template  : require('html-webpack-template'),
      mobile    : true,
      hash      : false,
      title     : 'Painel: COVID-19 - APIB - Municípios',
      favicon   : './web/images/icons/isa.ico',
      chunks    : [ 'indigenous_apib_cities' ],
      filename  : '../indigenas/apib/municipios/index.html',
      links     : [
        // Needed by Vuetify, see https://vuetifyjs.com/en/getting-started/quick-start/
        "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900",
        "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css",
      ],
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
      // Comment this when setting inject to true
      scripts: [
        {
          src: '/js/indigenous_apib_cities.js',
          type: 'module'
        },
        {
          src        : "https://unpkg.com/iframe-resizer@4.2.10/js/iframeResizer.min.js",
          integrity  : "sha384-2SLwonW5yBpif141hQ3+4T86vXbJL8Ge/uZLpdKkd6s0o2C0t6XuJor6D66Z6gkj",
          crossorigin: "anonymous",
        },
      ],
      bodyHtmlSnippet: '<div id="app"></div>',
    }),

    new HtmlWebpackPlugin({
      // The html-webpack-template plugin requires inject to be false,
      // but we need it to be true so we can use the webpack-cdn-plugin:
      // https://github.com/jaketrent/html-webpack-template/issues/61
      inject    : false,
      cdnModule : 'default',
      template  : require('html-webpack-template'),
      mobile    : true,
      hash      : false,
      title     : 'Painel: COVID-19 e o Risco a Quilombolas',
      favicon   : './web/images/icons/isa.ico',
      chunks    : [ 'quilombolas' ],
      filename  : '../quilombolas/index.html',
      links     : [
        // Needed by Vuetify, see https://vuetifyjs.com/en/getting-started/quick-start/
        "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900",
        "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css",
      ],
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
      // Comment this when setting inject to true
      scripts: [
        {
          src: '/js/quilombolas.js',
          type: 'module'
        },
        {
          src        : "https://unpkg.com/iframe-resizer@4.2.10/js/iframeResizer.min.js",
          integrity  : "sha384-2SLwonW5yBpif141hQ3+4T86vXbJL8Ge/uZLpdKkd6s0o2C0t6XuJor6D66Z6gkj",
          crossorigin: "anonymous",
        },
      ],
      bodyHtmlSnippet: '<div id="app"></div>',
    }),

    new HtmlWebpackPlugin({
      // The html-webpack-template plugin requires inject to be false,
      // but we need it to be true so we can use the webpack-cdn-plugin:
      // https://github.com/jaketrent/html-webpack-template/issues/61
      inject    : false,
      cdnModule : 'default',
      template  : require('html-webpack-template'),
      mobile    : true,
      hash      : false,
      title     : 'Painel: COVID-19 e o Risco a Quilombolas',
      favicon   : './web/images/icons/isa.ico',
      chunks    : [ 'quilombolas_cases' ],
      filename  : '../quilombolas/casos/index.html',
      links     : [
        // Needed by Vuetify, see https://vuetifyjs.com/en/getting-started/quick-start/
        "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900",
        "https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css",
      ],
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
      // Comment this when setting inject to true
      scripts: [
        {
          src: '/js/quilombolas_cases.js',
          type: 'module'
        },
        {
          src        : "https://unpkg.com/iframe-resizer@4.2.10/js/iframeResizer.min.js",
          integrity  : "sha384-2SLwonW5yBpif141hQ3+4T86vXbJL8Ge/uZLpdKkd6s0o2C0t6XuJor6D66Z6gkj",
          crossorigin: "anonymous",
        },
      ],
      bodyHtmlSnippet: '<div id="app"></div>',
    }),

    // See https://stackoverflow.com/questions/25553868/current-file-path-in-webpack#25565222
    //new webpack.DefinePlugin({
    //  __NAME: webpack.DefinePlugin.runtimeValue(
    //    v => {
    //      //const res = v.module.rawRequest.substr(2)
    //      //return `'${res.substr(0, res.lastIndexOf('.'))}'`
    //      var res  = v.module.rawRequest.substr(2);
    //          res  = res.substr(0, res.lastIndexOf('.'));
    //      var base = new String(res).substring(res.lastIndexOf('/') + 1);

    //      return base;
    //    }, []
    //  )
    //}),
  ],
  entry: {
    'index'                  : './src/pages/index.js',
    'cases'                  : './src/pages/cases.js',
    'indigenous'             : './src/pages/indigenous.js',
    'indigenous_risks'       : './src/pages/indigenous/risks.js',
    'indigenous_cases'       : './src/pages/indigenous/cases.js',
    'indigenous_yanomami'    : './src/pages/indigenous/yanomami.js',
    'indigenous_apib_totals' : './src/pages/indigenous/apib/totals.js',
    'indigenous_apib_series' : './src/pages/indigenous/apib/series.js',
    'indigenous_apib_peoples': './src/pages/indigenous/apib/peoples.js',
    'indigenous_apib_states' : './src/pages/indigenous/apib/states.js',
    'indigenous_apib_cities' : './src/pages/indigenous/apib/cities.js',
    'quilombolas'            : './src/pages/quilombolas.js',
    'quilombolas_cases'      : './src/pages/quilombolas/cases.js',
    //'worker'               : './src/components/tools/worker.js',
  },
};
